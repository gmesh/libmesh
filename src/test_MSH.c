/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: test_MSH.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"

int main(int argc, char **argv) {
  /* Date of the beginning of the simulation */
  char *date_of_simulation;
  /* Structure from time.h containing date, time, etc */
  static struct timeval current_time;
  /* Structure containing computer clock time */
  struct tm *clock_time;
  /* Current time in seconds when programm starts */
  time_t clock;
  /* Error report */
  int timi;
  /* Current time in simulation */
  double t, dt;
  /* Time of end of the simulation in s */
  double tend;

  FILE *fp;
  char nom_fichier[STRING_LENGTH_LP];

  /* Creates the simulation structure */
  Simul = init_simul();

  /* Date */
  timi = gettimeofday(&current_time, NULL);
  clock = (time_t)current_time.tv_sec;
  ctime(&clock);
  clock_time = localtime(&clock);
  date_of_simulation = asctime(clock_time);

  /* Opening of command file */
  if (argc != 3)
    LP_error(stderr, "WRONG COMMAND LINE : libmesh%4.2f CommandFileName DebugFileName\n", VERSION_MSH);

  if ((Simul->poutputs = fopen(argv[2], "w")) == 0)
    LP_error(stderr, "Impossible to open the debug file %s\n", argv[2]);

  LP_log_file_header(stderr, "libmesh", VERSION_MSH, date_of_simulation, argv[1]);
  LP_log_file_header(Simul->poutputs, "libmesh", VERSION_MSH, date_of_simulation, argv[1]);

  /********************************************************/
  /*    Now input files are read  --> see file input.y    */
  /********************************************************/

  CHR_begin_timer();
  lecture(argv[1], Simul->poutputs);
  Simul->pclock->time_spent[LEC_CHR] += CHR_end_timer();

  /********************************************************/
  /*           Starting HYDRO calculations                */
  /********************************************************/

  t = Simul->pchronos->t[BEGINNING_TS];
  tend = Simul->pchronos->t[END_TS];
  dt = Simul->pchronos->dt;
  CHR_calculate_simulation_date(Simul->pchronos, t);

  CHR_begin_timer();
  MSH_check_redundancy(Simul->pcmsh, Simul->poutputs);
  MSH_referencing_mesh(Simul->pcmsh, Simul->poutputs);

  MSH_order_element_in_ref(Simul->pcmsh, Simul->poutputs);

  MSH_check_element_ordering(Simul->pcmsh, Simul->poutputs);

  // Compatibilite avec GEOSAM mailleur de newsam
  //  MSH_print_geosam_input(Simul->pcmsh,Simul->poutputs);
  // Correspondance with GIS
  MSH_link_mesh_SIG(Simul->pcmsh, Simul->poutputs);

  MSH_create_ele_faces(Simul->pcmsh, Simul->poutputs);
  // MSH_link_mesh_SIG(Simul->pcmsh,Simul->poutputs);
  MSH_create_vertical_faces(Simul->pcmsh, Simul->poutputs);
  MSH_tab_all_neigh(Simul->pcmsh, Simul->poutputs);
  MSH_list_all_ele_pneigh(Simul->pcmsh, Simul->poutputs);
  if (Simul->write_odic == YES_MSH) {
    Simul->pcmsh->p_layer[0]->id = SURF_MSH;
    MSH_print_vois_odic(Simul->pcmsh, Simul->poutputs);
  }

  // MSH_list_all_ele(Simul->pcmsh,Simul->poutputs);
  // MSH_get_layer_rank_by_name(Simul->pcmsh,strdup("cretace"),Simul->poutputs);
  //  MSH_get_layer_rank_by_name(Simul->pcmsh,Simul->pcmsh->p_layer[0]->name,Simul->poutputs);
  // LP_printf(Simul->poutputs,"layer : %d \n", MSH_get_layer_rank_by_name(Simul->pcmsh,Simul->pcmsh->p_layer[0]->name,Simul->poutputs));
  // MSH_link_neighbour(Simul->pcmsh,Simul->poutputs);

  Simul->pclock->time_spent[OUTPUTS_CHR] += CHR_end_timer();

  /********************************************************/
  /*        End, summary of the computation length        */
  /********************************************************/

  CHR_calculate_calculation_length(Simul->pclock, Simul->poutputs);
  printf("Time of calculation : %f s\n", Simul->pclock->time_length);

  CHR_print_times(Simul->poutputs, Simul->pclock);

  fclose(Simul->poutputs);
  return 0;
}
