/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: param_MSH.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file param_MSH.h
Here are all the parameters used for libmesh. Most of this parameters are enumeration which explicitly caracterize integers to simplify the code writting and understanding.
*/
#define CODE_MSH CODE_TS
#define SURF_MSH CODE_TS

#define VERSION_MSH 0.17

enum id_msh { GIS_MSH, INTERN_MSH, ABS_MSH, NID_MSH }; /*!<GIS_MSH -> identifiant du SIG, INTERN_MSH-> identifiant interne a une layer varie de 0 a nele_layer-1, ABS_MSH -> identifiant absolu unique pour tout le maillage attention numerote de 1 a nelement total*/
enum dim_msh { X_MSH, Y_MSH, Z_MSH, ND_MSH };
enum orientation_msh { ONE_MSH, TWO_MSH, NFACE_MSH };
enum dir { E_MSH, W_MSH, N_MSH, S_MSH, NCARD_MSH };
enum dir_odic { HOR, INF, SUP, NODIC_MSH };
#define X X_MSH
#define Y Y_MSH
// #define Z Z_MSH
#define CURRENT NEXTREMA_TS

#define ONE ONE_MSH
#define TWO TWO_MSH

#define NCORNER_MSH 4
#define EPS_MSH 0.00000001

#define LLINE_F77_MSH LLINE_F77_LP
#define LLINE_NSAM_MSH LLINE_NSAM_LP

enum code_gigogne { NW, NE, SW, SE, NCODEG_MSH };
enum code_coord { CNW, CNE, CSE, CSW, NCODECOORD_MSH };
enum up_down { DOWN_MSH, UP_MSH, NUPDWN_MSH };
enum left_right { LEFT_MSH, RIGHT_MSH, NDIR_MSH };
enum answer { YES_MSH, NO_MSH, ANSW_MSH };

#define GEOSAM_BUG 10000 // maximum size of the length of a mesh side. geosam will interprete it as a fraction of meter above this value (10km)

#define IDSUBFACE_MSH 1000000
#define IDGHOST_MSH 9000000

enum cell_type { ACTIVE, REFERENCE, SUBREF, GHOST, BOUND, NCELL_TYPE_MSH };
enum loc_cell { NO_LOC_MSH, TOP_MSH, BOT_MSH, RIV_MSH, LOC_MSH };

enum nested_size { COARSE_MSH, FINE_MSH, NRESOLUTION_MSH };
