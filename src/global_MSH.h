/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: global_MSH.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file global_MSH.h
Here are the global parameters for input.y
*/
/* Parameter for error messages with LEX and YACC */
#define YYERROR_VERBOSE 1
/* Maximum number of lines that can be read in a file */
#define YYMAXDEPTH 50000
/* Maximum number of input files which can be read with include() */
#define NPILE 25

typedef struct file_pile s_file;

/* Structure containing files' charcteristics */
struct file_pile {
  /* File's name */
  // char *name;
  char *name;
  /* Content of the buffer read ahead */
  void *buffer;
  /* Adress of the file */
  FILE *address;
  /* Currently read line number */
  int line_nb;
};
