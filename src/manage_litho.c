/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_litho.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

/**\fn s_litho_msh *MSH_create_litho(char *name,int id_gis,int id)
 *\brief create litho
 *
 *
 */
s_litho_msh *MSH_create_litho(char *name, int id_gis, int id) {

  s_litho_msh *plitho;

  plitho = new_litho_msh();
  bzero((char *)plitho, sizeof(s_litho_msh));
  sprintf(plitho->name, "%s", name);
  plitho->id[GIS_MSH] = id_gis;
  plitho->id[INTERN_MSH] = id;
  return plitho;
}

/**\fn s_litho_msh *MSH_browse_litho(s_litho_msh *plitho,int iwhere)
 *\brief rewind or forward chained litho
 *
 * iwhere can be BEGINNING_TS or END_TS
 */
s_litho_msh *MSH_browse_litho(s_litho_msh *plitho, int iwhere) {
  s_litho_msh *ptmp, *preturn;
  ptmp = plitho;

  while (ptmp != NULL) {
    preturn = ptmp;
    switch (iwhere) {
    case BEGINNING_TS:
      ptmp = ptmp->prev;
      break;
    case END_TS:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}

/** \fn s_litho_msh *MSH_chain_fwd_litho(s_litho_msh *pd1,s_litho_msh *pd2)
/* \brief Links two s_litho together */
s_litho_msh *MSH_chain_fwd_litho(s_litho_msh *pd1, s_litho_msh *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd2;
}

s_litho_msh *MSH_copy_litho(s_litho_msh *plitho, FILE *flog) {
  s_litho *pl_cpy;
  pl_cpy = MSH_create_litho(plitho->name, plitho->id[GIS_MSH], plitho->id[INTERN_MSH]);
  return pl_cpy;
}

s_litho_msh *MSH_create_litho_sequence_in_ele(s_litho_msh *pl, FILE *flog) {
  s_litho_msh *pltmp, *plnew;
  pltmp = pl;
  plnew = MSH_copy_litho(pltmp, flog);
  pltmp = pltmp->next;
  while (pltmp != NULL) {
    plnew = MSH_chain_fwd_litho(plnew, MSH_copy_litho(pltmp, flog));
    pltmp = pltmp->next;
  }
  plnew = MSH_browse_litho(plnew, BEGINNING_TS);
  return plnew;
};

void *MSH_create_litho_for_each_ele_in_layer(s_layer_msh *player, FILE *flog) {
  int i, j, nlitho;
  s_ele_msh *pele;
  s_litho_msh *plitho;

  // nlitho=player->nlitho;
  plitho = MSH_browse_litho(player->plitho, BEGINNING_TS);
  for (i = 0; i < player->nele; i++) {
    pele = player->p_ele[i];
    pele->phydro->plitho = MSH_create_litho_sequence_in_ele(plitho, flog)
  }
}
