/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_layer.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

/**\fn s_layer_msh **MSH_tab_layer(s_layer_msh *play,int nlay,FILE *fp)
 *\brief generate the layer tab
 *
 */
s_layer_msh **MSH_tab_layer(s_layer_msh *play, int nlay, FILE *fp) {
  s_layer_msh **p_lay;
  int i = 0;
  p_lay = (s_layer_msh **)malloc(nlay * sizeof(s_layer_msh *));
  for (i = nlay - 1; i >= 0; i--) {
    p_lay[i] = play;
    play = play->prev;
  }
  return p_lay;
}

/**\fn void MSH_list_ele_layer(s_layer_msh *play,FILE *fp)
 *\brief print element in layer (short)
 *
 */
void MSH_list_ele_layer(s_layer_msh *play, FILE *fp) {

  s_ele_msh *pele;
  int i, n;
  n = play->nele;
  pele = play->pele;
  while (pele != NULL) {
    MSH_carac_ele_short(pele, fp);
    pele = pele->next;
  }
}
/**\fn void MSH_list_ele_layer_full(s_layer_msh *play,FILE *fp)
 *\brief print element in layer (full)
 *
 *print full element caracteristics, layer name and number of element in the layer
 */
void MSH_list_ele_layer_full(s_layer_msh *play, FILE *fp) {

  s_ele_msh *pele;
  int i, n;
  n = play->nele;
  pele = play->pele;
  MSH_carac_layer(play, fp);
  while (pele != NULL) {
    MSH_carac_ele_full(pele, fp);
    pele = pele->next;
  }
}

/**\fn void MSH_list_ele_layer_full(s_layer_msh *play,FILE *fp)
 *\brief print element in layer (full)
 *
 *print full element caracteristics, layer name and number of element in the layer
 */
void MSH_list_ele_layer_full_pneigh(s_layer_msh *play, s_cmsh *pcmsh, FILE *fp) {

  s_ele_msh *pele;
  int i, n;
  n = play->nele;
  pele = play->pele;
  MSH_carac_layer(play, fp);
  while (pele != NULL) {
    MSH_carac_ele_full_pneigh(pele, pcmsh, fp);
    pele = pele->next;
  }
}

/**\fn void MSH_tab_neigh_layer(s_layer_msh *play,FILE *fp)
 *\brief create neighboor tabular in layer
 *
 *create neighboor tab for each direction.
 */
void MSH_tab_neigh_layer(s_layer_msh *play, FILE *fp) {

  s_ele_msh *pele;
  int i, n;
  n = play->nele;
  pele = play->pele;
  while (pele != NULL) {
    MSH_tab_neigh(pele, fp);
    pele = pele->next;
  }
}

/**\fn void MSH_list_ele_layer(s_layer_msh *play,FILE *fp)
 *\brief print layer caracteristcs
 *
 */
void MSH_carac_layer(s_layer_msh *play, FILE *fp) {

  LP_printf(fp, "Layer %s, id %d : %d elements\n", play->name, play->id, play->nele);
  // MSH_list_ele_layer(play,fp);
}
/**\fn void MSH_check_layer_redundancy(s_layer_msh *play,FILE *fp)
 *\brief check if there is redundancy of element in layer mesh
 *
 *check  if there 2 different element with the same id_gis
 */
void MSH_check_layer_redundancy(s_layer_msh *play, FILE *fp) {

  s_ele_msh *pele;
  s_ele_msh *pele2;
  int count = 0;

  pele = play->pele;
  while (pele != NULL) {
    pele2 = play->pele;
    while (pele2 != NULL) {
      if ((pele->id[GIS_MSH] == pele2->id[GIS_MSH]) && (pele != pele2)) {
        count++;
        LP_warning(fp, "In layer %s: element %d is redundant, ie rec %d = rec %d", play->name, pele->id[GIS_MSH], pele->id[INTERN_MSH], pele2->id[INTERN_MSH]);
      }
      pele2 = pele2->next;
    }
    pele = pele->next;
  }
  if (count > 0)
    LP_error(fp, "%d Redundancies in layer %s --> check your input file", count / 2, play->name);
  else
    LP_printf(fp, "layer %s checked\n", play->name);
}
/**\fn s_layer_msh *MSH_create_layer(char *name,int id)
 *\brief create layer
 *
 *
 */
s_layer_msh *MSH_create_layer(char *name, int id) {

  s_layer_msh *player;

  player = new_layer_msh();
  bzero((char *)player, sizeof(s_layer_msh));
  player->type = CONFINED_AQ;
  player->nlitho = 1;
  player->cond_type = AUTO_COND; // NF 13/9/2022 changing initialisation of the cond type of a layer to AUTO_COND for the automatic calculation of the conductance between layers. It was previously set up to COND, that I donnot get. I'll switch to COND as long as a conductance is define on the layer!
  sprintf(player->name, "%s", name);
  player->id = id;

#ifdef COUPLED
#endif

  return player;
}

/**\fn s_layer_msh *MSH_chain_layer(s_layer_msh *pd1, s_layer_msh *pd2)
 *\brief chain layers
 *
 *
 */
s_layer_msh *MSH_chain_layer(s_layer_msh *pd1, s_layer_msh *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd1;
}

// BL NF 23/5/2015 BUG detected le gigo evoyé est celui de pele ( de l'appel) et non celui de pele in pele_ref !!! Il faut donc introduire un correctif pour checker le gigo[0]
int MSH_verif_vert_neigh_exist(s_ele_msh *peleref, int *gigo, int norder, s_cmsh *pcmsh, FILE *fpout) {

  s_ele_msh *ptmp;
  int nele, verif;

  verif = NO_MSH;
  nele = peleref->nele;

  if (nele > 0) {
    if (gigo[0] == 0) {
      verif = YES_MSH;
    } else {
      ptmp = MSH_get_subele(peleref, 0);
      if ((ptmp != NULL) && (MSH_is_ele_a_ref(ptmp, pcmsh) == YES_MSH))
        // if((ptmp!=NULL)&&(MSH_get_ele_order(ptmp)==1))
        verif = YES_MSH;
      else
        verif = MSH_verif_verti_layer(peleref, norder, gigo, fpout);
    }
  }
  return verif;
}

int MSH_get_layer_ref_up(s_cmsh *pcmsh, int layer, int id_ref, int *gigo, int norder, FILE *fpout) {
  s_ele_msh *peleref;
  int nele, ncount, verif;
  int layer_up;
  // peleref=new_ele_msh();
  // bzero((char*)peleref,sizeof(s_ele_msh));
  // peleref=NULL;
  ncount = 0;
  verif = NO_MSH;
  while (layer > 0 && verif == NO_MSH) {
    layer--;
    ncount++;
    peleref = pcmsh->play_ref[layer]->p_ele[id_ref];

    // BL NF 23/5/2015 BUG detected le gigo evoyé est celui de pele ( de l'appel) et non celui de pele in pele_ref !!! Il faut donc introduire un correctif pour checker le gigo[0]
    verif = MSH_verif_vert_neigh_exist(peleref, gigo, norder, pcmsh, fpout);
  }

  if (layer == 0 && (ncount == 0 || verif == NO_MSH)) {
    // LP_printf(fpout,"No neighbor UP : ele_ref %d in layer %d is a SURFACE element\n",id_ref,layer);
    layer_up = CODE_MSH;
  } else {
    layer_up = layer;
  }
  return layer_up;
}

int MSH_get_layer_ref_down(s_cmsh *pcmsh, int layer, int id_ref, int *gigo, int norder, FILE *fpout) {
  s_ele_msh *peleref, *ptmp;
  int nlayer, nele, layer_down, verif;
  int ncount;
  //  peleref=new_ele_msh();
  // bzero((char*)peleref,sizeof(s_ele_msh));
  // peleref=NULL;
  ncount = 0;
  nlayer = pcmsh->pcount->nlayer;
  verif = NO_MSH;

  //  LP_printf(fpout,"libmesh%4.2f: %s l%d DEBUG check id_ref send to MSH_get_layer_ref_down : %d for layer %d\n",VERSION_MSH,__FILE__,__LINE__,id_ref,layer); //BL to debug

  while (layer < (nlayer - 1) && verif == NO_MSH) {
    layer++;
    ncount++;
    peleref = pcmsh->play_ref[layer]->p_ele[id_ref];

    // BL NF 23/5/2015 BUG detected le gigo evoyé est celui de pele ( de l'appel) et non celui de pele in pele_ref !!! Il faut donc introduire un correctif pour checker le gigo[0]
    verif = MSH_verif_vert_neigh_exist(peleref, gigo, norder, pcmsh, fpout);
  }

  if (layer == (nlayer - 1) && (ncount == 0 || verif == NO_MSH)) {
    layer_down = CODE_MSH;
  } else {
    layer_down = layer;
  }

  return layer_down;
}

int MSH_verif_verti_layer(s_ele_msh *peleref, int norder, int *gigo, FILE *fpout) {
  s_ele_msh *verif_eleref;
  s_ele_msh *verif_subele;
  s_ele_msh *ptmp;
  int n, corner, j;
  int norder_verif, norder_sub;
  int verif = YES_MSH;
  int dir;
  int *gigo_sub;
  n = 0;
  j = 0;
  verif_eleref = peleref;
  while (n < norder && verif_eleref != NULL) {
    ptmp = verif_eleref;
    dir = MSH_convert_gigogne_dir(gigo[n], fpout);
    verif_eleref = MSH_get_subele(verif_eleref, dir);
    n++;
  }
  if (verif_eleref == NULL) {
    verif_eleref = ptmp;
    if (verif_eleref->type == ACTIVE || verif_eleref->type == BOUND) {
      verif = YES_MSH;
    } else {

      if (verif_eleref->nele == 1 && (verif_eleref->p_subele[0] != NULL)) {
        norder_sub = MSH_get_ele_order(verif_eleref->p_subele[0]);
        gigo_sub = MSH_get_ele_gigogne(verif_eleref->p_subele[0]);
        norder_verif = MSH_get_ele_order(verif_eleref);
        // if(norder_sub==norder_verif || norder_sub==1) //BL init
        if (norder_sub == norder_verif || gigo_sub[0] == 0) {
          verif = YES_MSH;
        } else {
          verif = NO_MSH;
        } // if (norder_sub==norder_verif || gigo_sub[0]==0)

      } // BL bug ici plante car nele ==1 n'est pas un cas suffisant exemple si dans la layer du bas verif_eleref==NULL car il n'y a qu'un subele au lieux de 4 dans la maille ref. (peut etre faire un test sur norder ?)

      // if(verif_eleref->nele==0)
      else {
        verif = NO_MSH;
      } // if (verif_eleref->type==ACTIVE || verif_eleref->type==BOUND)
    }
  } else {
    if (verif_eleref->nele > 1 && verif_eleref->p_subele != NULL) {
      for (corner = 0; corner < NCORNER_MSH; corner++) {
        verif_subele = MSH_get_subele(verif_eleref, corner);
        if (verif_subele != NULL && verif_subele->nele != 0) {
          break;
        }
        ++j;
      }
      if (j == NCORNER_MSH)
        verif = NO_MSH;
    }
  } // if(verif_eleref==NULL)

  return verif;
}

int MSH_get_verti_layer(s_cmsh *pcmsh, s_ele_msh *pele, int layer, int itype, FILE *fpout) {
  int layer_vert;
  int *gigo;
  int id_ref;
  int norder;

  gigo = MSH_get_ele_gigogne(pele);
  // peleref=pele->pele_ref;
  id_ref = MSH_get_eleref_id(pele, INTERN_MSH);
  norder = MSH_get_ele_order(pele);
  if (itype == ONE_MSH) {
    layer_vert = MSH_get_layer_ref_down(pcmsh, layer, id_ref, gigo, norder, fpout);
  } else {
    layer_vert = MSH_get_layer_ref_up(pcmsh, layer, id_ref, gigo, norder, fpout);
  }
  return layer_vert;
}

int MSH_get_layer_rank_by_name(s_cmsh *pcmsh, char *name_verif, FILE *fpout) {

  s_layer_msh *player;
  int i, length_name, length_verif, n;
  char *name;
  n = CODE_TS;
  for (i = 0; i < pcmsh->pcount->nlayer; i++) {
    player = pcmsh->p_layer[i];
    name = player->name;

    if (strcmp(name_verif, name) == 0) {
      n = i;
    }
  }
  if (n == CODE_TS) {
    LP_error(fpout, "NO layer named %s in grid mesh %s", name_verif, pcmsh->name);
  } else {
    return n;
  }
}

/**\fn s_layer_msh *MSH_find_layer_in_cmsh(s_cmsh *pcmsh,char *name,FILE *fp)
 *\brief returns a pointer towards a layer that is found by its name in a mesh
 *
 *
 */
s_layer_msh *MSH_find_layer_in_cmsh(s_cmsh *pcmsh, char *name, FILE *fp) {
  int idlay;
  s_layer_msh *play;

  idlay = MSH_get_layer_rank_by_name(pcmsh, name, fp);
  play = pcmsh->p_layer[idlay];

  LP_printf(fp, "Layer %s found in mesh %s\n", play->name, pcmsh->name);
  return play;
}

/*
void MSH_free_ref_layer(s_cmsh *pcmsh,FILE *fpout)
{

  int nlayer;
  int l;
  s_layer_msh *playref
  nlayer=pcmsh->pcount->nlayer;
  for(l=0;l<nlayer;l++)
    {

      playref=pcmsh->play_ref[l];
      nele=playref->nele;
      for(e=0;e<nele;e++)
        {
          peleref=playref->p_ele[e];
          peleref=MSH_free_ele(peleref,fpout);

        }

    }





}
*/

/**\fn s_layer_msh *MSH_copy_layer(s_cmsh *pcmsh,FILE *fp)
 *\brief returns pointer towards a copy of a layer
 *
 *
 */
s_layer_msh *MSH_copy_layer(s_layer_msh *play, FILE *fp) {

  s_layer_msh *pcpy;
  s_ele_msh *pele, *pele1, *pele2;

  pcpy = MSH_create_layer(play->name, play->id);
  pcpy->nele = play->nele;
  pele = play->pele;
  pele1 = pele2 = NULL;
  while (pele != NULL) {
    pele2 = MSH_copy_ele(pele, pcpy, fp);
    pele1 = MSH_secured_chain_ele_fwd(pele1, pele2);
    pele = pele->next;
  }
  pcpy->pele = MSH_browse_ele(pele1, BEGINNING_TS);
  pcpy->p_ele = MSH_tab_ele(pcpy->pele, pcpy->nele, fp);
  return pcpy;
}

/**\fn s_layer_msh *MSH_browse_layer(s_layer_msh *player,int iwhere)
 *\brief rewind or forward chained layer
 *
 * iwhere can be BEGINNING_TS or END_TS
 */
s_layer_msh *MSH_browse_layer(s_layer_msh *player, int iwhere) {
  s_layer_msh *ptmp, *preturn;
  ptmp = player;

  while (ptmp != NULL) {
    preturn = ptmp;
    switch (iwhere) {
    case BEGINNING_TS:
      ptmp = ptmp->prev;
      break;
    case END_TS:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}
