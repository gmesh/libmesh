/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_element.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**\file manage_element.c
 *\brief all functions dealing with element management
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

/**\fn int MSH_find_dir_neigh(int gigor,int idir,FILE *fpout)
 *\brief determine direction of neighbor in a same cell
 *\return N,S,E,W
 */
int MSH_find_dir_neigh(int gigor, int idir, FILE *fpout) {
  int gigo;
  switch (idir) {
  case X_MSH:
    switch (gigor) {
    case 1:
      gigo = 2;
      break;
    case 2:
      gigo = 1;
      break;
    case 3:
      gigo = 4;
      break;
    case 4:
      gigo = 3;
      break;
    }
    break;
  case Y:
    switch (gigor) {
    case 1:
      gigo = 3;
      break;
    case 2:
      gigo = 4;
      break;
    case 3:
      gigo = 1;
      break;
    case 4:
      gigo = 2;
      break;
    }
    break;
  }
  return MSH_convert_gigogne_dir(gigo, fpout);
}
/**\fn int MSH_is_neigh_in_up(int gigo,int idir,int itype,FILE *fpout)
 *\brief check if the neighbour of the element of gigo number has its neighbors in the same reference or subreference  element or not in idir of itype
 *
 * idir is X_MSH,Y dimension itype is ONE or TWO direction (ie ONE = i-1 TWO i+1)
 */

int MSH_is_neigh_in_up(int gigo, int idir, int itype, FILE *fpout) {
  int answer = NO_MSH;
  if (gigo == 1) {
    if (((idir == X_MSH) && (itype == TWO)) || ((idir == Y) && (itype == ONE)))
      answer = YES_MSH;
  } else if (gigo == 2) {
    if (((idir == X_MSH) && (itype == ONE)) || ((idir == Y) && (itype == ONE)))
      answer = YES_MSH;
  } else if (gigo == 3) {
    if (((idir == X_MSH) && (itype == TWO)) || ((idir == Y) && (itype == TWO)))
      answer = YES_MSH;
  } else {
    if (((idir == X_MSH) && (itype == ONE)) || ((idir == Y) && (itype == TWO)))
      answer = YES_MSH;
  }

  return answer;
}
/**\fn s_ele_msh *MSH_pick_ele_from_face(int dir,s_face_msh *pface)
 *\brief function to get element from face
 *
 * itype is ONE or TWO direction (ie ONE = i-1 TWO i+1)
 *\return pele(i-1) if dir = ONE pele(i+1) if dir = TWO
 */
s_ele_msh *MSH_pick_ele_from_face(int dir, s_face_msh *pface) {
  s_ele_msh *pele;

  switch (dir) {
  case ONE:
    pele = pface->p_ele[ONE];
    break;
  case TWO:
    pele = pface->p_ele[TWO];
    break;
  }
  return pele;
}
/**\fn s_ele_msh *MSH_pick_ele_in_layer(s_layer_msh *play,int id,FILE *fpout)
 *\brief function to get element by its id
 *
 *
 *\return pele[id]
 */
s_ele_msh *MSH_pick_ele_in_layer(s_layer_msh *play, int id, FILE *fpout) {

  s_ele_msh *pele;

  pele = play->p_ele[id];

  return pele;
}

/**\fn void MSH_destroy_descr(s_descr_msh *pdescr)
 *\brief function to destroy descritpion mesh pointer
 *
 *
 *
 */
s_descr_msh *MSH_destroy_descr(s_descr_msh *pdescr) {

  int i;
  if (pdescr->coord != NULL) {

    for (i = 0; i < NCORNER_MSH; i++) {
      if (pdescr->coord[i] != NULL)
        free(pdescr->coord[i]);
    }
    free(pdescr->coord);
  }
  if (pdescr->center != NULL)
    free(pdescr->center);
  if (pdescr->code_gigogne != NULL)
    free(pdescr->code_gigogne);
  free(pdescr);
  pdescr = NULL;
  return pdescr;
}
/**\fn s_descr_msh *MSH_create_descr()
 *\brief function to create description mesh
 *
 *
 *
 */
s_descr_msh *MSH_create_descr() {

  int i, j;
  s_descr_msh *pdescr;

  pdescr = new_descr_msh();
  bzero((char *)pdescr, sizeof(s_descr_msh));

  pdescr->coord = (double **)malloc(NCORNER_MSH * sizeof(double *));
  for (i = 0; i < NCORNER_MSH; i++) {
    int j;
    pdescr->coord[i] = (double *)malloc(Z_MSH * sizeof(double));
    for (j = 0; j < Z_MSH; j++)
      pdescr->coord[i][j] = (double)CODE_MSH;
  }

  pdescr->center = (double *)malloc(ND_MSH * sizeof(double));
  for (i = 0; i < ND_MSH; i++)
    pdescr->center[i] = (double)CODE_MSH;

  return pdescr;
}
/**\fn void MSH_destroy_ele(s_ele_msh *pele)
 *\brief function to destroy one element and its directions
 *
 */

s_ele_msh *MSH_destroy_ele(s_ele_msh *pele) {
  if (pele->pdescr != NULL)
    pele->pdescr = MSH_destroy_descr(pele->pdescr);
  free(pele);
  pele = NULL;
  return pele;
}

/**\fn s_ele_msh *MSH_create_element(int id_sig,int itype)
 *\brief function to create one element
 *
 * function to create one element given its id_sig and its type (ACTIVE,REFERENCE,SUBREF,GHOST)
 */
s_ele_msh *MSH_create_element(int id_sig, int itype) {
  s_ele_msh *pele;

  pele = new_ele_msh();
  bzero((char *)pele, sizeof(s_ele_msh));

  pele->id[GIS_MSH] = id_sig;
  pele->p_subele = NULL;
  pele->pdescr = MSH_create_descr();
  pele->ndiv = NCORNER_MSH;
  pele->type = itype;

#ifdef COUPLED
  pele->phydro = NULL;
#endif

#ifdef COUPLED_TTC // NG : 19/06/2023. Adding default init.
  pele->nbound_ttc = 0;
  pele->nvois_ttc = 0;
#endif

  return pele;
}

/**\fn s_ele_msh *MSH_chain_ele(s_ele_msh *pd1,s_ele_msh *pd2)
 *\brief chain element
 *
 *
 */
s_ele_msh *MSH_chain_ele(s_ele_msh *pd1, s_ele_msh *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd1;
}

/**\fn s_ele_msh *MSH_chain_ele(s_ele_msh *pd1,s_ele_msh *pd2)
 *\brief chain element
 *
 *
 */
s_ele_msh *MSH_chain_ele_fwd(s_ele_msh *pd1, s_ele_msh *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd2;
}

/**\fn s_ele_msh *MSH_secured_chain_ele(s_ele_msh *pd1,s_ele_msh *pd2)
 *\brief chain element
 *
 *
 */
s_ele_msh *MSH_secured_chain_ele(s_ele_msh *pd1, s_ele_msh *pd2) {
  if (pd1 != NULL)
    pd1 = MSH_chain_ele(pd1, pd2);
  else
    pd1 = pd2;
  return pd1;
}

/**\fn s_ele_msh *MSH_secured_chain_ele(s_ele_msh *pd1,s_ele_msh *pd2)
 *\brief chain element
 *
 *
 */
s_ele_msh *MSH_secured_chain_ele_fwd(s_ele_msh *pd1, s_ele_msh *pd2) {
  if (pd1 != NULL)
    pd1 = MSH_chain_ele_fwd(pd1, pd2);
  else
    pd1 = pd2;
  return pd1;
}

/**\fn void MSH_calcul_sur_ele(s_ele_msh *pele)
 *\brief calculate rapport_cote in element description and nsize in carac_mesh
 *
 * pele and cmsh must be created before
 */
void MSH_calcul_sur_ele(s_ele_msh *ptmp, s_cmsh *pcmsh) {
  double rcote;
  s_ele_msh *pele;                           // BL ajout pour invpiez
  pele = MSH_browse_ele(ptmp, BEGINNING_TS); // BL ajout pour invpiez

  while (pele != NULL) {
    pele->pdescr->rapport_cote = (pcmsh->l_max / pele->pdescr->l_side);
    pele->pdescr->rapport_cote = log(pele->pdescr->rapport_cote); // BL comprends pas pourquoi?
    pele->pdescr->rapport_cote /= log(2.);                        // BL comprends pas pourquoi ?
    if (pele->pdescr->rapport_cote < EPS_MSH)
      pele->pdescr->rapport_cote = 1; // BL pourquoi pas 0 (sans doute pour des boucles sur norder ?
    if (pcmsh->dimension > (1 + EPS_MSH)) {
      rcote = pele->pdescr->rapport_cote;
      rcote += 1.;
      rcote = TS_max(pcmsh->nsize, rcote);
      pcmsh->nsize = (int)rcote;
    } else
      pcmsh->nsize = 1;
    pele = pele->next;
  }
}

/**\fn void MSH_ordinate_mesh(s_ele_msh *pele,s_cmsh *pcmsh)
 *\brief ordinate coord of one element
 *
 * give the x,t max and min of the element, calculate the cell center and sizes
 */
void MSH_ordinate_mesh(s_ele_msh *pele, s_cmsh *pcmsh) {
  double xmin = BIG_TS, xmax = SMALL_TS; // FB SMALL_TS instead of 0 (otherwise if there are negative values the max is not correctly computed)
  double ymin = BIG_TS, ymax = SMALL_TS; // FB "
  int i;
  double length;

  s_descr_msh *pdescr;

  pdescr = pele->pdescr;

  for (i = 0; i < NCORNER_MSH; i++) {
    xmax = TS_max(xmax, pdescr->coord[i][X_MSH]);
    ymax = TS_max(ymax, pdescr->coord[i][Y]);
    xmin = TS_min(xmin, pdescr->coord[i][X_MSH]);
    ymin = TS_min(ymin, pdescr->coord[i][Y]);
  }

  pcmsh->mini[X_MSH] = TS_min(pcmsh->mini[X_MSH], xmin);
  pcmsh->mini[Y] = TS_min(pcmsh->mini[Y], ymin);
  pcmsh->maxi[X_MSH] = TS_max(pcmsh->maxi[X_MSH], xmax);
  pcmsh->maxi[Y] = TS_max(pcmsh->maxi[Y], ymax);
  pdescr->coord[0][X_MSH] = xmin;
  pdescr->coord[0][Y] = ymax;
  pdescr->coord[1][X_MSH] = xmax;
  pdescr->coord[1][Y] = ymax;
  pdescr->coord[2][X_MSH] = xmax;
  pdescr->coord[2][Y] = ymin;
  pdescr->coord[3][X_MSH] = xmin;
  pdescr->coord[3][Y] = ymin;
  pdescr->center[X_MSH] = (xmin + xmax) / 2;
  pdescr->center[Y] = (ymin + ymax) / 2;
  length = xmax - xmin;
  if (pcmsh->l_min > EPS_MSH)
    pcmsh->l_min = TS_min(pcmsh->l_min, length);
  else
    pcmsh->l_min = length;
  pcmsh->l_max = TS_max(pcmsh->l_max, length);
  pdescr->l_side = length;
  length *= length;
  pdescr->surf = length;
}
/**\fn void MSH_carac_ele_short(s_ele_msh *pele,FILE *fp)
 *\brief print element descrition
 *
 *
 */
void MSH_carac_ele_short(s_ele_msh *pele, FILE *fp) {

  s_descr_msh *pdescr;
  pdescr = pele->pdescr;
  if (pele->type == SUBREF)
    LP_error(fp, "libmesh%4.2f %s l%d --> pele id_sig %d, intern_id %d, abs_id %d: area %f m2, size length %f m, is %s, which is not possible in a consistent mesh\n", VERSION_MSH, __FILE__, __LINE__, pele->id[GIS_MSH], pele->id[INTERN_MSH], pele->id[ABS_MSH], pdescr->surf, pdescr->l_side, MSH_name_cell(pele->type, fp));
  else
    LP_printf(fp, "libmesh%4.2f: %s l%d pele id_sig %d, intern_id %d, abs_id %d: area %f m2, size length %f m, rapport_cote %f, is %s\n", VERSION_MSH, __FILE__, __LINE__, pele->id[GIS_MSH], pele->id[INTERN_MSH], pele->id[ABS_MSH], pdescr->surf, pdescr->l_side, pdescr->rapport_cote, MSH_name_cell(pele->type, fp));
}
/**\fn s_ele_msh *MSH_browse_ele(s_ele_msh *pele,int iwhere)
 *\brief rewind or forward chained element
 *
 * iwhere can be BEGINNING_TS or END_TS
 */
s_ele_msh *MSH_browse_ele(s_ele_msh *pele, int iwhere) {
  s_ele_msh *ptmp, *preturn;
  ptmp = pele;

  while (ptmp != NULL) {
    preturn = ptmp;
    switch (iwhere) {
    case BEGINNING_TS:
      ptmp = ptmp->prev;
      break;
    case END_TS:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}
/**\fn s_ele_msh **MSH_tab_ele(s_ele_msh *pele,int nele,FILE *fpout)
 *\brief put element in array.
 *
 * put pele->next in p_ele[i+1]
 *\return p_ele
 */
s_ele_msh **MSH_tab_ele(s_ele_msh *pele, int nele, FILE *fpout) {

  int i;
  s_ele_msh **p_ele;
  s_ele_msh *ptmp;

  ptmp = MSH_browse_ele(pele, BEGINNING_TS);

  p_ele = (s_ele_msh **)malloc(nele * sizeof(s_ele_msh *));
  pele = MSH_browse_ele(pele, BEGINNING_TS);
  // LP_printf(fpout,"\nlayer id : %d\n",pele->player->id); BL to debug
  for (i = 0; i < nele; i++) {
    // LP_printf(fpout,"ABS_ID : %d\n",MSH_get_ele_id(pele,ABS_MSH)); BL to debug
    p_ele[i] = pele;
    pele = pele->next;
  }
  return p_ele;
}
/**\fn int *MSH_get_ele_gigogne(s_ele_msh *pele)
 *\brief get code gigogne of element
 *
 *
 */
int *MSH_get_ele_gigogne(s_ele_msh *pele) { return pele->pdescr->code_gigogne; }
/**\fn int MSH_get_ele_order(s_ele_msh *pele)
 *\brief get element order
 *
 *the order is the size ration of element
 */
int MSH_get_ele_order(s_ele_msh *pele) { return pele->pdescr->rapport_cote; }
/**\fn int  MSH_print_ele_gigogne(s_ele_msh *pele)
 *\brief print the code gigogne of element
 *
 *
 */
int MSH_print_ele_gigogne(s_ele_msh *pele) {
  char *c, ctmp[1];
  int code_gigo;
  int *gigo;
  int n, ntot, i;
  int count = 0;

  gigo = MSH_get_ele_gigogne(pele);
  n = MSH_get_ele_order(pele);
  c = (char *)calloc(n + 1, sizeof(char));
  ntot = n;
  if (gigo[0] > 0) {
    while (count < ntot) {
      sprintf(&c[count], "%d", gigo[count]);
      count++;
    }
  } else
    sprintf(c, "%d", gigo[0]);
  code_gigo = atoi(c);
  free(c);
  return code_gigo;
}
/**\fn int MSH_get_ele_id(s_ele_msh *pele,int id_type)
 *\brief give the id_type (id_SIG,id_newsam) of one element
 *
 *
 */
int MSH_get_ele_id(s_ele_msh *pele, int id_type) { return pele->id[id_type]; }
/**\fn int MSH_get_eleref_id(s_ele_msh *pele,int id_type)
 *\brief give the ref element id_tyepe (id_SIG,id_newsam) in which one element is included
 *
 *
 */
int MSH_get_eleref_id(s_ele_msh *pele, int id_type) { return MSH_get_ele_id(pele->pele_ref, id_type); }
/**\fn s_ele_msh *MSH_get_subele(s_ele_msh *peleref,int icompass_dir)
 *\brief fonction to get subele in peleref using its code_gigo(dir) (E,W,N,S)
 *
 *\return NULL if subele have not been created yet.
 */
s_ele_msh *MSH_get_subele(s_ele_msh *peleref, int icompass_dir) {
  s_ele_msh *pele = NULL;
  if (peleref->p_subele != NULL)
    pele = peleref->p_subele[icompass_dir];
  return pele;
}

/**\fn s_ele_msh *MSH_pick_ele_in_ref(s_cmsh *pcmsh,int nx,int ny,int *gigo,int nratio,FILE *fpout)
 *\brief A FAIRE
 *
 *
 */
s_ele_msh *MSH_pick_ele_in_ref(s_cmsh *pcmsh, int nx, int ny, int layer, int *gigo, int nratio, FILE *fpout) {

  int i, n;
  s_ele_msh *peleref;

  n = nratio;
  peleref = MSH_pick_ele_ref(pcmsh, nx, ny, layer, fpout);

  for (i = 0; i < n; i++) {
    ; // NF revoir routine je ne sais plus ce que j'avais en tete
    peleref = MSH_get_subele(peleref, MSH_convert_gigogne_dir(gigo[i], fpout));
  }

  return peleref;
}

/**\fn void MSH_carac_ele_full_neigh(s_ele_msh *pele,FILE *fp)
 *\brief print all element caracteristics
 *
 *print id_sig, intern_id, area, size length, its type  (ACTIVE,REFERENCE,SUBREF,GHOST), the reference id, all the subelement, and finally, the neighbors,
(direction, id_ neighbor)
 */
void MSH_carac_ele_full_neigh(s_ele_msh *pele, s_cmsh *pcmsh, FILE *fp) {
  s_ele_msh *peleref, *pneigh;
  s_ele_msh *pele_tmp = NULL;
  s_descr_msh *pdescr;
  int i, j, n, count = 0;
  int idir, itype;
  int *gigo;
  int code_gigo;
  s_face_msh *pface, *psubf = NULL;

  pdescr = pele->pdescr;
  peleref = pele->pele_ref;
  gigo = MSH_get_ele_gigogne(pele);
  n = MSH_get_ele_order(pele);

  LP_printf(fp, "pele id_sig %d, intern_id %d: area %f m2, size length %f [m] is in:\n", pele->id[GIS_MSH], pele->id[INTERN_MSH], pdescr->surf, pdescr->l_side);
  LP_printf(fp, "\t --> %s ele %d\n", MSH_name_cell(peleref->type, fp), peleref->id[GIS_MSH]);
  if (gigo[0] == 0)
    LP_printf(fp, "\t --> %s ele %d\n", MSH_name_cell(pele->type, fp), pele->id[GIS_MSH]);

  else {
    while (count < n) {
      code_gigo = gigo[count];
      peleref = MSH_get_subele(peleref, MSH_convert_gigogne_dir(code_gigo, fp));
      LP_printf(fp, "\t%d. %s ele %d\n", ++count, MSH_name_cell(peleref->type, fp), MSH_get_ele_id(peleref, GIS_MSH));
    }
  }

#ifdef COUPLED
  if (pele->type == BOUND) {
    LP_printf(fp, "\t kindof boundary : %s frequence %s\n", AQ_name_BC(pele->phydro->pbound->type), AQ_name_freq(pele->phydro->pbound->freq));
  }
#endif
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pneigh = MSH_get_neigh_2D(pele, idir, itype);
      if (pneigh != NULL) {
        LP_printf(fp, " %s ", MSH_name_neigh(idir, itype));
        pface = pneigh->face[idir][MSH_switch_direction(itype, fp)];
        if (pface->p_subface != NULL && (pneigh->type == REFERENCE || pneigh->type == SUBREF)) // BL 27/05/2014 to print all neigh of element of size n next to element of size n-1 (to go down in reference mesh)
        {
          if (idir != Z_MSH) {
            for (j = 0; j < NDIR_MSH; j++) {
              psubf = pface->p_subface[j];
              if (psubf != NULL && psubf->p_ele[itype] != NULL) {
                LP_printf(fp, "  %d", MSH_get_ele_id(psubf->p_ele[itype], GIS_MSH));
              }
            }

          } else {
            for (j = 0; j < NCORNER_MSH; j++) {
              psubf = pface->p_subface[j];
              if (psubf != NULL && psubf->p_ele[itype] != NULL) {
                LP_printf(fp, "  %d", MSH_get_ele_id(psubf->p_ele[itype], GIS_MSH));
              }
            }
          }
        } else {
          LP_printf(fp, "  %d", MSH_get_ele_id(pneigh, GIS_MSH));
        } // if p_subf

      } // if pneigh
      count++;
    } // for itype
  }   // for idir
  if (count > 0)
    LP_printf(fp, "\n");
  else
    LP_printf(fp, "None.... Should be ghosts\n");

  // MSH_tab_neigh(pele,fp);
  // pcmsh=Simul->pcmsh;
  // MSH_print_ele_neigh(pele,pcmsh,fp);
}

void MSH_carac_ele_full_pneigh(s_ele_msh *pele, s_cmsh *pcmsh, FILE *fp) {
  s_ele_msh *peleref, *pneigh;
  s_ele_msh *pele_tmp = NULL;
  s_descr_msh *pdescr;
  int i, n, count = 0;

  int *gigo;
  int code_gigo;
  s_face_msh *pface, *psubf = NULL;

  pdescr = pele->pdescr;
  peleref = pele->pele_ref;
  gigo = MSH_get_ele_gigogne(pele);
  n = MSH_get_ele_order(pele);

  LP_printf(fp, "pele id_sig %d, intern_id %d, abs_id %d: area %f m2, size length %f [m] is in:\n", pele->id[GIS_MSH], pele->id[INTERN_MSH], pele->id[ABS_MSH], pdescr->surf, pdescr->l_side);
  LP_printf(fp, "\t --> %s ele %d\n", MSH_name_cell(peleref->type, fp), peleref->id[GIS_MSH]);
  if (gigo[0] == 0)
    LP_printf(fp, "\t --> %s ele %d\n", MSH_name_cell(pele->type, fp), pele->id[GIS_MSH]);

  else {
    while (count < n) {
      code_gigo = gigo[count];
      peleref = MSH_get_subele(peleref, MSH_convert_gigogne_dir(code_gigo, fp));
      LP_printf(fp, "\t%d. %s ele %d\n", ++count, MSH_name_cell(peleref->type, fp), MSH_get_ele_id(peleref, GIS_MSH));
    }
  }

#ifdef COUPLED
  if (pele->type == BOUND) {
    if (pele->phydro != NULL) {
      if (pele->phydro->pbound != NULL)
        LP_printf(fp, "\t kindof boundary : %s frequence %s\n", AQ_name_BC(pele->phydro->pbound->type), AQ_name_freq(pele->phydro->pbound->freq));
    }
  }
#endif
  // MSH_tab_neigh(pele,fp);

  MSH_print_ele_neigh(pele, pcmsh, fp);
}

/**\fn void MSH_carac_ele_full(s_ele_msh *pele,FILE *fp)
 *\brief print all element caracteristics
 *
 *print id_sig, intern_id, area, size length, its type  (ACTIVE,REFERENCE,SUBREF,GHOST), the reference id, all the subelement, and finally, the neighbors,
(direction, id_ neighbor)
 */
void MSH_carac_ele_full(s_ele_msh *pele, FILE *fp) {
  s_ele_msh *peleref, *pneigh;
  s_ele_msh *pele_tmp = NULL;
  s_descr_msh *pdescr;
  int i, j, n, count = 0;
  int idir, itype;
  int *gigo;
  int code_gigo;
  s_face_msh *pface, *psubf = NULL;

  pdescr = pele->pdescr;
  peleref = pele->pele_ref;
  gigo = MSH_get_ele_gigogne(pele);
  n = MSH_get_ele_order(pele);

  LP_printf(fp, "pele id_sig %d, intern_id %d: area %f m2, size length %f [m] is %s in:\n", pele->id[GIS_MSH], pele->id[INTERN_MSH], pdescr->surf, pdescr->l_side, MSH_name_cell(pele->type, fp));
  LP_printf(fp, "\t --> %s ele %d\n", MSH_name_cell(peleref->type, fp), peleref->id[GIS_MSH]);
  if (gigo[0] == 0)
    LP_printf(fp, "\t --> %s ele %d\n", MSH_name_cell(pele->type, fp), pele->id[GIS_MSH]);

  else {
    while (count < n) {
      code_gigo = gigo[count];
      peleref = MSH_get_subele(peleref, MSH_convert_gigogne_dir(code_gigo, fp));
      LP_printf(fp, "\t%d. %s ele %d\n", ++count, MSH_name_cell(peleref->type, fp), MSH_get_ele_id(peleref, GIS_MSH));
    }
  }

#ifdef COUPLED
  if (pele->type == BOUND) {
    LP_printf(fp, "\t kindof boundary : %s frequence %s\n", AQ_name_BC(pele->phydro->pbound->type), AQ_name_freq(pele->phydro->pbound->freq));
  }
#endif
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pneigh = MSH_get_neigh_2D(pele, idir, itype);
      if (pneigh != NULL) {
        LP_printf(fp, " %s ", MSH_name_neigh(idir, itype));
        pface = pneigh->face[idir][MSH_switch_direction(itype, fp)];
        if (pface->p_subface != NULL && (pneigh->type == REFERENCE || pneigh->type == SUBREF)) // BL 27/05/2014 to print all neigh of element of size n next to element of size n-1 (to go down in reference mesh)
        {
          if (idir != Z_MSH) {
            for (j = 0; j < NDIR_MSH; j++) {
              psubf = pface->p_subface[j];
              if (psubf != NULL && psubf->p_ele[itype] != NULL) {
                LP_printf(fp, "  %d", MSH_get_ele_id(psubf->p_ele[itype], GIS_MSH));
              }
            }

          } else {
            for (j = 0; j < NCORNER_MSH; j++) {
              psubf = pface->p_subface[j];
              if (psubf != NULL && psubf->p_ele[itype] != NULL) {
                LP_printf(fp, "  %d", MSH_get_ele_id(psubf->p_ele[itype], GIS_MSH));
              }
            }
          }
        } else {
          LP_printf(fp, "  %d", MSH_get_ele_id(pneigh, GIS_MSH));
        } // if p_subf

      } // if pneigh
      count++;
    } // for itype
  }   // for idir
  if (count > 0)
    LP_printf(fp, "\n");
  else
    LP_printf(fp, "None.... Should be ghosts\n");
}

void MSH_print_ele_neigh(s_ele_msh *pele, s_cmsh *pcmsh, FILE *fp) {
  s_id_io *pneigh;
  int count = 0, j;
  int idir, itype;
  LP_printf(fp, "\tNeighbours:");

  count = 0;
  for (idir = 0; idir < ND_MSH; idir++) { // NF 25/5/2015 Z_MSH but should ne ND_MSH
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pneigh = pele->p_neigh[idir][itype];
      if (pneigh != NULL) {
        LP_printf(fp, " %s ", MSH_name_neigh(idir, itype));
        pneigh = IO_browse_id(pneigh, BEGINNING_TS);
        if (idir != Z_MSH) {
          while (pneigh != NULL) {
            LP_printf(fp, "  %d", MSH_get_ele_id(pcmsh->p_layer[pneigh->id_lay]->p_ele[pneigh->id], GIS_MSH));
            pneigh = pneigh->next;
          }
        } else {
          while (pneigh != NULL) {
            LP_printf(fp, "  %d in layer %d", MSH_get_ele_id(pcmsh->p_layer[pneigh->id_lay]->p_ele[pneigh->id], GIS_MSH), pneigh->id_lay);
            pneigh = pneigh->next;
          }
        }
      } // if pneigh

      count++;
    } // for itype
  }   // for idir

  if (count > 0)
    LP_printf(fp, "\n");
  else
    LP_printf(fp, "None.... Should be ghosts\n");
}
/**\fn s_id_io ***MSH_init_neigh_tab()
 *\brief initialize the neighboor tabular
 *
 *\return p_neigh
 */
s_id_io ***MSH_init_neigh_tab() {
  s_id_io ***p_neigh;
  int idir, itype;
  p_neigh = ((s_id_io ***)malloc(ND_MSH * sizeof(s_id_io **)));
  for (idir = 0; idir < ND_MSH; idir++) {
    p_neigh[idir] = ((s_id_io **)calloc(NFACE_MSH, sizeof(s_id_io *)));
    /*
    bzero((char*)p_neigh[idir],NFACE_MSH*sizeof(s_id_io*));

    for(itype=0;itype<NFACE_MSH;itype++)
      {
        p_neigh[idir][itype]=((s_id_io*) malloc(sizeof(s_id_io)));
        p_neigh[idir][itype]=NULL;

      }
    */
  }

  return p_neigh;
}
/**\fn void MSH_tab_neigh(s_ele_msh *pele,FILE *fp)
 *\brief create the neighboor tabular in each cell
 *
 *
 */
void MSH_tab_neigh(s_ele_msh *pele, FILE *fp) {
  s_ele_msh *pneigh;
  int j;
  int idir, itype, nlayer;
  s_face_msh *pface, *psubf = NULL;
  s_id_io *pid_tmp;
  pele->p_neigh = MSH_init_neigh_tab();
  //  LP_printf(fp,"creating tab neighboor for ele %d in layer %d\n",MSH_get_ele_id(pele,GIS_MSH),pele->player->id); //BL to debug BL
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pneigh = MSH_get_neigh_2D(pele, idir, itype);
      if (pneigh != NULL) {
        pface = pneigh->face[idir][MSH_switch_direction(itype, fp)];
        //	      LP_printf(fp,"creating neighboor tab in %s direction\n",MSH_name_neigh(idir,itype)); //to debug BL

        if (pface->p_subface != NULL && (pneigh->type == REFERENCE || pneigh->type == SUBREF)) // BL 27/05/2014 to print all neigh of element of size n next to element of size n-1 (to go down in reference mesh)
        {

          if (idir != Z_MSH) {

            for (j = 0; j < NDIR_MSH; j++) {
              psubf = pface->p_subface[j];
              if (psubf->p_ele[itype] != NULL) {
                pneigh = psubf->p_ele[itype];
                if (pneigh->type != SUBREF && pneigh->type != REFERENCE) {
                  nlayer = pneigh->player->id;
                  // LP_printf(fp,"vois %d : %d ",j,MSH_get_ele_id(psubf->p_ele[itype],INTERN_MSH)); //to debug BL

                  pid_tmp = IO_create_id((nlayer - 1), MSH_get_ele_id(pneigh, INTERN_MSH));
                  pele->p_neigh[idir][itype] = IO_secured_chain_fwd_id(pele->p_neigh[idir][itype], pid_tmp);

                }

                else {
                  pele->type = BOUND;

                  // psubf->p_ele[itype]=MSH_free_ele(psubf->p_ele[itype],fp);
                  //  psubf->p_ele[itype]=MSH_destroy_ele(psubf->p_ele[itype]);
                  // LP_printf(fp,"no neigh created in %s direction\n",MSH_name_neigh(idir,itype)); //BL to debug
                }
              }
            }
            // LP_printf(fp,"\n"); //to debug BL
          } else {
            for (j = 0; j < NCORNER_MSH; j++) {
              psubf = pface->p_subface[j];
              if (psubf != NULL && psubf->p_ele[itype] != NULL) {
                nlayer = psubf->p_ele[itype]->player->id;

                pid_tmp = IO_create_id((nlayer - 1), MSH_get_ele_id(psubf->p_ele[itype], INTERN_MSH));
                pele->p_neigh[idir][itype] = IO_secured_chain_fwd_id(pele->p_neigh[idir][itype], pid_tmp);
                // pft_tmp=TS_free_ts(pft_tmp,fp);
              }
            }
          }
        } else {
          if (pneigh->type != REFERENCE && pneigh->type != SUBREF) {
            nlayer = pneigh->player->id;
            // LP_printf(fp,"vois : %d\n",MSH_get_ele_id(pneigh,INTERN_MSH)); //to debug BL
            // pft_tmp=(s_ft *) malloc(sizeof(s_ft));
            pid_tmp = IO_create_id((nlayer - 1), MSH_get_ele_id(pneigh, INTERN_MSH));
            pele->p_neigh[idir][itype] = IO_secured_chain_fwd_id(pele->p_neigh[idir][itype], pid_tmp);
            // pft_tmp=TS_free_ts(pft_tmp,fp);
          }

          else {
            pele->type = BOUND;
            // pface->p_ele[itype]=MSH_free_ele(pface->p_ele[itype],fp);
            //  pface->p_ele[itype]=MSH_destroy_ele(pface->p_ele[itype]);
            // LP_printf(fp,"no neigh created in %s direction\n",MSH_name_neigh(idir,itype)); //BL to debug
          }

        } // if p_subf
        // obliger de mettre cette condition sinon le browse renvoie des bétises et fait planter. Sans doute du à une mauvaise initialisation du tableau de id p_neigh[][] dans init_tab_neigh. Mais je ne vois pas comment faire autrement si on initialise jusqu'au bout les id on en crée des initiaux à zéro qui n'ont pas lieu d'etre!!
        if (pele->p_neigh[idir][itype] != NULL)
          pele->p_neigh[idir][itype] = IO_browse_id(pele->p_neigh[idir][itype], BEGINNING_TS);
      } // if pneigh
    }   // for itype
  }     // for idir
}

/**\fn int MSH_count_face_neigh(s_ele_msh *pele)
 *\brief count the number of faces between an element and its neighboor
 */
int MSH_count_face_neigh(s_ele_msh *pele) {
  int idir, itype, n_face_neigh;
  s_id_io *neigh;
  n_face_neigh = 0;
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      neigh = pele->p_neigh[idir][itype];

      if (neigh != NULL) {
        n_face_neigh++;
      }
    }
  }
  return n_face_neigh;
}

/**\fn void MSH_list_all_ele(s_cmsh *pcmsh,FILE *fp)
 *\brief print general meh caracteristics
 *
 *print the number of nested cell sizes (nsize in pcmsh), and the number of layers
 */
void MSH_list_all_ele_short(s_cmsh *pcmsh, FILE *fp) {
  int nlay, i;

  s_layer_msh *play;

  nlay = pcmsh->pcount->nlayer;
  LP_printf(fp, "Number of nested cell sizes: %d\n", pcmsh->nsize);
  LP_printf(fp, "\tMesh read with %d layer(s) :\n", nlay);
  for (i = 0; i < nlay; i++) {

    play = pcmsh->p_layer[i];
    LP_printf(fp, "-------------PRINTING NEIGH IN LAYER : %s-------------\n", play->name);
    MSH_list_ele_layer(play, fp);
  }
}

/**\fn void MSH_list_all_ele(s_cmsh *pcmsh,FILE *fp)
 *\brief print general meh caracteristics
 *
 *print the number of nested cell sizes (nsize in pcmsh), and the number of layers
 */
void MSH_list_all_ele(s_cmsh *pcmsh, FILE *fp) {
  int nlay, i;

  s_layer_msh *play;

  nlay = pcmsh->pcount->nlayer;
  LP_printf(fp, "Number of nested cell sizes: %d\n", pcmsh->nsize);
  LP_printf(fp, "\tMesh read with %d layer(s) :\n", nlay);
  for (i = 0; i < nlay; i++) {

    play = pcmsh->p_layer[i];
    LP_printf(fp, "-------------PRINTING NEIGH IN LAYER : %s-------------\n", play->name);
    MSH_list_ele_layer_full(play, fp);
  }
}

/**\fn void MSH_list_all_ele_pneigh(s_cmsh *pcmsh,FILE *fp)
 *\brief print general meh caracteristics
 *
 *print the number of nested cell sizes (nsize in pcmsh), and the number of layers
 */
void MSH_list_all_ele_pneigh(s_cmsh *pcmsh, FILE *fp) {
  int nlay, i;

  s_layer_msh *play;

  nlay = pcmsh->pcount->nlayer;
  LP_printf(fp, "Number of nested cell sizes: %d\n", pcmsh->nsize);
  LP_printf(fp, "\tMesh read with %d layer(s) :\n", nlay);
  for (i = 0; i < nlay; i++) {

    play = pcmsh->p_layer[i];
    LP_printf(fp, "-------------PRINTING NEIGH IN LAYER : %s-------------\n", play->name);
    MSH_list_ele_layer_full_pneigh(play, pcmsh, fp);
  }
}

/**\fn void MSH_tab_all_neigh(s_cmsh *pcmsh,FILE *fp)
 *\brief reate tabular neighboor for all element of model
 *
 *
 */
void MSH_tab_all_neigh(s_cmsh *pcmsh, FILE *fp) {
  int nlay, i;

  s_layer_msh *play;

  nlay = pcmsh->pcount->nlayer;

  for (i = 0; i < nlay; i++) {
    play = pcmsh->p_layer[i];
    MSH_tab_neigh_layer(play, fp);
  }
}

/**\fn int MSH_get_last_gigo(s_ele_msh *pele)
 *\brief get the gigo number of the finest ACTIVE cell
 */
int MSH_get_last_gigo(s_ele_msh *pele) {
  int *code_gigo;
  int gigo;
  int count = 0;
  int n;

  code_gigo = MSH_get_ele_gigogne(pele);
  if (pele->type != ACTIVE)
    gigo = code_gigo[0];
  else {
    n = MSH_get_ele_order(pele);
    while (count < n) {
      gigo = code_gigo[count++];
    }
  }
  return gigo;
}

/**\fn s_ele_msh *MSH_get_neigh_2D(s_ele_msh *pele,int idir,int itype)
 *\brief get the X,Y neighbor in E W N S direction
 */
s_ele_msh *MSH_get_neigh_2D(s_ele_msh *pele, int idir, int itype) { return pele->face[idir][itype]->p_ele[itype]; }

/*void MSH_create_ghost(s_cmsh *pcmsh,FILE *fpout)
{

  int i,j,n,count,idir,itype;
  s_ele_msh *pele,*peleref,*pneigh,*ptmp;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  int nlayer;
  int code_gigo,gigo_neigh;
  int *gigo;
  int id=IDGHOST_MSH;

  nlayer=pcmsh->pcount->nlayer;
  for(i=0;i<nlayer;i++){
    play=pcmsh->p_layer[i];
    for(j=0;j<play->nele;j++){
      pele=play->p_ele[j];
      peleref=pele->pele_ref;
      n=MSH_get_ele_order(pele);
      gigo=MSH_get_ele_gigogne(pele);
      count=0;
      if(gigo[0]>0){
        while(count<n){
          code_gigo = gigo[count];
          ptmp=MSH_get_subele(peleref,MSH_convert_gigogne_dir(code_gigo,fpout));
          for(idir=0;idir<Z_MSH;idir++){
            for(itype=0;itype<NFACE_MSH;itype++){
              pneigh=MSH_get_neigh_2D(pele,idir,itype);
              gigo_neigh=MSH_find_gigo_neigh(code_gigo,idir,fpout);
              if (pneigh==NULL){
                //either a limit (i check it with peleref)
                if (MSH_get_neigh_2D(peleref,idir,itype)!=NULL){
                //or a mesh mistake due to a number of cut>1
                }
                else
                  LP_error(fpout,"In libmesh%4.2f %s l.%d: Pb of mesh consistency with neighbour %s of %s ele %d\n",VERSION_MSH,__FILE__,__LINE__,MSH_name_neigh(idir,itype),MSH_name_cell(pele->type,fpout),MSH_get_ele_id(pele,GIS_MSH);
              }
            }
          }
          count++;
        }

      }
      else{
        gigo_neigh=0;
        for(idir=0;idir<Z_MSH;idir++){
          for(itype=0;itype<NFACE_MSH;itype++){
            pneigh=MSH_get_neigh_2D(pele,idir,itype);
            if (pneigh!=NULL){
              if (pneigh->ndiv>1){
                //on est dans le cas d'un ghost sup

              }
              else {
                pneigh=pneigh->p_subele[0];
                pele->face[idir][itype]->p_ele[itype]=pneigh;
              }

            }
          }//itype
        }//idir
      }
    }
  }
}*/

int MSH_is_ele_empty(s_ele_msh *pele) {

  int answer = YES_MSH;

  if (pele != NULL) {
    if (pele->nele != 0)
      answer = NO_MSH;
  }

  return answer;
}

s_ele_msh *MSH_get_ele_from_id_gis(s_cmsh *pcmsh, int id_lay, int id_gis, FILE *fpout) {

  s_ele_msh *pele;
  s_layer_msh *play;
  int ifound = NO_TS;
  play = pcmsh->p_layer[id_lay];

  if (play != NULL) {
    pele = play->pele;
  } else {
    LP_error(fpout, "no layer %d in the model", id_lay);
  }

  while (ifound = NO_TS) {
    if (pele->id[GIS_MSH] != id_gis) {
      pele = pele->next;
    } else {
      ifound = YES_TS;
    }
  }

  return pele;
}

s_ele_msh *MSH_get_ele(s_cmsh *pcmsh, int id_lay, int intern_id, FILE *fpout) {
  s_ele_msh *pele;
  s_layer_msh *play;
  play = pcmsh->p_layer[id_lay];
  if (play != NULL) {
    pele = play->p_ele[intern_id];
  } else {
    LP_error(fpout, "no layer %d in the model", id_lay);
  }

  if (pele != NULL) {
    return pele;
  }

  else {
    LP_error(fpout, "no element %d in layer %d", intern_id, id_lay);
  }
}

s_ele_msh *MSH_get_ele_from_abs(s_cmsh *pcmsh, int id_abs, FILE *fp) {
  int nb_ele_ini = 0;
  int nb_ele_end = 0;
  int find = NO_MSH;
  int i;
  s_layer_msh *player;
  s_ele_msh *pele;
  player = pcmsh->p_layer[0];
  nb_ele_end = pcmsh->p_layer[0]->nele;
  while (player != NULL) {
    if (id_abs >= nb_ele_ini && id_abs <= nb_ele_end) // NF 11/10/2017 Il manque un <=player->nele au lieu de < stricte car id_abs commence a 1 et non pas 0
      break;
    nb_ele_ini = nb_ele_end;
    player = player->next;
    nb_ele_end += player->nele;
  }
  if (player == NULL)
    LP_error(fp, "libmesh%4.2f %s in %s l%d : the element ID_ABS %d can't be found \n", VERSION_MSH, __func__, __FILE__, __LINE__, id_abs);

  for (i = 0; i < player->nele; i++) {
    pele = player->p_ele[i];

    if (pele->id[ABS_MSH] == id_abs) {
      find = YES_MSH;
      break;
    }
  }
  if (find == YES_MSH)
    return pele;
  else
    LP_error(fp, "libmesh%4.2f %s in %s l%d : the element ID_ABS %d can't be found \n", VERSION_MSH, __func__, __FILE__, __LINE__, id_abs);
}

/* BL cette fonction a été crée suite à la découverte d'un bug sur l'orgeval
les mailles à la frontiere ne sont plus reconnues je ne comprends pas pourquoi
il faudrait regarder les fonctions dans manage_face.c ou le type BOUND est déclaré.
Cette fonction va parcourir l'ensemble des mailles et regarder les voisins.
s'il n'y a pas de voisin le type de l'élément et de la face à travers laquelle on a "regarder" sera fixé à BOUND de plus elle créer des subface là ou elles n'ont pas été créées avant le cas ou un element est voisin d'un seul sous élément !!
*/
// BL attention à faire  avant calcul_on_face !!!
void MSH_check_boundary_ele(s_ele_msh *pele, FILE *fpout) {

  int sf;
  int idir, itype;
  s_face_msh *pface, *psubf;
  s_ele_msh *pneigh;

  for (idir = 0; idir < Z_MSH; idir++) {
    for (itype = ONE; itype < NFACE_MSH; itype++) {
      pface = pele->face[idir][itype];
      if (pface->p_subface != NULL) {
        for (sf = 0; sf < NFACE_MSH; sf++) {
          psubf = pface->p_subface[sf];
          if (psubf != NULL) {
            if (psubf->p_ele[itype] == NULL || (psubf->p_ele[itype]->type == REFERENCE || psubf->p_ele[itype]->type == SUBREF)) // get_2D_neighboor
            {
              psubf->type = BOUND;
              pele->type = BOUND; // BL A vérifier si dans le check des bounds je déscneds bien aux subfaces !!!
            }
          } else // if psubf ==NULL
          {
            /*création de la subface qui n'a pas été crée dans create_ele_face cas d'un element qui a pour voisin un seul element de talle inférieure*/
            psubf = MSH_create_face(CODE_MSH, sf);
            psubf->p_ele[itype] = NULL;
            psubf->p_ele[MSH_switch_direction(itype, fpout)] = pele;
            psubf->type = BOUND;
            pele->type = BOUND; // BL A vérifier si dans le check des bounds je déscneds bien aux subfaces !!!
            pface->nsubface++;
            pface->p_subface[sf] = psubf;
          }
        }

      } else {
        if (pface->p_ele[itype] == NULL || (pface->p_ele[itype]->type == REFERENCE || pface->p_ele[itype]->type == SUBREF)) {

          pface->type = BOUND;
          pele->type = BOUND;
        }
      }
    }
  }
}

s_ele_msh *MSH_free_ele(s_ele_msh *pele, FILE *fpout) {

  int i, j;

  if (pele->pdescr != NULL) {
    LP_printf(fpout, "freeing pdescr\n");
    pele->pdescr = MSH_destroy_descr(pele->pdescr);
  }
  if (pele->pele_ref != NULL) {
    LP_printf(fpout, "freeing ele_ref\n");
    pele->pele_ref = MSH_destroy_ele(pele->pele_ref);
  }
  if (pele->p_subele != NULL) {
    LP_printf(fpout, "freeing sub_eles\n");
    for (i = 0; i < NCORNER_MSH; i++) {
      if (pele->p_subele[i] != NULL) {
        pele->p_subele[i] = MSH_destroy_ele(pele->p_subele[i]);
      }
    }
    free(pele->p_subele);
    pele->p_subele = NULL;
  }

  if (pele->p_neigh != NULL) {
    LP_printf(fpout, "freeing neigh\n");
    for (i = 0; i < ND_MSH; i++) {
      for (j = 0; j < NFACE_MSH; j++) {
        if (pele->p_neigh[i][j] != NULL) {
          pele->p_neigh[i][j] = IO_free_id(pele->p_neigh[i][j]);
        }
      }
      free(pele->p_neigh[i]);
      pele->p_neigh[i] = NULL;
    }
    free(pele->p_neigh);
    pele->p_neigh = NULL;
  }
  free(pele);
  pele = NULL;
  return pele;
}

// NF 23/5/2012 copy an array
int *MSH_copy_gigo(int *gigo, int size) {
  int *cpy;
  int i;

  cpy = ((int *)malloc(size * sizeof(int)));
  for (i = 0; i < size; i++)
    cpy[i] = gigo[i];
  return cpy;
}

/* function to check the vertical connexion between cells */

// NG 10/09/19 : Fonction uniquement appelée dans un cas multicouche
void MSH_check_vertical_connexion(s_cmsh *pcmsh, FILE *fpout) {
  int nlayer, nele;
  int l, e, itype;
  s_layer_msh *player;
  s_ele_msh *pele, *pneigh;
  nlayer = pcmsh->pcount->nlayer;

  for (l = 0; l < nlayer; l++) {
    player = pcmsh->p_layer[l];
    nele = player->nele;
    for (e = 0; e < nele; e++) // AR NG: correction nlayer remplacé par nele
    {
      pele = player->p_ele[e];
      for (itype = 0; itype < NFACE_MSH; itype++) {
        pneigh = pele->face[Z_MSH][itype]->p_ele[itype];
        if (itype == ONE) {
          if (pneigh == NULL && pele->loc != BOT_MSH)
            LP_error(fpout, "in layer %d the neighboor of ele ID_GIS %d ID_INTERN %d ID_ABS %d in position %s can't be found in direction %s type %s", l + 1, pele->id[GIS_MSH], pele->id[INTERN_MSH], pele->id[ABS_MSH], MSH_name_position_cell(pele->loc, fpout), MSH_name_direction(Z_MSH, fpout), MSH_name_type(itype, fpout));

        } else {
          if (pneigh == NULL && (pele->loc != TOP_MSH && pele->loc != RIV_MSH))
            LP_error(fpout, "in layer %d the neighboor of ele ID_GIS %d ID_INTERN %d ID_ABS %d in position %s can't be found in direction %s type %s", l + 1, pele->id[GIS_MSH], pele->id[INTERN_MSH], pele->id[ABS_MSH], MSH_name_position_cell(pele->loc, fpout), MSH_name_direction(Z_MSH, fpout), MSH_name_type(itype, fpout));
        }
      }
    }
  }
}

s_descr_msh *MSH_copy_descr(s_descr_msh *pdescr, FILE *fp) {
  int i;
  s_descr_msh *pcpy;
  pcpy = MSH_create_descr();

  for (i = 0; i < NCORNER_MSH; i++) {
    int j;
    for (j = 0; j < Z_MSH; j++)
      pcpy->coord[i][j] = pdescr->coord[i][j];
  }

  for (i = 0; i < ND_MSH; i++)
    pcpy->center[i] = pdescr->center[i];

  return pcpy;
}

s_ele_msh *MSH_copy_ele(s_ele_msh *pele, s_layer_msh *play, FILE *fp) {
  s_ele_msh *pcpy;

  pcpy = MSH_create_element(pele->id[GIS_MSH], pele->type);
  pcpy->id[INTERN_MSH] = pele->id[INTERN_MSH];
  pcpy->id[ABS_MSH] = pele->id[ABS_MSH];
  pcpy->player = play;
  pcpy->pdescr = MSH_copy_descr(pele->pdescr, fp);
  pcpy->ndiv = pele->ndiv;

  return pcpy;
}
