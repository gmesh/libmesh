/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_simul.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "IO.h"
#include "spa.h"
#include "MSH.h"

/* Function to initialize a simulation structure */
s_simul_msh *init_simul() {
  /* Structure containing all global variables */
  s_simul_msh *psimul;
  int i;

  psimul = new_simul_msh();
  bzero((char *)psimul, sizeof(s_simul_msh));

  psimul->pchronos = CHR_init_chronos();
  psimul->pclock = CHR_init_clock();

  // psimul->pcmsh=MSH_create_carac_glo();//NF 22/12/2015

  return psimul;
}

/* Function to initialize a simulation structure */
s_simul_msh *MSH_init_simul() {
  /* Structure containing all global variables */
  s_simul_msh *psimul;
  int i;

  psimul = new_simul_msh();
  bzero((char *)psimul, sizeof(s_simul_msh));

  psimul->pchronos = CHR_init_chronos();
  psimul->pclock = CHR_init_clock();

  // psimul->pcmsh=MSH_create_carac_glo();//NF 22/12/2015

  return psimul;
}
