/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: direction.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

int MSH_convert_gigogne_dir(int type, FILE *fp) {
  int c;

  switch (type) {
  case 1: {
    c = NW;
    break;
  }
  case 2: {
    c = NE;
    break;
  }
  case 3: {
    c = SW;
    break;
  }
  case 4: {
    c = SE;
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: %s l.%d Gigogne code Out of scope in MSH_convert_gigogne_dir(),itype %d\n", VERSION_MSH, __FILE__, __LINE__, type);
  }
  }

  return c;
}

int MSH_switch_direction(int direction, FILE *fp) { // NF 18/12/2017 I use abusively this function for type (ONE/TWO)
  int i;
  char *error;
  switch (direction) {
  case X_MSH:
    i = Y_MSH;
    break;
  case Y_MSH:
    i = X_MSH;
    break;
  default: // No switch if it's other direction as CODE
    error = MSH_name_direction(i, fp);
    break;
  }
  return i;
}

int MSH_switch_type(int type, FILE *fp) {
  int i;
  char *error;
  switch (type) {
  case ONE_MSH:
    i = TWO_MSH;
    break;
  case TWO_MSH:
    i = ONE_MSH;
    break;
  default: // No switch if it's other direction as CODE
    error = MSH_name_direction(i, fp);
    break;
  }
  return i;
}
