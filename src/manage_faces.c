/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_faces.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

/**\fn s_face_msh *MSH_create_face(int id,int type)
 *\brief create face id, itype (X,Y)
 *
 */
s_face_msh *MSH_create_face(int id, int type) {
  s_face_msh *pface;
  int k;

  pface = new_face_msh();
  bzero((char *)pface, sizeof(s_face_msh));
  pface->id = id;
  pface->kindof = type;
  pface->p_ele = (s_ele_msh **)malloc(Z_MSH * sizeof(s_ele_msh *));
  bzero((char *)pface->p_ele, Z_MSH * sizeof(s_ele_msh *));
  pface->p_descr = (s_descr_msh **)malloc(NFACE_MSH * sizeof(s_descr_msh *));
  bzero((char *)pface->p_descr, NFACE_MSH * sizeof(s_descr_msh *));
  for (k = 0; k < NFACE_MSH; k++)
    pface->p_ele[k] = NULL;
  pface->p_subface = NULL;
#ifdef COUPLED
  pface->phydro = NULL;
#endif
  return pface;
}

s_face_msh *MSH_free_face(s_face_msh *pface) {

  int e;
  if (pface->p_ele != NULL)
    free(pface->p_ele);
  if (pface->p_descr != NULL)
    free(pface->p_descr);
  free(pface);
  pface = NULL;
  return pface;
}

/**\fn s_face_msh ****MSH_define_ref_faces(int dim[Z_MSH],s_cmsh *pcmsh,FILE *fpout)
 *\brief create p_face
 *
 *create X,Y faces for all elements (faces(i,j) i in dim[Y]+1, j in dim[X]+1)
 * (dim[Y]+1)*(dim[X]+1) faces are created in each directions to link all elements togethers (for a mesh dim[Y]*dim[X])
 *In reality too much faces are created only (dim[Y]+1)*dim[X] faces are needed in the Y direction
 * and dim[Y]*(dim[X]+1) faces are needed in the X direction
 * p_faces[i][j][z] (z in X Y Z) ie (ex : p_faces[i][j][0] face[i][j] in X direction)
 */
s_face_msh *****MSH_define_ref_faces(int dim[Z_MSH], s_cmsh *pcmsh, FILE *fpout) {

  s_face_msh *****p_faces;
  int i, j, k = 0, layer;
  int m, nlayer;
  int counter = 0;
  nlayer = pcmsh->pcount->nlayer;
  p_faces = ((s_face_msh *****)malloc(nlayer * sizeof(s_face_msh ****)));
  bzero((char *)p_faces, sizeof(s_face_msh ****));
  for (layer = 0; layer < nlayer; layer++) {
    // counter=0;
    p_faces[layer] = ((s_face_msh ****)malloc((dim[Y] + 1) * sizeof(s_face_msh ***)));
    bzero((char *)p_faces[layer], sizeof(s_face_msh ***));

    for (i = 0; i < dim[Y] + 1; i++) {
      p_faces[layer][i] = ((s_face_msh ***)malloc((dim[X_MSH] + 1) * sizeof(s_face_msh **)));
      bzero((char *)p_faces[layer][i], sizeof(s_face_msh **));
      for (j = 0; j < dim[X_MSH] + 1; j++) {
        p_faces[layer][i][j] = ((s_face_msh **)malloc(ND_MSH * sizeof(s_face_msh *))); // BL init Z
        for (k = 0; k < Z_MSH; k++)                                                    // BL init Z
        {
          p_faces[layer][i][j][k] = MSH_create_face(++counter, k);
        }
      }
    }
  }
  LP_printf(fpout, "In libmesh%4.2f: Reference faces created %d*%d\n", VERSION_MSH, dim[Y] + 1, dim[X_MSH] + 1);
  return p_faces;
}
/**\fn s_face_msh *MSH_get_faceref(int gigo,int itype,int idir,s_ele_msh *pele,FILE *fpout)
 *\brief get face ref
 *
 */
s_face_msh *MSH_get_faceref(int gigo, int itype, int idir, s_ele_msh *pele, FILE *fpout) {
  s_face_msh *pface;
  pface = NULL;
  switch (itype) {
  case X_MSH:
    switch (idir) {
    case (ONE):
      if ((gigo == 1) || (gigo == 3))
        pface = pele->face[X_MSH][ONE];
      break;
    case (TWO):
      if ((gigo == 2) || (gigo == 4))
        pface = pele->face[X_MSH][TWO];
      break;
    }
    break;
  case Y:
    switch (idir) {
    case (ONE):
      if ((gigo == 3) || (gigo == 4))
        pface = pele->face[Y][ONE];
      break;
    case (TWO):
      if ((gigo == 1) || (gigo == 2))
        pface = pele->face[Y][TWO];
      break;
    }
    break;
  }
  return pface;
}

/**\fn s_face_msh *MSH_get_faceref(int gigo,int itype,int idir,s_ele_msh *pele,FILE *fpout)
 *\brief check if face is ref face
 *
 */
int MSH_is_ref_face(int gigo, int itype, int idir, s_ele_msh *pele, FILE *fpout) {
  int answer = NO_MSH;
  switch (itype) {
  case X_MSH:
    switch (idir) {
    case (ONE):
      if ((gigo == 1) || (gigo == 3))
        answer = YES_MSH;
      break;
    case (TWO):
      if ((gigo == 2) || (gigo == 4))
        answer = YES_MSH;
      break;
    }
    break;
  case Y:
    switch (idir) {
    case (ONE):
      if ((gigo == 3) || (gigo == 4))
        answer = YES_MSH;
      break;
    case (TWO):
      if ((gigo == 1) || (gigo == 2))
        answer = YES_MSH;
      break;
    }
    break;
  }
  return answer;
}
/**\fn int MSH_subface_id(int idir,int icorner)
 *\brief get subface id by direction and corner
 *
 */
int MSH_subface_id(int idir, int icorner) {
  int id = CODE_TS;

  switch (idir) {
  case (X_MSH):
    switch (icorner) {
    case (NW):
      id = UP_MSH;
      break;
    case (NE):
      id = UP_MSH;
      break;
    case (SW):
      id = DOWN_MSH;
      break;
    case (SE):
      id = DOWN_MSH;
      break;
    }
    break;
  case (Y):
    switch (icorner) {
    case (NW):
      id = RIGHT_MSH;
      break;
    case (SW):
      id = RIGHT_MSH;
      break;
    case (NE):
      id = LEFT_MSH;
      break;
    case (SE):
      id = LEFT_MSH;
      break;
    }
    break;
  }
  return id;
}
/**\fn int MSH_reflect_corner(int idir,int icorner)
 *\brief get next face throught face idir
 *
 */

int MSH_reflect_corner(int idir, int icorner) {
  int ineigh;

  switch (idir) {
  case (Y):
    switch (icorner) {
    case (NW):
      ineigh = SW;
      break;
    case (NE):
      ineigh = SE;
      break;
    case (SW):
      ineigh = NW;
      break;
    case (SE):
      ineigh = NE;
      break;
    }
    break;
  case (X_MSH):
    switch (icorner) {
    case (NW):
      ineigh = NE;
      break;
    case (SW):
      ineigh = SE;
      break;
    case (NE):
      ineigh = NW;
      break;
    case (SE):
      ineigh = SW;
      break;
    }
    break;
  }
  return ineigh;
}
/**\fn void MSH_create_subele_ref_face(s_ele_msh *pup,int iorder,int *idf,FILE *fpout)
 *\brief create sub element faces in reference mesh
 *
 */
void MSH_create_subele_ref_face(s_ele_msh *pup, int iorder, int *idf, FILE *fpout) {
  int n = 0, j, idir, itype;
  s_ele_msh *pele = NULL, *pneigh = NULL;
  s_face_msh *pface = NULL;
  int id;

  id = *idf;
  if (pup != NULL) {
    // LP_printf(fpout,"in  MSH_create_subele_ref_face getting ele_ref id %d , status : %s \n",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout)); //BL to debug

    pele = MSH_get_subele(pup, 0);

    if (pele != NULL)
      // LP_printf(fpout," creating face of ele id %d , status : %s \n",MSH_get_ele_id(pele,GIS_MSH),MSH_name_cell(pele->type,fpout));
      //*/ // BL to debug
      for (idir = 0; idir < Z_MSH; idir++) // BL init Z_MSH
      {
        for (itype = 0; itype < NFACE_MSH; itype++) // for itype in ONE TWO
        {
          pneigh = MSH_get_neigh_2D(pup, idir, itype);

          if (pneigh != NULL && MSH_is_ele_empty(pneigh) == NO_MSH) // init

          {
            if (pneigh->ndiv == 1) {
              pneigh = MSH_get_subele(pneigh, 0);
            }
            pface = pneigh->face[idir][MSH_switch_direction(itype, fpout)]; // get face  face[idir][itype] linking element and neight here switch itype because neigh have been selected

          } else {
            if (pneigh == NULL)
              pface = pup->face[idir][itype];
            else {
              pface = pneigh->face[idir][MSH_switch_direction(itype, fpout)];
              pface->p_ele[itype] = NULL;
            }
            /* commenter défini les boundaries à la fin
            if(pele->type !=REFERENCE && pele->type != SUBREF)
              {
                pele->type=BOUND;
                pface->type=BOUND;
              }
            */
          } // if pneigh
          if (pface == NULL) {
            pface = MSH_create_face(++id, idir);
            *idf = id;
          } // if pface
          pface->p_ele[MSH_switch_direction(itype, fpout)] = pele;
          pele->face[idir][itype] = pface;

        } // for itype
      }   // for idir
  }       // if pup
}

/**\fn void MSH_create_subele_face(s_ele_msh *pup,int iorder,int *idf,FILE *fpout)
 *\brief create sub element faces and attributes the pointers to element through the subfaces
 *
 */
void MSH_create_subele_face(s_ele_msh *pup, int iorder, int *idf, FILE *fpout) {
  int n = 0, icorner, idir, itype;
  s_ele_msh *pele = NULL, *pneigh = NULL, *p_tmp = NULL;
  s_face_msh *pface = NULL, *psubf = NULL;
  int id;
  int gigo, icompass;
  int check_is_neigh_in_pup;
  int debug = 0;
  id = *idf;

  if (pup != NULL) {
    // LP_printf(fpout,"in MSH_create_subele_face  getting sub_ref id %d , status : %s \n",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout)); //BL to debug
    for (icorner = 0; icorner < NCORNER_MSH; icorner++) {
      pele = MSH_get_subele(pup, icorner);
      // BL to debug

      if (pele != NULL) {

        if (n < pup->nele) {
          /* //BL to debug
          //if(pele->id[ABS_MSH]==1127)
          if(pele->id[INTERN_MSH]==1001493 || pele->id[INTERN_MSH]==1001649)
            {
              LP_printf(fpout,"DEBUG for corner : %d in ele_ref direction %s\n",icorner,MSH_name_compass_dir(icorner,fpout));

              debug=1;

            }
          LP_printf(fpout,"in MSH_create_subele_face Creating faces of %s ele %d of %s ele %d\n",MSH_name_cell(pele->type,fpout),pele->id[INTERN_MSH],MSH_name_cell(pup->type,fpout),pup->id[INTERN_MSH]); //BL to debug
          */
          gigo = MSH_get_last_gigo(pele); // BL 270514

          for (idir = 0; idir < Z_MSH; idir++) // BL init Z_MSH
          {
            for (itype = 0; itype < NFACE_MSH; itype++) {

              // NF 22/5/2015 Explanation: Step 1 of the algorithm = get the concerned subface. If does not exist, the subface is allocated and initialised to NULL

              check_is_neigh_in_pup = MSH_is_neigh_in_up(gigo, idir, itype, fpout);

              if (check_is_neigh_in_pup == NO_MSH) {

                pneigh = MSH_get_neigh_2D(pup, idir, itype);

                if (pneigh != NULL && MSH_is_ele_empty(pneigh) == NO_MSH) { // init
                  /*
                   if(debug==1)
                    {
                      LP_printf(fpout,"ele Neigh in the neighboor ele_ref \n");
                    }
                  if(debug==1)
                    {
                      LP_printf(fpout,"getting REF neigh of ele_ref %d type %s\n Neigh is ele_ref %d type %s\n",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout),MSH_get_ele_id(pneigh,GIS_MSH),MSH_name_cell(pneigh->type,fpout));

                    }
                  */
                  pface = pneigh->face[idir][MSH_switch_direction(itype, fpout)];
                  if (pface->p_subface == NULL) {
                    // pface->p_subface=(s_face_msh **) calloc(NDIR_MSH,sizeof(s_face_msh*));
                    pface->p_subface = (s_face_msh **)malloc(NDIR_MSH * sizeof(s_face_msh *)); // BL remplace calloc par malloc car a plante chez laurianne
                    bzero((char *)pface->p_subface, NDIR_MSH * sizeof(s_face_msh *));
                  }
                  psubf = pface->p_subface[MSH_subface_id(idir, MSH_reflect_corner(idir, icorner))];
                } else {

                  pface = pup->face[idir][itype];
                  /* //BL to debug
                  if(debug==1)
                    {
                      LP_printf(fpout,"Neigh of ele %d type %s in direction %s %s is null",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout),MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
                    }
                  */
                  if (pface->p_subface == NULL) {
                    // pface->p_subface=(s_face_msh **) calloc(NDIR_MSH,sizeof(s_face_msh*)); // BL  init
                    pface->p_subface = (s_face_msh **)malloc(NDIR_MSH * sizeof(s_face_msh *)); // BL remplace calloc par malloc car a plante chez laurianne
                    bzero((char *)pface->p_subface, NDIR_MSH * sizeof(s_face_msh *));
                  }
                  psubf = pface->p_subface[MSH_subface_id(idir, icorner)];
                } // if pneigh

                if (psubf == NULL) {
                  pface->nsubface++;
                  psubf = MSH_create_face(++id, idir);
                  /*
                  if(psubf->id==1002493)
                    LP_printf(stderr,"libmesh%4.2f: DEBUG %s l%d psubface %d\n",VERSION_MSH,__FILE__,__LINE__,psubf->id);
                  */ //BL to debug
                  *idf = id;
                  if (pneigh != NULL && MSH_is_ele_empty(pneigh) == NO_MSH) { // pface est celle du voisin du coup il faut revenir en arrière pour faire pointer psubf

                    // NF 22/5/2015 Je ne pense pas qu'il faille faire un lien. La philisophie de cette routine est qu'une face d'un element pointe vers l'element qui cherche sa subface. Comme tout est initialise a NULL alors si un element existe la face ou subface pointera vers lui. Il est vrai qu'a la fin de cette function une subface ne pointera jamais vers un element de plus grande taille. Il
                    // faut donc ecrire une seconde function pour gerer ce cas NF 22/5/2015 Par consequent les trois if suivant me paraissent etre des bugs

                    // if(pneigh->type == REFERENCE )
                    //{psubf->p_ele[itype]=MSH_get_subele(pneigh,0);} //BL 27/05/2014 pour que l'element pointe vers le voisin de taille différente.//NF 22/05/2015 ici icompass=0 car pneigh est de type REFERENCE. Par contre, il faut verifier que nele >0, sinon on rajourte le pb des bondary
                    // if (pneigh->type==SUBREF && (pneigh->nele == 1 && pneigh->p_subele == NULL))
                    //{psubf->p_ele[itype]=MSH_get_subele(pneigh,0);} //BL 27/05/2014 pour que l'element pointe vers le voisin de taille différente.//NF 22/05/2015 pourquoi icompass=0? Cela me parait louche
                    // if(pneigh->ndiv > 2 && (pneigh->type != SUBREF && pneigh->type != REFERENCE))
                    //{psubf->p_ele[itype]=pneigh;} //BL 27/05/2014 pour que l'element pointe vers le voisin de taille différente.
                    pface->p_subface[MSH_subface_id(idir, MSH_reflect_corner(idir, icorner))] = psubf;

                  } else // ok on a tout créé
                  {
                    /*
                    if(debug==1)
                      {
                        LP_printf(fpout,"Neigh of ele %d type %s in direction %s %s is null\n",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout),MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
                      }
                    */ //BL to debug
                    pface->p_subface[MSH_subface_id(idir, icorner)] = psubf; // init
                    // pface->p_subface[MSH_subface_id(idir,j)]=NULL;
                  } // if pneigh
                }   // if psubf

                psubf->p_ele[MSH_switch_direction(itype, fpout)] = pele;
                pele->face[idir][itype] = psubf;
                /*
                if(debug==1)
                  {
                    if(psubf->p_ele[itype] !=NULL)
                      {
                      LP_printf(fpout,"Linking ele %d status %s with ele %d status %s through face %s %s\n",MSH_get_ele_id(pele,GIS_MSH),MSH_name_cell(pele->type,fpout),MSH_get_ele_id(psubf->p_ele[itype],GIS_MSH),MSH_name_cell(psubf->p_ele[itype]->type,fpout),MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
                      }
                    else
                      {
                        LP_printf(fpout,"sub_face %d  in dir %s %s of ele %d status %s  is not linked yet \n",MSH_subface_id(idir,icorner),MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout),MSH_get_ele_id(pele,GIS_MSH),MSH_name_cell(pele->type,fpout));
                      }

                      }*/ //BL to debug
              } else { // if check_is_neigh_in_pup

                icompass = MSH_find_dir_neigh(gigo, idir, fpout);
                pneigh = MSH_get_subele(pup, icompass);
                /*
                if(debug==1)
                      {
                        LP_printf(fpout,"Neigh in the same ele_ref %d status %s \n",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout));
                        LP_printf(fpout,"Getting sub_ele in dir %d %s \n",icompass,MSH_name_compass_dir(icompass,fpout));
                      }
                */ //BL to debug
                // if(pneigh!=NULL &&  (pneigh->type!=REFERENCE ||pneigh->type!=SUBREF )){
                if (pneigh != NULL && MSH_is_ele_empty(pneigh) == NO_MSH) { // init
                  /*
                  if(debug==1)
                    {
                      LP_printf(fpout,"getting REF neigh of ele %d type %s\n Neigh is ele_ref %d type %s\n",MSH_get_ele_id(pup,GIS_MSH),MSH_name_cell(pup->type,fpout),MSH_get_ele_id(pneigh,GIS_MSH),MSH_name_cell(pneigh->type,fpout));
                    }
                  */ // BL to debug
                  pface = pneigh->face[idir][MSH_switch_direction(itype, fpout)];
                  if (pface == NULL)
                    pface = MSH_create_face(++id, idir);
                } else {
                  pface = MSH_create_face(++id, idir);
                }
                pface->p_ele[MSH_switch_direction(itype, fpout)] = pele;
                pele->face[idir][itype] = pface;
                /*
                if(debug==1)
                  {
                    if(pface->p_ele[itype]!=NULL)
                      {
                        LP_printf(fpout,"Linking ele %d status %s with ele %d status %s through face %s %s\n",MSH_get_ele_id(pele,GIS_MSH),MSH_name_cell(pele->type,fpout),MSH_get_ele_id(pface->p_ele[itype],GIS_MSH),MSH_name_cell(pface->p_ele[itype]->type,fpout),MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
                      }
                    else
                      {
                        LP_printf(fpout,"face  %s %s of ele %d status %s  is not linked yet \n",MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout),MSH_get_ele_id(pele,GIS_MSH),MSH_name_cell(pele->type,fpout));

                      }

                      }*/ //BL to debug
              } // if check_is_neigh_in_pup
            }   // for itype
          }     // for itype
        }       // if n
        else
          LP_error(fpout, "In libmesh%4.2f %s l.%d Problem to link subelements %d: too many subelements contained in element %d", VERSION_MSH, __FILE__, __LINE__, pele->id, pup->id);
        n += pele->nele;

      } // if pele
    }   // for j
  }     // if pup
}

/**\fn void MSH_create_ele_faces(s_cmsh *pcmsh,FILE *fpout)
 *\brief create element faces
 *
 */
void MSH_create_ele_faces(s_cmsh *pcmsh, FILE *fpout) {
  int i, j, kk, idim;
  int idir, itype, isub, icorner, layer;
  int nlayer;
  s_layer_msh *play, *play2;
  s_face_msh *pface, *pface2;
  s_ele_msh *psub, *peleref, *pneigh, *pup, *pele;
  int n, nele, neleref;
  int *gigo;
  int nsub = 0;
  int ndim;
  s_ele_msh **p_sub;
  s_ele_msh **p_up;
  int powup;
  int powsub;
  int idface = IDSUBFACE_MSH;

  /*Il faut le faire ordre par ordre et donc creer des tables de subelement psub_faces*/

  ndim = pcmsh->nsize;

  pup = NULL;
  nlayer = pcmsh->pcount->nlayer;

  for (layer = 0; layer < nlayer; layer++) {
    play = pcmsh->play_ref[layer];
    neleref = play->nele;
    for (idim = 1; idim < ndim; idim++) {
      LP_printf(fpout, "libmesh%4.2f: Creating subelements' faces for mesh nested order %d\n", VERSION_MSH, idim);

      for (i = 0; i < neleref; i++) {
        peleref = play->p_ele[i];
        pup = peleref;

        nsub = peleref->ndiv;
        if (nsub > 1) { // else nothing to do because we are at order > ref mesh// equivalent to (MSH_is_ele_a_ref(pele)==NO_TS) mais pour cela il faudrait pele!

          j = 0;
          // je cree un tableau de pointeurs vers le nb theorique d'element a l'ordre idim-1
          powup = (int)pow(NCORNER_MSH, j);
          p_up = (s_ele_msh **)malloc(powup * sizeof(s_ele_msh *));
          p_up[j] = pup;

          while (j < idim - 1) {
            // je cree un tableau de pointeurs vers le nb theorique d'element a l'ordre idim
            powsub = (int)pow(NCORNER_MSH, (j + 1));
            p_sub = (s_ele_msh **)malloc(powsub * sizeof(s_ele_msh *));

            for (isub = 0; isub < powup; isub++) {
              pup = p_up[isub];
              if (pup != NULL) {
                for (icorner = 0; icorner < NCORNER_MSH; icorner++)
                  p_sub[NCORNER_MSH * isub + icorner] = MSH_get_subele(pup, icorner);
              } else {
                for (kk = 0; kk < NCORNER_MSH; kk++)
                  p_sub[NCORNER_MSH * isub + kk] = NULL;
              } // if pup
            }   // for isub
            free(p_up);
            powup = powsub;
            p_up = (s_ele_msh **)malloc(powup * sizeof(s_ele_msh *));
            for (isub = 0; isub < powup; isub++)
              p_up[isub] = p_sub[isub];
            free(p_sub);
            j++;
          } // while j

          for (isub = 0; isub < powup; isub++) {
            pup = p_up[isub];
            if (pup != NULL)
              MSH_create_subele_face(pup, idim, &idface, fpout);
          } // for isub
          free(p_up);
        } // if nsub

      } // for isub
    }   // for idim

    // maintenant il faut gerer les elements qui sont des ref
    idim = 0;
    LP_printf(fpout, "libmesh%4.2f: Creating subelements' faces for mesh nested order %d\n", VERSION_MSH, idim);
    for (i = 0; i < neleref; i++) {
      peleref = play->p_ele[i];
      pup = peleref;
      nsub = peleref->ndiv;

      if (nsub == 1) {
        MSH_create_subele_ref_face(pup, idim, &idface, fpout);
      } // if nsub
    }   // for i

    // NF 23/5/2015 At the end of the loop faces and subfaces are created but there is still a pb with subfaces connecting a small and a larger element. The pointer towards the small element exists but not the one towards the larger one. It needs to be corrected
    play2 = pcmsh->p_layer[layer];

    // LP_printf(fpout,"libmesh%4.2f: %s l%d Now correcting pointer of subfaces towards larger elements of layer %s\n",VERSION_MSH,__FILE__,__LINE__,play2->name);
    pele = play2->pele;
    while (pele != NULL) {
      for (idir = X_MSH; idir < Z_MSH; idir++) {
        for (itype = ONE_MSH; itype < NFACE_MSH; itype++) {
          pface = pele->face[idir][itype];
          if (pface->p_subface != NULL) {
            for (j = ONE_MSH; j < NFACE_MSH; j++) {
              pface2 = pface->p_subface[j];
              if (pface2 != NULL) {
                pface2->p_ele[MSH_switch_direction(itype, fpout)] = pele;
                // LP_printf(fpout,"\tlibmesh%4.2f: %s l%d Pointer of subface %d now pointing towards ele %d in dir %s%s\n",VERSION_MSH,__FILE__,__LINE__,pface2->id,pele->id[GIS_MSH],MSH_name_direction(itype,fpout),MSH_name_type(itype,fpout)); //BL to debug
              }
            }
          }
        }
      }
      pele = pele->next;
    }

  } // for layer
}

void MSH_create_vertical_faces(s_cmsh *pcmsh, FILE *fpout) {
  int nlayer, layer, i, j, n;
  int layer_vert;
  int *gigo, *gigo_vert;
  int *gigo_tmp; // NF 23/5/2015 Correction d'un bug majeur concernant les la detection des voisins verticaux pour les mailles correspondant a des elements de reference. On a besoin de creer une copie des gigo et de remplacer le gigo[0] si l'element qui appel est de niveau de decoupage 2 situe ailleurs qu'au NO dans l'element de reference. Alors il faut imposer gigo[0]=0
  int nele_in_ref, id_ref, dir;
  int code_gigo, norder, norder_vert;
  int itype, corner;
  int count;
  // int create_subface;
  int id = IDSUBFACE_MSH;
  s_ele_msh *pele, *peleref;
  s_ele_msh *peleref_vert, *pneigh, *ptmp;
  s_face_msh *pface, *psubf;
  s_layer_msh *play;
  int nele;
  int counter = 2 * pcmsh->pcount->nele;

  LP_printf(fpout, "creating vertical faces\n");
  nlayer = pcmsh->pcount->nlayer;

  for (layer = 0; layer < nlayer; layer++) {
    play = pcmsh->p_layer[layer];

    //  LP_printf(fpout,"libmesh%4.2f: %s l/%d Connecting layer %s vertically:\n",VERSION_MSH,__FILE__,__LINE__,play->name); //BL to debug
    for (i = 0; i < play->nele; i++) {

      pele = play->p_ele[i];

      gigo_tmp = MSH_get_ele_gigogne(pele);

      // if((MSH_get_ele_id(pele,ABS_MSH)==1817 || MSH_get_ele_id(pele,ABS_MSH)==4626) || MSH_get_ele_id(pele,ABS_MSH)==1 )
      //  LP_printf(fpout,"apoub\n");
      //  LP_printf(fpout,"\tcreating vertical faces of ele %d/%d : GIS_ID : %d, INTER_ID ; %d ABS_ID : %d in %s\n",i,play->nele,MSH_get_ele_id(pele,GIS_MSH),MSH_get_ele_id(pele,INTERN_MSH),MSH_get_ele_id(pele,ABS_MSH),play->name); //BL to debug
      peleref = pele->pele_ref;
      nele_in_ref = peleref->nele;
      id_ref = MSH_get_ele_id(peleref, INTERN_MSH);
      for (itype = 0; itype < Z_MSH; itype++) {
        norder = MSH_get_ele_order(pele);
        layer_vert = MSH_get_verti_layer(pcmsh, pele, layer, itype, fpout);

        pface = pele->face[Z_MSH][itype];

        if (layer_vert != CODE_MSH) {
          // create_subface=NO;
          peleref_vert = pcmsh->play_ref[layer_vert]->p_ele[id_ref];
          //		  LP_printf(fpout,"getting ele_ref %d in layer %d\n",MSH_get_ele_id(peleref_vert,INTERN_MSH),layer_vert);

          // NF 23/5/2015 Correction of the first gigo for the case of the targeted element is a reference element and the calling element is smaller with a gigo different than NO then gigo[0]=0
          gigo = MSH_copy_gigo(gigo_tmp, MSH_get_ele_order(pele));
          n = 0;
          ptmp = MSH_get_subele(peleref_vert, 0);
          if (ptmp != NULL) {
            if (MSH_is_ele_a_ref(ptmp, pcmsh) == YES_MSH)
              gigo[0] = 0;
          }

          if (gigo[n] == 0 && peleref_vert->nele == 1) // si l'élément vertical est de meme taille qu'un ele ref
          {
            pneigh = MSH_get_subele(peleref_vert, 0);
            // LP_printf(fpout,"link ele : %d status : %s with ele : %d status %s in layer %d\n",MSH_get_ele_id(pele,GIS_MSH),MSH_name_cell(pele->type,fpout),MSH_get_ele_id(pneigh,GIS_MSH),MSH_name_cell(pneigh->type,fpout),layer_vert);
          } else {
            if (gigo[n] != 0) {

              pneigh = peleref_vert;
              while (n < norder && pneigh != NULL) {
                ptmp = pneigh;

                dir = MSH_convert_gigogne_dir(gigo[n], fpout);
                pneigh = MSH_get_subele(pneigh, dir);
                n++;
              }
              if (pneigh == NULL) // on est allé trop en avant ordre de pele > à ordre de pele_vert
                pneigh = ptmp;
              if (pneigh->nele > 1 && pneigh->p_subele != NULL) // il existe des subele ordre de pele < à ordre de pele_vert
              {
                //	       LP_printf(fpout,"creating subfaces in while\n");

                pface = MSH_create_face(counter, Z_MSH);
                pface->p_subface = MSH_create_subfaces_verti(pneigh, pele, itype, id, fpout);
                // create_subface=YES;
                for (corner = 0; corner < NCORNER_MSH; corner++) {
                  // psubf=pface->p_subface[corner];
                  //  LP_printf(fpout,"ele : %d in layer %d \n",MSH_get_ele_id(psubf->p_ele[itype],GIS_MSH),layer_vert);
                  id++;
                }
              }
              // if(pneigh->nele==1 && pneigh->p_subele!=NULL)
              //  pneigh=MSH_get_subele(pneigh,0); //logiquement il suffit de récuperer l'élément actif dans l'élé ref
            } // if gigo

            else // si l'élément regardé est de taille d'un élé ref et qu'il y a plus d'un élément dans peleref
            {
              dir = 0;
              pneigh = peleref_vert;
              // LP_printf(fpout,"creating subfaces\n");

              pface = MSH_create_face(counter, Z_MSH);
              pface->p_subface = MSH_create_subfaces_verti(pneigh, pele, itype, id, fpout);
              // LP_printf(fpout,"linking subfaces of ele %d with : \n",MSH_get_ele_id(pele,GIS_MSH));
              //	  create_subface=YES;
              if (pface->p_subface != NULL) {
                for (corner = 0; corner < NCORNER_MSH; corner++) {
                  psubf = pface->p_subface[corner];
                  //	  LP_printf(fpout,"ele : %d in layer %d \n",MSH_get_ele_id(psubf->p_ele[itype],GIS_MSH),layer_vert);
                  id++;
                }
              }

            } // else gigo
          }

          if (pface == NULL)
            pface = MSH_create_face(counter, Z_MSH);
          if (pneigh != NULL) {
            if (pneigh->face[Z_MSH][MSH_switch_direction(itype, fpout)] == NULL)
              pneigh->face[Z_MSH][MSH_switch_direction(itype, fpout)] = pface;
            pface->p_ele[itype] = pneigh;
          } else
            LP_error(fpout, "libmesh%4.2f %s in %s l.%d: Impossible to find neighbor for ele %d (GIS_MSH) of layer %s  in direction %s number %s. It may be a problem of vertical link between a cell of size S with an incomplete set of upper or lower cells of size S/4\n", VERSION_MSH, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, GIS_MSH), pele->player->name, MSH_name_direction(Z_MSH, fpout),
                     MSH_name_type(MSH_switch_direction(itype, fpout), fpout));

          free(gigo);
        }                  // if pele_vert
        if (pface == NULL) // BL on est dans le cas ou il n'y a pas de voisins
          pface = MSH_create_face(counter, Z_MSH);
        pface->p_ele[MSH_switch_direction(itype, fpout)] = pele;
        pele->face[Z_MSH][itype] = pface;

        if (layer_vert == CODE_MSH) {
          if (itype == ONE) {
            pele->face[Z_MSH][itype]->loc = BOT_MSH;
            pele->loc = BOT_MSH;
          } else {
            pele->face[Z_MSH][itype]->loc = TOP_MSH;
            pele->loc = TOP_MSH;
          }
        }

      } // for itype
    }   // for i

    counter++;
  } // for layer

  // LP_error(fpout,"TO DEBUG");
  LP_printf(fpout, "done\n");
}

s_face_msh **MSH_create_subfaces_verti(s_ele_msh *pneigh, s_ele_msh *pele, int itype, int id, FILE *fpout) {

  s_face_msh **psubf;
  s_ele_msh *ptmp;
  int i;

  psubf = (s_face_msh **)malloc(NCORNER_MSH * sizeof(s_face_msh *));
  bzero((char *)psubf, NCORNER_MSH * sizeof(s_face_msh *));

  for (i = 0; i < NCORNER_MSH; i++) {

    ptmp = MSH_get_subele(pneigh, i);
    if (ptmp != NULL) {
      if (ptmp->face[Z_MSH][MSH_switch_direction(itype, fpout)] == NULL) {
        psubf[i] = MSH_create_face(id, Z_MSH);
        psubf[i]->p_ele[itype] = ptmp;
        psubf[i]->p_ele[MSH_switch_direction(itype, fpout)] = pele;
        ptmp->face[Z_MSH][MSH_switch_direction(itype, fpout)] = psubf[i];
      } else {
        psubf[i] = ptmp->face[Z_MSH][MSH_switch_direction(itype, fpout)];
        if (psubf[i]->p_ele[itype] == NULL)
          psubf[i]->p_ele[itype] = ptmp;
        psubf[i]->p_ele[MSH_switch_direction(itype, fpout)] = pele;
      }

    } else {
      LP_warning(fpout, "Warning no subelement in corner %s \n", MSH_name_compass_dir(i, fpout));
      psubf[i] = MSH_create_face(id, Z_MSH);
      psubf[i]->p_ele[itype] = ptmp;
      psubf[i]->p_ele[MSH_switch_direction(itype, fpout)] = pele;
    }
    id++;
  }

  return psubf;
}

int MSH_dir_by_card(int icard) {
  int idir;
  switch (icard) {
  case W_MSH:
    idir = X_MSH;
    break;
  case E_MSH:
    idir = X_MSH;
    break;
  case S_MSH:
    idir = Y;
    break;
  case N_MSH:
    idir = Y;
    break;
  }
  return idir;
}

int MSH_type_by_card(int icard) {
  int itype;
  switch (icard) {
  case W_MSH:
    itype = ONE;
    break;
  case E_MSH:
    itype = TWO;
    break;
  case S_MSH:
    itype = ONE;
    break;
  case N_MSH:
    itype = TWO;
    break;
  }
  return itype;
}

/**\fn s_face_msh *MSH_return_face(s_ele_msh *pele,s_ele_msh *pneigh,FILE *fpout)
 *\brief return the face between two elements pele pneigh in the direction idir itype.
 *
 * pele and cmsh must be created before
 */
s_face_msh *MSH_return_face(s_ele_msh *pele, s_ele_msh *pneigh, int idir, int itype, FILE *fpout) {
  s_face_msh *pface;

  if (pele->pdescr->l_side <= pneigh->pdescr->l_side)
    pface = pele->face[idir][itype];
  else
    pface = pneigh->face[idir][MSH_switch_direction(itype, fpout)];
  return pface;
}

#ifdef COUPLED
/**\fn void MSH_calcul_on_face(s_ele_msh *pele)
 *\brief calculate rapport_cote in element description and nsize in carac_mesh
 *
 * pele and cmsh must be created before
 */
void MSH_calcul_on_face(s_face_msh *pface, int idir, FILE *fpout) {
  int itype, k;
  s_face_msh *psubf;
  double l_side, Thick, surf;

  if (pface->p_subface != NULL) {
    if (idir != Z_MSH) {
      for (k = 0; k < NFACE_MSH; k++) {
        psubf = pface->p_subface[k];
        if (psubf != NULL) {
          for (itype = 0; itype < NFACE_MSH; itype++) {
            if (psubf->p_descr[itype] == NULL && psubf->p_ele[itype] != NULL) {
              psubf->p_descr[itype] = MSH_create_descr();
              l_side = psubf->p_ele[itype]->pdescr->l_side / 2;
              surf = psubf->p_ele[itype]->pdescr->surf;
              psubf->p_descr[itype]->l_side = l_side;
              psubf->p_descr[itype]->surf = surf;
            }

          } // for itype in ONE TWO
        }   // if psubf !=NULL
      }     // for all faces in XY dir
    }       // if idir !=Z

    else {
      for (k = 0; k < NCODEG_MSH; k++) {
        psubf = pface->p_subface[k];
        if (psubf != NULL) {
          for (itype = 0; itype < NFACE_MSH; itype++) {
            if (psubf->p_descr[itype] == NULL) {
              psubf->p_descr[itype] = MSH_create_descr();
              if (psubf->p_ele[itype] != NULL) {

                //	        if(Thick!=0)
                //  l_side=psubf->p_ele[itype]->phydro->Thick/2; //BL ATTENTION calcul on face doit etre fait apres la creation de phydro!!
                // else
                l_side = psubf->p_ele[itype]->pdescr->l_side / 2; // BL ATTENTION si l'epaisseur pas renseignee par defaut à la face la longueur de l'element divisee par 2 est definie.
                surf = psubf->p_ele[itype]->pdescr->surf;
              } // if there is a neighboor in itype direction
              else {
                // psubf->p_descr[itype]=MSH_create_descr();
                l_side = 0;
                surf = psubf->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->surf;

              } // if psubf is on top or bottom of the model
              psubf->p_descr[itype]->l_side = l_side;
              psubf->p_descr[itype]->surf = surf;
            } // if psubf->p_descr[itype] haven't been defined yet
          }   // for itype in ONE TWO
        }     // if psubf !=NULL
      }       // for k in all Z faces
    }         // else idir !=Z
  }           // if psubf !=NULL

  else {
    if (pface != NULL) {

      for (itype = 0; itype < NFACE_MSH; itype++) {
        if (idir != Z_MSH) {
          if (pface->p_descr[itype] == NULL && pface->p_ele[itype] != NULL) {
            pface->p_descr[itype] = MSH_create_descr();
            l_side = pface->p_ele[itype]->pdescr->l_side / 2;
            pface->p_descr[itype]->l_side = l_side;
            pface->p_descr[itype]->surf = pface->p_ele[itype]->pdescr->surf;
          }
        } // if idir !=Z
        else {
          if (pface->p_descr[itype] == NULL) {
            pface->p_descr[itype] = MSH_create_descr();
            if (pface->p_ele[itype] != NULL) {

              // Thick=pface->p_ele[itype]->phydro->Thick;
              surf = pface->p_ele[itype]->pdescr->surf;
              // if(Thick!=0)
              // l_side=Thick/2;//BL ATTENTION calcul on face doit etre fait apres la creation de phydro!!
              // else
              l_side = pface->p_ele[itype]->pdescr->l_side / 2;

            } // if there is a neighboor in itype direction
            else {
              l_side = 0;
              surf = pface->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->surf;

            } // if psubf is on top or bottom of the model
            pface->p_descr[itype]->l_side = l_side;
            pface->p_descr[itype]->surf = surf;
          } // if psubf->p_descr[itype] haven't been defined yet

        } // else idir == Z
      }   // for itype in ONE TWO
    }

    else
      LP_error(fpout, "no faces for the element\n");
  }
}

void MSH_calcul_on_all_faces(s_cmsh *pcmsh, FILE *fpout) {
  s_layer_msh *play;
  s_ele_msh *pele;
  s_face_msh *pface;
  int nlayer, nele;
  int idir, itype;
  int layer, ele;
  nlayer = pcmsh->pcount->nlayer;
  for (layer = 0; layer < nlayer; layer++) {
    play = pcmsh->p_layer[layer];
    nele = play->nele;
    for (ele = 0; ele < nele; ele++) {
      pele = play->p_ele[ele];

      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          pface = pele->face[idir][itype];
          MSH_calcul_on_face(pface, idir, fpout);
        }
      }
    }
  }
}
#endif
