/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: functions_MSH.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file function_MSH.h
Here are listed all the shared functions of libmesh.
*/

// manage_simul.c
s_simul_msh *init_simul();
s_simul_msh *MSH_init_simul();

// manage_element.c
s_ele_msh *MSH_create_element(int, int);
s_ele_msh *MSH_destroy_ele(s_ele_msh *);
s_ele_msh *MSH_copy_ele(s_ele_msh *, s_layer_msh *, FILE *); // NF 24/12/2015
s_ele_msh *MSH_chain_ele(s_ele_msh *, s_ele_msh *);
s_ele_msh *MSH_chain_ele_fwd(s_ele_msh *, s_ele_msh *);
s_ele_msh *MSH_secured_chain_ele(s_ele_msh *, s_ele_msh *);
s_ele_msh *MSH_secured_chain_ele_fwd(s_ele_msh *, s_ele_msh *);
s_ele_msh *MSH_browse_ele(s_ele_msh *, int);
s_ele_msh **MSH_tab_ele(s_ele_msh *, int, FILE *);
void MSH_ordinate_mesh(s_ele_msh *, s_cmsh *);
void MSH_carac_ele_short(s_ele_msh *, FILE *);
void MSH_carac_ele_full(s_ele_msh *, FILE *); // init no int
s_ele_msh *MSH_get_neigh_2D(s_ele_msh *, int, int);
void MSH_list_all_ele_pneigh(s_cmsh *, FILE *); // most complete. Necessary to have treated vertical layers
void MSH_list_all_ele(s_cmsh *, FILE *);        // Necessary to have treated vertical layers
void MSH_list_all_ele_short(s_cmsh *, FILE *);
s_descr_msh *MSH_create_descr();
s_descr_msh *MSH_destroy_descr(s_descr_msh *);
s_descr_msh *MSH_copy_descr(s_descr_msh *, FILE *); // NF 24/12/2015
void MSH_calcul_sur_ele(s_ele_msh *, s_cmsh *);
int *MSH_get_ele_gigogne(s_ele_msh *);
int MSH_get_last_gigo(s_ele_msh *);
int MSH_print_ele_gigogne(s_ele_msh *);
int MSH_get_ele_order(s_ele_msh *);
int MSH_get_ele_id(s_ele_msh *, int);
int MSH_get_eleref_id(s_ele_msh *, int);
int MSH_find_dir_neigh(int, int, FILE *);
s_ele_msh *MSH_get_subele(s_ele_msh *, int);
int MSH_is_neigh_in_up(int, int, int, FILE *);
int MSH_is_ele_empty(s_ele_msh *);
void MSH_print_ele_neigh(s_ele_msh *, s_cmsh *, FILE *);
s_id_io ***MSH_init_neigh_tab();
void MSH_tab_neigh(s_ele_msh *, FILE *); // init no int
void MSH_carac_ele_full_neigh(s_ele_msh *, s_cmsh *, FILE *);
void MSH_carac_ele_full_pneigh(s_ele_msh *, s_cmsh *, FILE *);
void MSH_tab_all_neigh(s_cmsh *, FILE *);
s_ele_msh *MSH_get_ele(s_cmsh *, int, int, FILE *);
s_ele_msh *MSH_get_ele_from_id_gis(s_cmsh *pcmsh, int id_lay, int id_gis, FILE *fpout);
int MSH_count_face_neigh(s_ele_msh *);
s_ele_msh *MSH_get_ele_from_abs(s_cmsh *, int, FILE *);
int *MSH_copy_gigo(int *, int); // NF 23/5/2012 copy an array
void MSH_check_vertical_connexion(s_cmsh *, FILE *);
// manage_layer.c
s_layer_msh *MSH_create_layer(char *, int);
void MSH_attribute_ele2layer(s_layer_msh *, s_ele_msh *, int);
s_layer_msh *MSH_chain_layer(s_layer_msh *, s_layer_msh *);
void MSH_list_ele_layer(s_layer_msh *, FILE *);
void MSH_carac_layer(s_layer_msh *, FILE *);
s_layer_msh **MSH_tab_layer(s_layer_msh *, int, FILE *);
void MSH_check_layer_redundancy(s_layer_msh *, FILE *);
void MSH_list_ele_layer_full(s_layer_msh *, FILE *);
int MSH_get_layer_ref_up(s_cmsh *, int, int, int *, int, FILE *);
int MSH_get_layer_ref_down(s_cmsh *, int, int, int *, int, FILE *); // modif
int MSH_get_verti_layer(s_cmsh *, s_ele_msh *, int, int, FILE *);
int MSH_get_layer_rank_by_name(s_cmsh *, char *, FILE *);
void MSH_list_ele_layer_full_pneigh(s_layer_msh *, s_cmsh *, FILE *);
void MSH_tab_neigh_layer(s_layer_msh *, FILE *);
int MSH_verif_verti_layer(s_ele_msh *, int, int *, FILE *); // init
s_layer_msh *MSH_browse_layer(s_layer_msh *, int);          // NF 24/12/2015
// s_ele_msh *MSH_verif_verti_layer(s_ele_msh *, int , int *, FILE *);
void MSH_check_boundary_ele(s_ele_msh *, FILE *);
s_ele_msh *MSH_free_ele(s_ele_msh *, FILE *);
s_layer_msh *MSH_find_layer_in_cmsh(s_cmsh *, char *, FILE *); // NF 24/12/2015
s_layer_msh *MSH_copy_layer(s_layer_msh *, FILE *);            // NF 24/12/2015
// manage_carac_msh.c
s_cmsh *MSH_create_carac_glo();
void MSH_carac_mesh(s_cmsh *, FILE *);
void MSH_check_redundancy(s_cmsh *, FILE *);
int MSH_id_ref(s_cmsh *, int, int, FILE *);
void MSH_link_mesh_SIG(s_cmsh *, FILE *);
s_ele_msh *MSH_pick_ele_ref(s_cmsh *, int, int, int, FILE *);
void MSH_link_ele_ref(s_cmsh *, FILE *);
void MSH_check_boundary(s_cmsh *, FILE *);
void MSH_create_msh(s_cmsh *, int, FILE *);                    // NF 13/10/2015
s_cmsh *MSH_find_cmsh_in_simul(s_simul_msh *, char *, FILE *); // NF 23/12/2015
s_cmsh *MSH_rewind_cmsh(s_cmsh *);                             // NF 23/12/2015
s_cmsh *MSH_chain_cmsh_fwd(s_cmsh *, s_cmsh *);                // NF 23/12/2015
// gridder.c
void MSH_referencing_mesh(s_cmsh *, FILE *);
s_ele_msh ****MSH_define_ref_grid(int dim[Z_MSH], s_cmsh *, FILE *);
int MSH_fit_ref_grid(int, int, double, double, int, s_cmsh *, FILE *);
void MSH_print_ref_grid(s_cmsh *, FILE *);
s_layer_msh **MSH_generate_reference_layer(int *, s_ele_msh ****, int, FILE *);
void MSH_order_element_in_ref(s_cmsh *, FILE *);
void MSH_link_neighbour(s_cmsh *, FILE *);
void MSH_print_all_gigo(FILE *, s_cmsh *, FILE *);
void MSH_print_geosam_input(s_cmsh *, char *, FILE *);
void MSH_check_element_ordering(s_cmsh *, FILE *);
int MSH_is_ele_a_ref(s_ele_msh *, s_cmsh *);
void MSH_print_vois_odic(s_cmsh *, FILE *);
int **MSH_create_neigh_tab(s_cmsh *, int, int, int, int, int, int, int, FILE *);
// itos.c
int MSH_code_gigogne(int, FILE *);
int MSH_gigo_from_dir(int, FILE *);
char *MSH_name_dir_from_gigo(int, FILE *);
char *MSH_name_compass_dir(int, FILE *);
char *MSH_name_gigo_from_dir(int, FILE *);
char *MSH_name_direction(int, FILE *);
char *MSH_name_type(int, FILE *);
char *MSH_name_cell(int, FILE *);
char *MSH_name_neigh(int, int);
char *MSH_name_icard(int, FILE *);
char *MSH_name_position_cell(int, FILE *);
#ifdef COUPLED
char *AQ_name_BC(int);
char *AQ_name_source(int);
char *AQ_name_freq(int);
#endif

// direction.c

int MSH_switch_direction(int, FILE *);
int MSH_switch_type(int, FILE *);
int MSH_convert_gigogne_dir(int, FILE *);

// manage_faces.c
s_face_msh *MSH_create_face(int, int);
s_face_msh *MSH_free_face(s_face_msh *);
s_face_msh *****MSH_define_ref_faces(int dim[Z_MSH], s_cmsh *, FILE *);
void MSH_create_ele_faces(s_cmsh *, FILE *);
int MSH_subface_id(int, int);
int MSH_reflect_corner(int, int);
void MSH_create_subele_face(s_ele_msh *, int, int *, FILE *);
int MSH_is_ref_face(int, int, int, s_ele_msh *, FILE *);
s_face_msh *MSH_get_faceref(int, int, int, s_ele_msh *, FILE *);
void MSH_create_vertical_faces(s_cmsh *, FILE *);
s_face_msh **MSH_create_subfaces_verti(s_ele_msh *, s_ele_msh *, int, int, FILE *);
void MSH_calcul_sur_face(s_ele_msh *, FILE *);
int MSH_type_by_card(int);
int MSH_dir_by_card(int);
s_face_msh *MSH_return_face(s_ele_msh *, s_ele_msh *, int, int, FILE *);
#ifdef COUPLED
void MSH_calcul_on_face(s_face_msh *, int, FILE *);
void MSH_calcul_on_all_faces(s_cmsh *, FILE *);
#endif

// manage_output.c

void MSH_export_geom_surface_layer(s_cmsh *, FILE *);
void MSH_print_geom_ele_surf(s_ele_msh *, FILE *);
void MSH_print_id_ele_surf(s_ele_msh *, FILE *);

// manage_nested.c
s_nestl_msh *MSH_create_nestl(int);
s_neste_msh *MSH_create_neste(int);
