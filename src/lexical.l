/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libmesh
* FILE NAME: lexical.l
* 
* CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS, 
*               Deniz KILIC
* 
* LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh 
* of squared cells.
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


/** @file lexique.l
* @brief Définition des mots du lexique
*/

%x incl str incl_str variable incl_variable unit

%{

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "MSH.h"
#include "global_MSH.h"

#include "input.h"
#include "ext_MSH.h"

/**declaration de la function_inclure**/
void include_function(char *);
/*void yyerror(char *);*/
int yylex();

/** Pointeur vers une chaîne de caractères */
char *pname;


%}

%a 10000
%p 10000
%o 15000
%e 5000
%n 2000

number	[0-9]
to_power [DdEe][-+]?{number}+
alnum [0-9A-Za-z]

%%
#[^\n]*\n 	{ line_nb++;}	/* To not read the comments */
\n	{ line_nb++;}
[ \t]+ ;			/* To ignore spaces and tabulations */
\$ {BEGIN(variable);} 
\[ {BEGIN(unit); return LEX_OPENING_BRACKET;}
[Ii][Nn][Cc][Ll][Uu][DdRr][Ee] {BEGIN(incl);}
"=" return LEX_EQUAL; 
"{" return LEX_OPENING_BRACE;
"}" return LEX_CLOSING_BRACE;
":" return LEX_COLON;
";" return LEX_SEMI_COLON;
"," return LEX_VIRGULE ;
[Nn][oO] {yylval.integer = NO_TS; return LEX_ANSWER;}
[Yy][Ee][Ss] {yylval.integer = YES_TS; return LEX_ANSWER;}


[Ii][Nn][Pp][Uu][Tt]"_"[Ff][Oo][Ll][Dd][Ee][Rr][Ss] return LEX_INPUT_FOLDER;
[Oo][Uu][Tt][Pp][Uu][Tt]"_"[Ff][Oo][Ll][Dd][Ee][Rr] return LEX_OUTPUT_FOLDER;
[Ww][Rr][Ii][Tt][Ee]"_"[Oo][Uu][Tt][Pp][Uu][Tt]"_"[Oo][Dd][Ii][Cc] return LEX_WRITE_ODIC;
[Aa][Uu][Tt][Oo] return LEX_AVIEW;
[Ee][Nn][Dd] return LEX_AVIEW;
[Gg][Ww]"_"[Mm][Ee][Ss][Hh] return LEX_NEWSAM;
[Ll][Aa][Yy][Ee][Rr] return LEX_LAYER ;

[Nn][Ee][Ss][Tt][Ee][Dd] return LEX_NESTED;
[Cc][Oo][Aa][Rr][Ss][Ee] {yylval.integer = COARSE_MSH; return LEX_RESOLUTION_MSH;};
[Ff][Ii][Nn][Ee] {yylval.integer = FINE_MSH; return LEX_RESOLUTION_MSH;};

{number}+ 	|
"-"{number}+ {yylval.integer  = atoi(yytext); return LEX_INT;}
{number}+"."{number}*({to_power})?	| 
"-"{number}+"."{number}*({to_power})?	| 
{number}*"."{number}+({to_power})?	 |
"-"{number}*"."{number}+({to_power})?	{ yylval.real = atof(yytext); return LEX_DOUBLE;}
<incl>[ \t]*    /* skip spaces and tabulations */
<incl>[ \n]*    /* skip newlines */
<incl>\$ {BEGIN(incl_variable);} 
<incl>[^$ \t\n:;,\"]+ {
               char *name;
               name = (char *)strdup(yytext);
               include_function(name);
               BEGIN(INITIAL);
 }
\" {/* Allows to read string chains with spaces */ BEGIN(str);} 
<incl>\" {BEGIN(incl_str); /* Beginning of a string chain */ } 
<incl_str>\n { /* A string chain ends with \n */
         LP_error(Simul->poutputs,"lmesh %4.2f -> String chain not ended\n",VERSION_MSH);}
<incl_str>[^\\\n\"]+\" {
             int i,k;
             i = strlen(yytext);
             pname = (char *)malloc((i - 1) * sizeof(char));
             for (k = 0; k < i - 1; k++)
                   pname[k] = yytext[k];
             include_function(pname);
             BEGIN(INITIAL);
         }
<str>\" {BEGIN(INITIAL);} 
<str>\n { /* A string chain ends with \n */
         LP_error(Simul->poutputs,"lmesh %4.2f -> String chain not ended\n",VERSION_MSH);}
<str>[^\\\n\"]+ {
             yylval.string = (char *)strdup(yytext);
             return LEX_NAME;
}

<unit>"/" {return LEX_INV;}
<unit>"^" {return LEX_POW;}

<unit>"°C" {yylval.real = 1.0; return LEX_A_UNIT;}

<unit>"min" {yylval.real = 60.0; return LEX_A_UNIT;}
<unit>"s" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"h" {yylval.real = 3600.0; return LEX_A_UNIT;}
<unit>"d" {yylval.real = 86400.0; return LEX_A_UNIT;}
<unit>"yr" {yylval.real = 31536000; return LEX_A_UNIT;} /* years on the bases of 365 days per year */


<unit>"m" {yylval.real = 1.0; return LEX_A_UNIT;}
<unit>"km" {yylval.real = 1000.0; return LEX_A_UNIT;}
<unit>"cm" {yylval.real = 0.01; return LEX_A_UNIT;}
<unit>"ha" {yylval.real = 10000; return LEX_A_UNIT;}
<unit>"mm" {yylval.real = 0.001; return LEX_A_UNIT;}

<unit>"l" {yylval.real = 0.001; return LEX_A_UNIT;}

<unit>"ug" {yylval.real = 0.000001; return LEX_A_UNIT;}
<unit>"mg" {yylval.real = 0.001; return LEX_A_UNIT;}
<unit>"g" {yylval.real = 1.; return LEX_A_UNIT;}
<unit>"kg" {yylval.real = 1000.0; return LEX_A_UNIT;}
<unit>"t" {yylval.real = 1000000.0; return LEX_A_UNIT;}

<unit>"%" {yylval.real = 1.0; return LEX_A_UNIT;}

<unit>{number}+ 	|
<unit>"-"{number}+ {yylval.integer  = atoi(yytext); return LEX_INT;}
<unit>{number}+"."{number}*({to_power})?	| 
<unit>"-"{number}+"."{number}*({to_power})?	| 
<unit>{number}*"."{number}+({to_power})?	 |
<unit>"-"{number}*"."{number}+({to_power})?	{ yylval.real = atof(yytext); return LEX_DOUBLE;}
<unit>\] {
  BEGIN(INITIAL);
  return LEX_CLOSING_BRACKET;
}
<variable>[[:alnum:]]+ {
  /* Inserts the contents of the given variable */
  char *valeur = getenv(yytext);
  if (valeur == NULL) {
    LP_error(Simul->poutputs,"lmesh %4.2f -> File \"%s\", variable \"%s\" undefined, line %d\n",VERSION_MSH,current_read_files[pile].name,yytext,line_nb);
  }
  {
    int i;
    for (i = strlen(valeur) - 1; i >= 0; i--)
      unput(valeur[i]);
  }
  BEGIN(INITIAL);
}
<incl_variable>[[:alnum:]]+ {
  /* Inserts the contents of the given variable */
  char *valeur = getenv(yytext);
  if (valeur == NULL) {
    LP_error(Simul->poutputs,"lmesh %4.2f --> File \"%s\", variable \"%s\" undefined, line %d\n",VERSION_MSH,current_read_files[pile].name,yytext,line_nb);
  }
  {
    int i;
    for (i = strlen(valeur) - 1; i >= 0; i--)
      unput(valeur[i]);
  }
  BEGIN(incl);
}
[^$ \t\n:;,\"\[\]=]+ {
yylval.string = (char *)strdup(yytext);
return LEX_NAME;
}

%%

/* Procedure used to find a file which name is given
* The folders defined by the user in the command file are searched in
* yyin points towards the file read by the parser
*/

void find_file(char *name)
{
int i;
char *new_name;

yyin = fopen(name,"r");
if (yyin == NULL) {
for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i]) + strlen(name) + 2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       /*printf("%s\n",new_name);*/
       yyin = fopen(new_name,"r");
       free(new_name);
       if (yyin != NULL) 
       break;
       }
       }
       if (yyin == NULL)
       LP_error(Simul->poutputs,"lmesh %4.2f -> No file %s\n",VERSION_MSH,name);
       
       }
       
       /* Procedure used to include a file
       *
* - opening of the file which name is in argument
* - storage of the variables related to the file which was read (line_nb, file name, buffer's content)
* - incrementation of the variables of the file pile
*/

void include_file(char *name)
{
int i;
char *new_name;

current_read_files[pile].line_nb = line_nb;
   current_read_files[pile].buffer = YY_CURRENT_BUFFER;
   line_nb = 0;
   pile++;
   if (pile >= NPILE)
       LP_error(Simul->poutputs,"lmesh %4.2f -> File %s : Too many files included the ones in the others, maximum = %d\n",VERSION_MSH,name,NPILE);
   yyin = fopen(name,"r");
   if (yyin != NULL)
       current_read_files[pile].name = (char *)strdup(name);
   else {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i])+strlen(name)+2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       yyin = fopen(new_name,"r");
       current_read_files[pile].name = (char *)strdup(new_name);
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"lmesh %4.2f -> Not possible to find the file %s\n",VERSION_MSH,name);
   
   current_read_files[pile].address = yyin;
   yy_switch_to_buffer(yy_create_buffer(yyin,YY_BUF_SIZE));
   LP_printf(Simul->poutputs,"File %s opened\n",current_read_files[pile].name);
   free(name);

}

void include_function(char *name)
{
int i;
   char *new_name;

   current_read_files[pile].line_nb = line_nb;
   current_read_files[pile].buffer = YY_CURRENT_BUFFER;
   line_nb = 0;
   pile++;
   if (pile >= NPILE)
       LP_error(Simul->poutputs,"lmesh %4.2f -> File %s : Too many files included the ones in the others, maximum = %d\n",VERSION_MSH,name,NPILE);
   yyin = fopen(name,"r");
   if (yyin != NULL)
       current_read_files[pile].name = (char *)strdup(name);
   else {
     for (i = 0; i <folder_nb; i++) {
       new_name = (char *)calloc(strlen(name_out_folder[i])+strlen(name)+2,sizeof(char));
       sprintf(new_name,"%s/%s",name_out_folder[i],name);
       yyin = fopen(new_name,"r");
       current_read_files[pile].name = (char *)strdup(new_name);
       free(new_name);
       if (yyin != NULL) 
         break;
     }
   }
   if (yyin == NULL)
      LP_error(Simul->poutputs,"lmesh %4.2f -> Not possible to find the file %s\n",VERSION_MSH,name);
   
   current_read_files[pile].address = yyin;
   yy_switch_to_buffer(yy_create_buffer(yyin,YY_BUF_SIZE));
   LP_printf(Simul->poutputs,"File %s opened\n",current_read_files[pile].name);
   free(name);

}

/* Procedure used to close the files read by the parser
* Called when the parser identifies the end of a file
*
* - closure of the file
* - decrementation of the file pile (pile--)
* - reset of the variables (name, line_nb, buffer) to the values of the last file (if it exists)
* - no more file to read if pile<0
*/

int yywrap()
{
   fclose(current_read_files[pile].address);
   yy_delete_buffer(YY_CURRENT_BUFFER);
   /*if (line_nb == 0)
      printf("\nWARNING : Empty file %s\n",current_read_files[pile].name);*/
   pile--;
   //num = 0;
    if (pile < 0)
      return 1;
   else {
      yyin = current_read_files[pile].address;

      #if defined GCC481 ||  defined GCC473 || GCC472 ||  defined GCC471
      yy_switch_to_buffer((YY_BUFFER_STATE) current_read_files[pile].buffer);//gcc4.7
      #elif defined GCC441  || defined GCC442 ||  defined GCC443  || defined GCC444
// || defined GCC445
      yy_switch_to_buffer((yy_buffer_state*)current_read_files[pile].buffer);//gcc4.4
      #else 
// defined GCC445
      yy_switch_to_buffer(current_read_files[pile].buffer);//gcc3.x
      #endif /*test on GCC*/

      line_nb = current_read_files[pile].line_nb;
      return 0;
   }
}
