/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

int MSH_code_gigogne(int type, FILE *fp) {
  int c;

  switch (type) {
  case NW: {
    c = 0;
    break;
  }
  case NE: {
    c = 1;
    break;
  }
  case SW: {
    c = 2;
    break;
  }
  case SE: {
    c = 3;
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Gigogne code Out of scope in code_gigogne()\n", VERSION_MSH);
  }
  }

  return c;
}

int MSH_gigo_from_dir(int type, FILE *fp) {
  int c;
  c = MSH_code_gigogne(type, fp) + 1;
  return c;
}

char *MSH_name_compass_dir(int type, FILE *fp) {
  char *name;

  switch (type) {
  case NW: {
    name = strdup("NW");
    break;
  }
  case NE: {
    name = strdup("NE");
    break;
  }
  case SW: {
    name = strdup("SW");
    break;
  }
  case SE: {
    name = strdup("SE");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Gigogne code Out of scope in name_gigogne()\n", VERSION_MSH);
  }
  }

  return name;
}

char *MSH_name_gigo_from_dir(int type, FILE *fp) {
  char *name;
  switch (type) {
  case NW: {
    name = strdup("1");
    break;
  }
  case NE: {
    name = strdup("2");
    break;
  }
  case SW: {
    name = strdup("3");
    break;
  }
  case SE: {
    name = strdup("4");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Gigogne code Out of scope in name_gigogne()\n", VERSION_MSH);
  }
  }

  return name;
}

char *MSH_name_direction(int type, FILE *fp) {
  char *name;

  switch (type) {
  case X_MSH: {
    name = strdup("X");
    break;
  }
  case Y_MSH: {
    name = strdup("Y");
    break;
  }
  case Z_MSH: {
    name = strdup("Z");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Direction doesn't exist in MSH_name_direction(int type,FILE *fp)\n", VERSION_MSH);
  }
  }

  return name;
}

char *MSH_name_type(int type, FILE *fp) {
  char *name;

  switch (type) {
  case ONE: {
    name = strdup("ONE");
    break;
  }
  case TWO: {
    name = strdup("TWO");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Type doesn't exist in MSH_name_direction(int type,FILE *fp)\n", VERSION_MSH);
  }
  }

  return name;
}

char *MSH_name_cell(int type, FILE *fp) {
  char *name;

  switch (type) {
  case ACTIVE: {
    name = strdup("ACTIVE");
    break;
  }
  case REFERENCE: {
    name = strdup("REFERENCE");
    break;
  }
  case SUBREF: {
    name = strdup("SUBREF");
    break;
  }
  case GHOST: {
    name = strdup("GHOST");
    break;
  }
  case BOUND: {
    name = strdup("BOUNDARY");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Type of cell doesn't exist in %s l.%d\n", VERSION_MSH, __FILE__, __LINE__);
  }
  }

  return name;
}
char *MSH_name_position_cell(int type, FILE *fp) {
  char *name;

  switch (type) {
  case NO_LOC_MSH: {
    name = strdup("INTERLAYER");
    break;
  }
  case TOP_MSH: {
    name = strdup("TOP");
    break;
  }
  case BOT_MSH: {
    name = strdup("BOTTOM");
    break;
  }
  case RIV_MSH: {
    name = strdup("RIVER");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: Type of cell doesn't exist in %s l.%d\n", VERSION_MSH, __FILE__, __LINE__);
  }
  }

  return name;
}

char *MSH_name_neigh(int idir, int itype) {
  char *name;

  if ((idir == X_MSH) && (itype == ONE))
    name = strdup("W");
  else if ((idir == X_MSH) && (itype == TWO))
    name = strdup("E");
  else if ((idir == Y_MSH) && (itype == ONE))
    name = strdup("S");
  else if ((idir == Y_MSH) && (itype == TWO))
    name = strdup("N");
  else if ((idir == Z_MSH) && (itype == TWO))
    name = strdup("UP");
  else if ((idir == Z_MSH) && (itype == ONE))
    name = strdup("DOWN");
  return name;
}

char *MSH_name_icard(int type, FILE *fp) {
  char *name;

  switch (type) {
  case N_MSH: {
    name = strdup("NORTH");
    break;
  }
  case S_MSH: {
    name = strdup("SOUTH");
    break;
  }
  case W_MSH: {
    name = strdup("WEST");
    break;
  }
  case E_MSH: {
    name = strdup("EAST");
    break;
  }
  default: {
    LP_error(fp, "In libmesh%4.2f: %s l.%d This is not a direction\n", __FILE__, __LINE__, VERSION_MSH);
  }
  }

  return name;
}
