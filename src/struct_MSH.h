/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: struct_MSH.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <time.h>

typedef struct simul_msh s_simul_msh;
typedef struct face_msh s_face_msh;
typedef struct description_msh s_descr_msh;
typedef struct mesh_layer s_layer_msh;
typedef struct element_msh s_ele_msh;
typedef struct counter_msh s_count_msh;
typedef struct carac_mesh s_cmsh;
typedef struct nested_layer s_nestl_msh;
typedef struct nested_element s_neste_msh;
typedef struct interface s_interface_msh;

/** \struct interface_msh of type s_interface_msh**/ // NF 27/7/2021 we introduce a new structure interface in msh to account for transfer and reactivity of the river streambed and aquitars in cawaqs. The interfaces are accessible through faces. They include the conductance for the hydraulics and TBD for libttc
struct interface {
  double thickness;
#ifdef COUPLED
  s_cond_aq *pcond; /*!< pointer to strucutur of conductivities values*/
#endif
#ifdef COUPLED_TTC
  s_param_calc_ttc *pparam_calc_ttc;
  double ttc_val;
  double ttc_flux;
#endif
};

/** \struct simul_msh of type s_simul_msh
\brief Is the wrapping structur of libmesh
*/
struct simul_msh {
  char name[STRING_LENGTH_LP];
  char *name_outputs; /*!< Name of the output director */
  FILE *poutputs;     /*!< Debug File where warnings and indications about the sinulation are written */
  /** define the corners of the domain*/
  // double corner[ND_MSH][NMAXI];
  s_chronos_CHR *pchronos; /*!< time structur*/
  s_clock_CHR *pclock;     /*!< time structur*/
  s_cmsh *pcmsh;           /*!< mesh caracterisation structur*/
  int write_odic;          /*!< simul option to write ODIC neighboor WARNING : if write_odic==YES && nlayer > 1 the first layer has to be surface layer (pour l'instant en vrac dans simul si les options se multiplient une structure devra �tre faite !!*/
  s_nestl_msh *nestl;      /*Pointer towards nested layers*/
};
/** \struct  counter_msh of type s_count_msh
\brief numberize layer and element in layers
*/
struct counter_msh {
  int nlayer; /*!< number of layers*/
  int nele;   /*!< number of elements*/
#ifdef COUPLED
  s_count_hydro_aq *pcount_hydro; /*!<structur hydro if coupled with libaq*/
#endif
#ifdef COUPLED_TTC
  s_count_ttc *pcount_ttc; /*!<structur hydro if coupled with libaq*/
#endif
};

/** \struct  mesh_layer of type  s_layer_msh
\brief caracterise a mesh grid
*/
struct mesh_layer {
  char name[STRING_LENGTH_LP]; /*!< name of the mesh grid*/
  int id;                      /*!< id of mesh grid*/
  int nele;                    /*!< number of elements in the mesh grid*/
  int type;                    /*!< type of layer CONFINED_AQ/UNCONFINED_AQ */
  int nlitho;                  /*!< number of lithologies different than 1 for the UNCONFINED type */
  s_litho_aq *plitho;          /*!< pointer to a chain of lithologies*/
  s_ele_msh *pele;             /*!< pointer to an element*/
  s_ele_msh **p_ele;           /*!< array of all element in mesh */
  s_layer_msh *next;           /*!< next grid mesh */
  s_layer_msh *prev;           /*!< previous grid mesh */
#ifdef COUPLED
  int mean_type;                                                                             /*!< define how the mean should be calculated in [GEOM,HARMO,ARI]*/
  int cond_type; /*!< define how the conductance at face should be calculated in [COND,TZ]*/ // NF 13/9/2022 This is not the proper location for this attribute that is an hydro attribute. It shouldn't be present directely under layer. Moreover it doesn't make sens to have it here for layer that may have part of itself as the impluvium and another part confined.... From this mistake comes a lot of
                                                                                             // programming issue for the calculation of the equivalent conductance between aquifer layers.
  int lay_type;                                                                              /*!< caracterize layer type in [CONFINED,UNCONFINED]*/
#endif
};
/** \struct element_msh of type  s_ele_msh
\brief caracterise a cell in mesh grid
*/
struct element_msh {
  int n[ND_MSH];                       /*!<Coordinates in the ref grid for the ref element. Not used for nested elements*/
  int id[NID_MSH];                     /*!<GIS_MSH -> identifiant du SIG, INTERN_MSH-> identifiant interne a une layer varie de 0 a nele_layer-1, ABS_MSH -> identifiant absolu unique pour tout le maillage*/
  s_face_msh *face[ND_MSH][NFACE_MSH]; /*!<pointer to faces : X and Y*/
  s_descr_msh *pdescr;                 /*!< pointer to the description of the element, coordinates, physical properties,...*/
  /** pointer to the hydrological component of the element: velocities, head not implemented yet*/
  s_ele_msh *next;                                             /*!< next element */
  s_ele_msh *prev;                                             /*!< previous element */
  s_layer_msh *player;                                         /*!< layer where the grid cell is */
  s_ele_msh *pele_ref;                                         /*!< pointer to the reference element in the reference mesh*/
  int nvoisin[ND_MSH][NFACE_MSH]; /*!< number of neighbours */ /// 5_10_2010///
  s_id_io ***p_neigh;                                          /*!<list the neighboors in each direction. The layer (id-1)  and the element intern_id  of each neighboor is stored. The dimension of the pointer tabulars is ND_MSH in [X,Y,Z] *  NFACE_MSH in [ONE,TWO]*/
  s_ele_msh **p_ghost;                                         /*!< ghost node matrix in a grid cell */
  int nele;                                                    /*!< Number of elements in an element. For now used only for the reference elements, but might serve for Baptiste */
  int nratio;                                                  /*!<Maximum number of element within an element*/
  s_ele_msh **p_subele;                                        /*!<sub element in reference mesh */
  int ndiv;                                                    /*!<defines the sizeof p_subele NCORNER_MSH or 1*/
  int type;                                                    /*!<Type of element (ACTIVE,REFERENCE,SUBREF,GHOST,BOUNDARY)*/
  int loc;                                                     /*!<position of element (NO_LOC_MSH,TOP_MSH,BOT_MSH,RIV_MSH)*/
#ifdef COUPLED
  s_hydro_aq *phydro;         /*!<structur hydro if coupled with libaq*/
  s_id_spa *link[NB_LINK_AQ]; /*!<pointeur to all linked element of other modules*/
  s_id_io *id_riv;            /*!<pointer to river cell identifiant Attention peut marcher seulement si 1 cell riv -> 1gw_cell*/
  int modif[NB_MODIF];        /*!< integer in [YES,NO] to know if mat6 has been modified*/
#endif
#ifdef COUPLED_TTC
  s_param_ttc *pparam_ttc; /*!< pointer to ttc parameter values*/                                           // DK AR 25 08 2021 pparambase > pparam_ttc as in DK branch
  s_val_ttc *pval_ttc; /*!< pointer to ttc state variable*/                                                 // DK AR 25 08 2021 pvar_ttc > pval_ttc as in DK branch
  s_flux_ttc *pflux_ttc; /*!< pointer to ttc flux*/                                                         // DK AR 25 08 2021 update pflux_ttc as a table
  s_aquitard_ttc *paquitard_ttc; /*!< pointer to ttc aquitard structure */                                  // DK AR 25 08 2021 update pflux_ttc as a table
  s_aquitard_ttc *priverbed_ttc; /*!< pointer to ttc aquitard structure for riverbed analytical solution */ //
  int nsource_type_ttc[NB_SOURCE_TTC];
  s_source_ttc ***psource_ttc; /*!< pointer to ttc source structure */ // DK AR 25 08 2021
  int nbound_ttc;                                                      // NG : 19/06/2023 : Initialized to 0 at element creation
  int nvois_ttc;                                                       // NG : 19/06/2023 : Initialized to 0 at element creation
#endif
};

/** \struct description_msh of type s_descr_msh
\brief mesh description
*/
struct description_msh {
  double **coord;      /*!<Coordinates of the element corners, note used if description of a face*/
  double *center;      /*!<Coordinates of the element center: x,y,z*/
  double surf;         /*!<surface of element*/
  double l_side;       /*!<element length*/
  double rapport_cote; /*!<ratio of element and subelement */
  int *code_gigogne;   /*!<code to describe subelement position in reference element*/
};

/** \struct face of type  s_face_msh
\brief caracterise a face of element
*
*
*/
struct face_msh {

  s_ele_msh **p_ele; /*!<pointer to elements (the face is separating the two elements, index NELEMENT*/
  /** pointer to neighbor faces. For instance a i cell point to i-1 (ONE) and i+1 (TWO). And this in all direction N,S,E,W*/
  // s_face ***neighbor[ND_MSH];//NF 8/15/06
  int kindof;               /*!< X, Y or Z face*/
  s_descr_msh **p_descr;    /*!<pointer to the description of the face separating two elements*/
  s_face_msh **p_subface;   /*!< pointer to subface in ref element*/
  int nsubface;             /*!< number of subfaces*/
  int id;                   /*<face id*/
  int type;                 /*!<Type of element (ACTIVE,REFERENCE,SUBREF,GHOST,BOUNDARY)*/
  int loc;                  /*!<position of element (NO_LOC_MSH,TOP_MSH,BOT_MSH,RIV_MSH)*/
  s_interface_msh *pinterf; // NF 27/7/2021 pointers towards an interface structure that is needed to calculate the transported state variable in the interface (HZ and aquitard)

#ifdef COUPLED
  s_hydro_aq *phydro; /*!<structur hydro if coupled with libaq*/ // NF 27/7/2021 the structure towards the pcond is here. It will be replaced by a pointer towards pcond through the interface structure added above
  s_flux_aq *pflux;                                              /*<!structur flux if coupled with libaq*/
#endif

#ifdef COUPLED_TTC
  double u_ttc; /*!<  ttc velocity*/ // DK modification added subface
  double flux_ttc; /*!< ttc flux*/   // DK AR Remove [0] here
  double val_bc_ttc;                 /// valeur de la bound
  double *val_bc_t_ttc;              /// valeur transient  // I might have to add value here to get valbc transient  DK AR transient 27 07 2021
  double *tval;
  s_boundary_ttc *pboundary_ttc; /*!< pointer to ttc source structure */ // DK AR 25 08 2021
  int icl_bc_ttc;                                                        /// valeur de la bound
#endif
};

/** \struct carac_mesh of type s_cmsh
\brief caracterise grid mesh
*
*
*/

struct carac_mesh {
  char name[STRING_LENGTH_LP];
  s_count_msh *pcount;               /*!<struct counter_msh*/
  double mini[ND_MSH], maxi[ND_MSH]; /*!<  coordonnées des coins de la grille de référence newsam*/
  int dim[ND_MSH];                   /*!< number of column X and rows Y*/
  double l_min, l_max /*!< cell dimensions of min and max cell mesh size*/;
  double MINI[ND_MSH], MAXI[ND_MSH], Haut_g[ND_MSH]; /*!<reference grid mesh coordinate. MINI botom left corner, MAX top right corner, Haut_g top left corner*/
  int dimension;                                     /*!< l_max/l_min ration*/
  int compteur[ND_MSH];                              /*!< used for reference grid referencement*/
  int nsize;                                         /*!< number of different cell size*/
  s_layer_msh *play;                                 /*!< pointer to layer structur*/
  s_layer_msh **p_layer;                             /*!<array to all the pointers to layers*/
  s_layer_msh **play_ref;                            /*!<array to all the pointers to reference layer*/
  s_ele_msh ****pele_ref;                            /*!< Array containing all the pointers to elements of the reference grid Watch, elements are refered to as pele_ref[layer][Y][X]*/
  s_face_msh *****pfaces_ref;                        /*!< Array containing all the pointers to faces of the reference grid, faces are refered to as pfaces_ref[layer][Y][X][dir]*/
  s_cmsh *next;
  s_cmsh *prev;
};

/** \struct  nested_layer of type  s_nestl
\brief defines nested layers between two models
*/
struct nested_layer {
  // char name[STRING_LENGTH_LP]; /*!< name of the nested area*/
  int id;                               /*!< id of nested layer*/
  s_layer_msh *nested[NRESOLUTION_MSH]; /*!< pointers towards layers in a tab accessible with COARSE_MSH and FINE_MSH */
  s_neste_msh **p_nested;               /*!< pointers array towards the coarser element of the nested area */
  int nnested;                          /*!< number of coarser elements in the nested area */
  s_cmsh *pnestcmsh;                    /*!< Pointer towards a local cmsh corresponding to the nested layers*/
  s_nestl_msh *next;                    /*!< next nested layer */
  s_nestl_msh *prev;                    /*!< previous nested layer  */
};

/** \struct  nested_element of type  s_neste_msh
\brief defines nested elements between two models
*/
struct nested_element {
  int id;                                    /*!< id of nested_element*/
  s_ele_msh *pcoarse;                        /*!< pointer towards the element of the coarse grid */
  s_ele_msh **pfine;                         /*!< pointers array towards all the fine elements contained in the coarse element */
  int nfine;                                 /*!< number of fine elements in the coarser element */
  s_ele_msh **pele_border[Z_MSH][NFACE_MSH]; /*!< pointers array towards fine element composing the face of the coarse element */
  int nele_border[Z_MSH][NFACE_MSH];         /*!< number of fine elements composing the face (X_MSH or Y_MSH/ONE_MSH or TWO_MSH) of the coarse element */
  s_neste_msh *next;                         /*!< next nested layer */
  s_neste_msh *prev;                         /*!< previous nested layer  */
};

#define new_simul_msh() ((s_simul_msh *)malloc(sizeof(s_simul_msh)))
#define new_ele_msh() ((s_ele_msh *)malloc(sizeof(s_ele_msh)))
#define new_face_msh() ((s_face_msh *)malloc(sizeof(s_face_msh)))
#define new_descr_msh() ((s_descr_msh *)malloc(sizeof(s_descr_msh)))
#define new_layer_msh() ((s_layer_msh *)malloc(sizeof(s_layer_msh)))
#define new_counter_msh() ((s_count_msh *)malloc(sizeof(s_count_msh)))
#define new_carac_mesh() ((s_cmsh *)malloc(sizeof(s_cmsh)))
#define new_neste_msh() ((s_neste_msh *)malloc(sizeof(s_neste_msh)))
#define new_nestl_msh() ((s_nestl_msh *)malloc(sizeof(s_nestl_msh)))
#define new_interface_msh() ((s_interface_msh *)malloc(sizeof(s_interface_msh)))
