#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libmesh
# FILE NAME: update_param.awk
# 
# CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS, 
#               Deniz KILIC
# 
# LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh 
# of squared cells.
#
# Library developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/


BEGIN{
  name="awk.out";
}
{
    if (substr($2,0,7)=="VERSION")
    printf("#define VERSION_MSH %4.2f\n",nversion)>name;
  else
    printf("%s\n",$0)>name;
}
