/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_nested.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

/**\fn s_nestl_msh *MSH_create_nestl(char *name,int id)
 *\brief create a nested layer
 *
 *
 */
s_nestl_msh *MSH_create_nestl(int id) {

  s_nestl_msh *pnestl;

  pnestl = new_nestl_msh();
  bzero((char *)pnestl, sizeof(s_nestl_msh));
  pnestl->id = id;
  return pnestl;
}

/**\fn s_neste_msh *MSH_create_neste(char *name,int id)
 *\brief create a nested element
 *
 *
 */
s_neste_msh *MSH_create_neste(int id) {

  s_neste_msh *pneste;

  pneste = new_neste_msh();
  bzero((char *)pneste, sizeof(s_neste_msh));
  pneste->id = id;
  return pneste;
}
