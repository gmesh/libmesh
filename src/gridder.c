/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: gridder.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/** @file gridder.c
 * @brief Functions for creating a mesh referenced within a reference mesh
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "IO.h"
#include "spa.h"
#include "MSH.h"

/** @fn void MSH_print_all_gigo(FILE *fich,s_cmsh *pcmsh,FILE *fpout)
 * @brief Prints in fich the input file for geosam
 *
 * @arg FILE *fich: pointer to geosam input file
 * @arg s_cmsh *pcmsh: pointer to the characteristics structur of a mesh
 * @arg FILE *fpout: pointer to log file
 * @return void
 */
void MSH_print_all_gigo(FILE *fich, s_cmsh *pcmsh, FILE *fpout) {

  s_ele_msh *pele;
  char *nombre1;
  int i, j;
  int long_maille;
  int long_tot;
  double rapport;
  int nlay;

  nlay = pcmsh->pcount->nlayer;

  nombre1 = (char *)calloc(STRING_LENGTH_LP, sizeof(char));

  for (i = 0; i < nlay; i++) {
    long_maille = 0;
    long_tot = 0;
    pele = pcmsh->p_layer[i]->pele;
    while (pele != NULL) {
      sprintf(nombre1, "%d", MSH_get_ele_id(pele, ABS_MSH));
      long_maille = strlen(nombre1);
      sprintf(nombre1, "%d", MSH_get_eleref_id(pele, ABS_MSH));
      long_maille += strlen(nombre1);
      long_maille += MSH_get_ele_order(pele) + 3;
      long_tot += long_maille;
      if (long_tot > LLINE_F77_MSH) {
        long_tot = long_maille;
        fprintf(fich, "\n");
      }
      fprintf(fich, "%d,%d,", MSH_get_ele_id(pele, ABS_MSH), MSH_get_eleref_id(pele, INTERN_MSH));
      rapport = MSH_get_ele_order(pele);
      fprintf(fich, "%d;", MSH_print_ele_gigogne(pele));
      pele = pele->next;
    }
    fprintf(fich, "\nfin\n");
  }
  free(nombre1);
}
/**\fn void MSH_print_header_fichsam(FILE *fich,s_cmsh *pcmsh)
 *\brief print header to geosam (MODCOU gridder)
 *
 */
void MSH_print_header_fichsam(FILE *fich, s_cmsh *pcmsh, char *name) {
  int i;
  int longueur = 0;
  int ncouche;
  int dim[Z_MSH];

  for (i = 0; i < Z_MSH; i++)
    dim[i] = pcmsh->dim[i];

  ncouche = pcmsh->pcount->nlayer;

  fprintf(fich, "10,'unknown','%s/fic10','unformatted'\n", getenv("RESULT"));
  fprintf(fich, "11,'unknown','%s/fic11','unformatted'\n", getenv("RESULT"));
  fprintf(fich, "19,'unknown','%s/ficxy','unformatted'\n", getenv("RESULT"));
  fprintf(fich, "fin \n");
  fprintf(fich, "#titre de l'etude \n");
  fprintf(fich, "%s\n", name);
  fprintf(fich, "# Schema-type %d * %d\t%d couche(s)\t %d tailles de mailles ntraits\n", dim[Y], dim[X_MSH], ncouche, pcmsh->nsize);
  // if (pcmsh->nsize<GEOSAM_BUG)
  // fprintf(fich,"%5d%5d%5d%4.0f.    1    1    0    0    090000\n",dim[X_MSH],dim[Y],ncouche,pcmsh->nsize);
  // else{

  // LP_warning(fp,"In lmesh%4.2f: The length of the reference grid side might be  interpreted by geosam  as fraction of meters... It's a bug in geosam that is restructed by a strong fortran77 input format. Unfortunatelly the program is not maintained anymore.", VERSION_MSH);
  fprintf(fich, "%5d%5d%5d%5d    1    1    0    0    090000\n", dim[X_MSH], dim[Y], ncouche, pcmsh->nsize);
  //}
  fprintf(fich, "# surface de la maille de reference\n");
  fprintf(fich, "%9.0f.\n", pcmsh->l_max);
  fprintf(fich, "# nombre de mailles par couche\n");
  for (i = 0; i < ncouche; i++) {
    longueur = LP_jump_line(longueur, LLINE_NSAM_MSH, fich);
    fprintf(fich, "%5d", pcmsh->p_layer[i]->nele);
    longueur += 5;
  }
  fprintf(fich, "\n# numero operateur et schema-type\n");
}
/**\fn void MSH_print_geosam_input(s_cmsh *pcmsh,FILE *fpout)
 *\brief print input to geosam (MODCOU gridder)
 *
 */
void MSH_print_geosam_input(s_cmsh *pcmsh, char *name, FILE *fpout) {
  int i;
  char nom_fichier[STRING_LENGTH_LP];
  FILE *fichier;
  double ratio[Z_MSH];
  int ratio_int[Z_MSH];
  int dim[Z_MSH];
  LP_printf(fpout, "Printing input file for geosam: ");

  sprintf(nom_fichier, "%s/geosam.def", getenv("RESULT"));
  fichier = fopen(nom_fichier, "w");

  MSH_print_header_fichsam(fichier, pcmsh, name);
  MSH_print_all_gigo(fichier, pcmsh, fpout);
  fclose(fichier);
  LP_printf(fpout, " %s\n", nom_fichier);
}

/**\fn void MSH_print_vois_odic(s_cmsh *pcmsh,FILE *fpout)
 *\brief print neighboor inputs for Eaudyssee (fortran)
 *
 */
void MSH_print_vois_odic(s_cmsh *pcmsh, FILE *fpout) {
  int dir_odic;
  int nele_tot, beg_idir, end_idir, beg_itype, end_itype;
  int i, j, nlayer, dim_neigh, k;
  int **neigh;
  char nom_fichier[STRING_LENGTH_LP];
  FILE *fichier;
  int bck_space = 0;

  LP_printf(fpout, "Printing eaudyssee neighboor files\n ");

  nele_tot = 0;
  nlayer = pcmsh->pcount->nlayer;
  for (i = 1; i < nlayer; i++) {
    nele_tot += pcmsh->p_layer[i]->nele;
  }
  LP_printf(fpout, "neletot : %d\n", nele_tot);
  for (dir_odic = 0; dir_odic < NODIC_MSH; dir_odic++) {
    if (dir_odic == HOR) {
      LP_printf(fpout, "Printing VOIS_HOR\n ");
      sprintf(nom_fichier, "%s/VOIS_HOR.txt", getenv("RESULT"));
      fichier = fopen(nom_fichier, "w");
      dim_neigh = 8;
      beg_idir = Y;    // init 0
      end_idir = -1;   // init Z
      beg_itype = TWO; // init 0
      end_itype = -1;  // init NFACE_MSH
    } else {
      beg_idir = Z_MSH; // init Z
      end_idir = Y;     // init ND_MSH
      if (dir_odic == INF) {
        LP_printf(fpout, "Printing VOIS_INF\n ");
        sprintf(nom_fichier, "%s/VOIS_INF.txt", getenv("RESULT"));
        dim_neigh = 4;
        beg_itype = ONE; // init ONE
        end_itype = -1;  // init TWO
      }
      if (dir_odic == SUP) {
        LP_printf(fpout, "Printing VOIS_SUP\n ");
        sprintf(nom_fichier, "%s/VOIS_SUP.txt", getenv("RESULT"));
        dim_neigh = 4;
        beg_itype = TWO; // init TWO
        end_itype = 0;   // init NFACE_MSH
      }
    }
    fichier = fopen(nom_fichier, "w");
    neigh = MSH_create_neigh_tab(pcmsh, dir_odic, nele_tot, dim_neigh, beg_idir, end_idir, beg_itype, end_itype, fpout);
    bck_space = 0;

    for (j = 0; j < dim_neigh; j++) {
      for (i = 0; i < nele_tot; i++) {
        fprintf(fichier, "%8d ,", neigh[i][j]);
        bck_space++;
        if (bck_space % 10 == 0)
          fprintf(fichier, "\n");
      }
    }
    fprintf(fichier, "\n");
    fclose(fichier);
  }
}

/**\fn void MSH_print_vois(FILE *fichier,s_cmsh *pcmsh,int idir, int itype,int bck_space,FILE *fpout)
 *\brief print neighboor in eaudyssee format for each element in each directions
 *
 */
int **MSH_create_neigh_tab(s_cmsh *pcmsh, int dir_odic, int nele_tot, int dim_neigh, int beg_idir, int end_idir, int beg_itype, int end_itype, FILE *fpout) {
  int idir, itype;
  int nlayer, nele, pos_ele;
  int i, j, k, l, m;
  int **neigh;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_id_io *pid;
  s_ele_msh *pneigh;
  nlayer = pcmsh->pcount->nlayer;

  neigh = ((int **)malloc(nele_tot * sizeof(int *)));
  for (l = 0; l < nele_tot; l++) {
    neigh[l] = ((int *)malloc(dim_neigh * sizeof(int)));
    bzero((char *)neigh[l], dim_neigh * sizeof(int));
  }
  pos_ele = 0;
  for (i = 1; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];

      // LP_printf(fpout,"pos_ele %d ",pos_ele); //BL to debug
      k = 0;
      for (idir = beg_idir; idir > end_idir; idir--) {

        for (itype = beg_itype; itype > end_itype; itype--) {

          pid = pele->p_neigh[idir][itype];
          if (pid != NULL)
            pid = IO_browse_id(pid, END_TS);

          while (pid != NULL) {
            pneigh = pcmsh->p_layer[pid->id_lay]->p_ele[pid->id];
            // LP_printf(fpout,"k%d ",k);//BL to debug
            if (pneigh->player->id != SURF_MSH) {
              if (pos_ele > (nele_tot - 1) || k > (dim_neigh - 1))
                LP_error(fpout, "dépassement de tableau : pos_ele %d nele_tot %d k %d dim_neigh %d !!\n", pos_ele, nele_tot, k, dim_neigh);
              neigh[pos_ele][k] = MSH_get_ele_id(pneigh, ABS_MSH);
            } else {
              if (pos_ele > (nele_tot - 1) || k > (dim_neigh - 1))
                LP_error(fpout, "dépassement de tableau : pos_ele : %d nele_tot %d k %d dim_neigh %d !!\n", pos_ele, nele_tot, k, dim_neigh);
              neigh[pos_ele][k] = (-MSH_get_ele_id(pneigh, ABS_MSH));
            }
            k++;

            pid = pid->prev;
          }
        }
      }
      // LP_printf(fpout,"\n"); //to debug
      pos_ele++;
    }
  }
  return neigh;
}

/**\fn void MSH_definir_code_gigogne(s_cmsh *pcmsh,FILE *fpout)
 *\brief define code gigogne in mesh
 *
 */
void MSH_definir_code_gigogne(s_cmsh *pcmsh, FILE *fpout) {
  s_ele_msh *pele, *peleref;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  int i, j;
  double decal = 1.;
  double num_case[Z_MSH];
  double num_case_int[Z_MSH];
  int n;
  int nlayer;
  double errormin;
  double errormax;
  double crit;
  double eps;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    pele = pcmsh->p_layer[i]->pele;
    while (pele != NULL) {
      pdescr = pele->pdescr;
      n = pdescr->rapport_cote;
      pdescr->code_gigogne = ((int *)malloc(n * sizeof(int)));
      bzero((char *)pdescr->code_gigogne, sizeof(int));
      for (j = 0; j < Z_MSH; j++) {
        errormin = pele->pdescr->center[j] - pcmsh->MINI[j];
        errormax = pele->pdescr->center[j] - pcmsh->MAXI[j];
        eps = EPS_MSH;
        if ((errormax > eps) || (errormin < -eps)) {
          printf("errormin = %g , errormax = %g, compared to %g\n", errormin, errormax, eps);
          if (errormax > eps)
            LP_error(fpout, "In libmesh%4.2f: Element %d out of the reference grid from the top or right d%s = %g m \n Please contact the program developper Nicolas.Flipo@mines-paristech.fr\n", VERSION_MSH, pele->id[GIS_MSH], MSH_name_direction(j, fpout), errormax);
          else
            LP_error(fpout, "In libmesh%4.2f: Element %d out of the reference grid from the bottom or left d%s = %g m \n Please contact the program developper Nicolas.Flipo@mines-paristech.fr\n", VERSION_MSH, pele->id[GIS_MSH], MSH_name_direction(j, fpout), errormin);
        }
        num_case[j] = fabs(pele->pdescr->center[j] - pcmsh->Haut_g[j]);
        num_case[j] /= pcmsh->l_max;
        num_case_int[j] = num_case[j];

      } // for j
      peleref = pcmsh->pele_ref[i][(int)num_case[Y]][(int)num_case[X_MSH]];
      peleref->nele++;
      peleref->nratio = TS_max(peleref->nratio, n - 1);
      pele->pele_ref = peleref;

      decal = 0.5;
      for (j = 0; j < Z_MSH; j++) {
        num_case[j] -= (int)num_case[j];
        num_case[j] -= decal;
      }

      for (j = 0; j < n; j++) {
        decal *= 0.5;
        if (num_case[X_MSH] < -EPS_MSH) {
          if (num_case[Y] > EPS_MSH)
            pdescr->code_gigogne[j] = 3;
          if (num_case[Y] < -EPS_MSH) // on ne peut pas avoir X non nul et Y nul
            pdescr->code_gigogne[j] = 1;
        } else {
          if (num_case[X_MSH] > EPS_MSH) {
            if (num_case[Y] > EPS_MSH)
              pdescr->code_gigogne[j] = 4;
            if (num_case[Y] < -EPS_MSH)
              pdescr->code_gigogne[j] = 2;
          } else {
            pdescr->code_gigogne[j] = 0; // la maille equivaut a la maille du schema type
          }
        }
        switch (pdescr->code_gigogne[j]) {
        case 1: // coin haut gauche
          num_case[X_MSH] += decal;
          num_case[Y] += decal;
          break;
        case 2: // coin haut droit
          num_case[X_MSH] -= decal;
          num_case[Y] += decal;
          break;
        case 3: // coin bas gauche
          num_case[X_MSH] += decal;
          num_case[Y] -= decal;
          break;
        case 4: // coin bas droit
          num_case[X_MSH] -= decal;
          num_case[Y] -= decal;
          break;
        }
      }

      pele = pele->next;
    } // while pele
    LP_printf(fpout, "code gigogne calcule pour chaque maille de la couche %d : %s\n", i, pcmsh->p_layer[i]->name);
  } // for i
}

int intersection_oui_non(double minim, double maxim, double valeur) {
  int vrai_faux = NO_MSH;

  maxim -= EPS_MSH;
  minim += EPS_MSH;

  if ((valeur < maxim) && (valeur > minim))
    vrai_faux = YES_MSH;
  return vrai_faux;
}
/**\fn void  MSH_fit_ref_grid(int ind_coord,int compte,double val_coord,double ratio,int chevauchement,s_cmsh *pcmsh,FILE *fpout)
 *\brief fit ref grid on mesh
 *
 */
int MSH_fit_ref_grid(int ind_coord, int compte, double val_coord, double ratio, int chevauchement, s_cmsh *pcmsh, FILE *fpout) {
  int j, k;
  s_ele_msh *pele;
  double point;
  double compare[NUPDWN_MSH];
  double chevauche;
  int ncouche;
  double l_min, l_max;

  ncouche = pcmsh->pcount->nlayer;
  l_min = pcmsh->l_min;
  l_max = pcmsh->l_max;
  chevauche = chevauchement;
  point = val_coord;

  pcmsh->compteur[ind_coord] = compte;
  pcmsh->MINI[ind_coord] = val_coord;

  printf("entree dans calage_grille_ref() coin bas %f l_min %f compteur %d / %d\n", val_coord, pcmsh->l_min, pcmsh->compteur[ind_coord], compte);

  while ((pcmsh->compteur[ind_coord] < pcmsh->dimension) && (chevauche != NO_MSH)) {
    for (j = ratio; j > 0; j--) {
      point = pcmsh->MINI[ind_coord] + j * pcmsh->l_max;
      for (k = 0; k < ncouche; k++) {

        pele = pcmsh->p_layer[k]->pele;
        while (pele != NULL) {
          if (pele->pdescr->l_side > l_min) {
            compare[DOWN_MSH] = pele->pdescr->coord[3][ind_coord]; // valeur inf de la maille (en X ou Y suivant la valeur de coord)
            compare[UP_MSH] = pele->pdescr->coord[1][ind_coord];   // valeur sup de la maille (en X ou Y suivant la valeur de coord)
            if (intersection_oui_non(compare[DOWN_MSH], compare[UP_MSH], point) == NO_MSH) {
              pele = pele->next;
              chevauche = NO_MSH;
            } else {
              printf("rebouclage pour coord %d avec compteur = %d et dimension %d\n", ind_coord, pcmsh->compteur[ind_coord], pcmsh->dimension);
              printf("ancien coin %f\n", pcmsh->MINI[ind_coord]);
              pcmsh->MINI[ind_coord] += l_min;
              printf("nveau coin %f\n", pcmsh->MINI[ind_coord]);
              pcmsh->compteur[ind_coord] += 1;
              chevauche = MSH_fit_ref_grid(ind_coord, pcmsh->compteur[ind_coord], pcmsh->MINI[ind_coord], ratio, YES_MSH, pcmsh, fpout);

              if (chevauche == NO_MSH) {
                return chevauche;
                break;
              }
            }
          } else
            pele = pele->next;
        }
      }
    }
  }
  return chevauche;
}

/**\fn s_ele_msh ***MSH_define_ref_grid(int dim[Z_MSH],s_cmsh *pcmsh,FILE *fpout)
 *\brief define the reference grid
 *
 * The reference grid starts its numbering from the top left corner. Then from left to right per row
 */
s_ele_msh ****MSH_define_ref_grid(int dim[Z_MSH], s_cmsh *pcmsh, FILE *fpout) {

  s_ele_msh ****grille;
  int i, j, k = 0, layer;
  int m, nlayer;
  int counter = 0;
  s_descr_msh *pdescr;
  double *MINI, *MAXI, l_min, l_max;

  MINI = pcmsh->MINI;
  // MAXI=pcmsh->MAXI;
  l_min = pcmsh->l_min;
  l_max = pcmsh->l_max;
  nlayer = pcmsh->pcount->nlayer;
  grille = ((s_ele_msh ****)malloc(nlayer * sizeof(s_ele_msh ***)));
  bzero((char *)grille, sizeof(s_ele_msh ***));
  for (layer = 0; layer < nlayer; layer++) {
    counter = 0;
    grille[layer] = ((s_ele_msh ***)malloc(dim[Y] * sizeof(s_ele_msh **)));
    bzero((char *)grille[layer], sizeof(s_ele_msh **));

    for (i = 0; i < dim[Y]; i++) {
      grille[layer][i] = ((s_ele_msh **)malloc(dim[X_MSH] * sizeof(s_ele_msh *)));
      bzero((char *)grille[layer][i], sizeof(s_ele_msh *));
      for (j = 0; j < dim[X_MSH]; j++) {
        grille[layer][i][j] = MSH_create_element(counter++, REFERENCE);
        grille[layer][i][j]->n[Y] = i;
        grille[layer][i][j]->n[X_MSH] = j;
        grille[layer][i][j]->id[INTERN_MSH] = grille[layer][i][j]->id[GIS_MSH];
        pdescr = grille[layer][i][j]->pdescr;
        pdescr->coord[0][X_MSH] = MINI[X_MSH] + j * l_max;
        pdescr->coord[0][Y] = MINI[Y] + (dim[Y] - i) * l_max;
        pdescr->coord[1][X_MSH] = MINI[X_MSH] + (j + 1) * l_max;
        pdescr->coord[1][Y] = MINI[Y] + (dim[Y] - i) * l_max;
        pdescr->coord[2][X_MSH] = MINI[X_MSH] + (j + 1) * l_max;
        pdescr->coord[2][Y] = MINI[Y] + (dim[Y] - (i + 1)) * l_max;
        pdescr->coord[3][X_MSH] = MINI[X_MSH] + j * l_max;
        pdescr->coord[3][Y] = MINI[Y] + (dim[Y] - (i + 1)) * l_max;
        for (m = 0; m < Z_MSH; m++) {
          pdescr->center[m] = pdescr->coord[0][m] + pdescr->coord[2][m]; // BL init pdescr->coord[0][m]+pdescr->coord[3][m] ne donnait pas le centre de la maille mais les coordonnées du milieu du coté gauche de la maille
          pdescr->center[m] /= 2;
        } // for m
      }   // for j
    }     // for i
  }       // for layer
  for (i = 0; i < Z_MSH; i++) {
    pcmsh->MAXI[i] = MINI[i] + dim[i] * l_max; // NF 20/11/2013 recalculating maxi corner
    pcmsh->dim[i] = dim[i];
  }
  pcmsh->Haut_g[X_MSH] = grille[0][0][0]->pdescr->coord[0][X_MSH];
  pcmsh->Haut_g[Y] = grille[0][0][0]->pdescr->coord[0][Y];

  LP_printf(fpout, "Ending with:\n");
  LP_printf(fpout, "\tBottom Left (%d,%d)->(%f,%f)\n", 0, 0, grille[0][dim[Y] - 1][0]->pdescr->coord[3][X_MSH], grille[0][dim[Y] - 1][0]->pdescr->coord[3][Y]);
  LP_printf(fpout, "\tTop Left on grille (%f,%f)\n", pcmsh->Haut_g[X_MSH], pcmsh->Haut_g[Y]);
  LP_printf(fpout, "\tTop Left based on maxi mini (%f,%f)\n", pcmsh->MINI[X_MSH], pcmsh->MAXI[Y]);
  LP_printf(fpout, "\tTop Right  (%d,%d)->(%f,%f)\n", dim[Y] - 1, dim[X_MSH] - 1, grille[0][0][dim[X_MSH] - 1]->pdescr->coord[1][X_MSH], grille[0][0][dim[X_MSH] - 1]->pdescr->coord[1][Y]);
  // copy layer_grid for each layers.

  return grille;
}
/**\fn void MSH_print_ref_grid(s_cmsh *pcmsh,FILE *fpout)
 *\brief print reference grid
 *
 */
void MSH_print_ref_grid(s_cmsh *pcmsh, FILE *fpout) {
  char namef[STRING_LENGTH_LP];
  FILE *fp;
  int nele;
  int i, j;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  s_ele_msh *pele;

  sprintf(namef, "%s/INFO_SOUT/ref_grid.dat", getenv("RESULT"));
  fp = fopen(namef, "w");

  play = pcmsh->play_ref[0];
  nele = play->nele;
  for (i = 0; i < nele; i++) {
    pele = play->p_ele[i];
    pdescr = pele->pdescr;
    fprintf(fp, "%d AUTO\n", pele->id[GIS_MSH]);
    for (j = 0; j < NCORNER_MSH; j++)
      fprintf(fp, "\t%f,%f\n", pdescr->coord[j][X_MSH], pdescr->coord[j][Y]);

    fprintf(fp, "\t%f,%f\n", pdescr->coord[0][X_MSH], pdescr->coord[0][Y]);
    fprintf(fp, "END\n");
    // fprintf(fp,"CENTER : \t%f,%f\n",pdescr->center[X_MSH],pdescr->center[Y]); //BL to debug
  }
  fprintf(fp, "END\n");
  fclose(fp);
}

/** @fn s_layer_msh *MSH_generate_reference_layer(s_cmsh *pcmsh,int *nele,s_ele_msh ***pele,FILE *fpout)
 * @brief generates for each layers a squared grid of nele[X_MSH]*nele[Y] with the name "Reference" and fills the layer with pele, which size must be nele[X_MSH]*nele[Y]
 *
 * @arg int *nele: number of elements following direction X_MSH and Y
 * @arg s_ele_msh ****pele:  pointers array to element's pointers organised following layer, X_MSH and Y
 * @arg int nlayer: number of layers
 * @arg FILE *fpout: pointer to log file
 * @return a layer structure containing the reference elements of pele
 **/
s_layer_msh **MSH_generate_reference_layer(int *nele, s_ele_msh ****pele, int nlayer, FILE *fpout) {
  s_layer_msh **p_lay;
  int count;
  int i, j, k;

  p_lay = (s_layer_msh **)malloc(nlayer * sizeof(s_layer_msh *));
  for (k = 0; k < nlayer; k++) {
    p_lay[k] = MSH_create_layer("Reference", 0);
    p_lay[k]->nele = nele[X_MSH] * nele[Y];
    p_lay[k]->p_ele = (s_ele_msh **)malloc(p_lay[k]->nele * sizeof(s_ele_msh *));
    count = 0;

    for (i = 0; i < nele[Y]; i++) {
      for (j = 0; j < nele[X_MSH]; j++) {

        p_lay[k]->p_ele[count] = pele[k][i][j];

        // LP_printf(fpout,"p_lay[%d]->p_ele[%d] is getting ele pele[%d][%d][%d] of id %d\n",k,count,k,i,j,MSH_get_ele_id(pele[k][i][j],GIS_MSH));
        count++;
      }
    }
  }
  return p_lay;
}
/**\fn int MSH_is_ele_a_ref(s_ele_msh *pele)
 *\brief check if element is a ele ref
 *
 */
int MSH_is_ele_a_ref(s_ele_msh *pele, s_cmsh *pcmsh) {
  int answer = NO_MSH;
  int *gigo;
  int n;
  double l_side, Lref;
  l_side = pele->pdescr->l_side;
  Lref = pcmsh->l_max;
  n = MSH_get_ele_order(pele);
  gigo = MSH_get_ele_gigogne(pele);
  if ((n == 1) && (fabs(l_side - Lref) < EPS_MSH))
    answer = YES_MSH;
  return answer;
}

/**\fn int MSH_is_ele_a_ref(s_ele_msh *pele)
 *\brief function to order element in ref
 *
 *this function also creates subelement in reference element
 *\return ordered referencing mesh
 */
void MSH_order_element_in_ref(s_cmsh *pcmsh, FILE *fpout) {

  int i, j, n, count, idir, itype, k;
  s_ele_msh *pele, *peleref, *ptmp;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  int nlayer;
  int code_gigo;
  int *gigo;
  int id = IDSUBFACE_MSH;
  // LP_printf(fpout,"entering order element in ref \n \n");
  nlayer = pcmsh->pcount->nlayer;
  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    for (j = 0; j < play->nele; j++) {
      pele = play->p_ele[j];
      peleref = pele->pele_ref;
      // if(peleref->id[INTERN_MSH]==305)
      // LP_printf(fpout,"For Debugging only\n");
      n = MSH_get_ele_order(pele);
      gigo = MSH_get_ele_gigogne(pele);
      count = 0;
      if (gigo[0] > 0) // then element is not a ref_element
      {
        while (count < n) {
          if (peleref->p_subele == NULL) {
            // peleref->p_subele=(s_ele_msh**)calloc(NCORNER_MSH,sizeof(s_ele_msh*)); //BL init

            peleref->p_subele = (s_ele_msh **)malloc(NCORNER_MSH * sizeof(s_ele_msh *)); // modifie calloc en malloc car cela a plante chez laurianne. plus lourd car doit un bzero pour initialiser les pointeurs à NULL
            for (k = 0; k < NCORNER_MSH; k++)
              peleref->p_subele[k] = NULL;
          }
          code_gigo = gigo[count];
          // LP_printf(fpout," code_gigo %i\n",code_gigo); //BL to debug
          // LP_printf(fpout,"enter convert_gigogne for ele GIS : %i ABS : %i in layer %i\n",MSH_get_ele_id(pele,GIS_MSH),MSH_get_ele_id(pele,ABS_MSH),i); //BL to debug
          ptmp = MSH_get_subele(peleref, MSH_convert_gigogne_dir(code_gigo, fpout));
          if (ptmp == NULL) // then no subele have been created yet
          {
            ptmp = MSH_create_element(id++, SUBREF); // subface numbured from IDSUBFACE_MSH
            ptmp->id[INTERN_MSH] = ptmp->id[GIS_MSH];
            ptmp->pdescr->code_gigogne = (int *)malloc(sizeof(int));
            ptmp->pdescr->code_gigogne[0] = code_gigo;
            // ptmp->pdescr->rapport_cote=MSH_get_ele_order(pele);// BL init
            ptmp->pdescr->rapport_cote = MSH_get_ele_order(peleref) + 1; // pour que l'order des subeles augmente et ne soit pas directement égal à celui de l'ele sinon on peut se perdre dans la descente vers pele...
            // LP_printf(fpout,"enter convert_gigogne in ptmp=NULL for ele %i in layer %i\n",j,i); //BL to debug
            peleref->p_subele[MSH_convert_gigogne_dir(code_gigo, fpout)] = ptmp;
          }
          /* else{
            if (count>0)
              peleref->nele++;
              }*/
          ptmp = peleref;
          peleref = peleref->p_subele[MSH_convert_gigogne_dir(code_gigo, fpout)];
          // peleref=(s_ele_msh*)calloc(1,sizeof(s_ele_msh));
          peleref->nele++;
          count++;
        }

        ptmp->p_subele[MSH_convert_gigogne_dir(code_gigo, fpout)] = MSH_destroy_ele(ptmp->p_subele[MSH_convert_gigogne_dir(code_gigo, fpout)]);
        ptmp->p_subele[MSH_convert_gigogne_dir(code_gigo, fpout)] = pele;
        // peleref=pele;
      } else // then element is a ref_element
      {
        peleref->p_subele = (s_ele_msh **)malloc(sizeof(s_ele_msh *)); // BL init

        peleref->p_subele[0] = pele;
        // peleref->nele++;
        if (MSH_is_ele_a_ref(pele, pcmsh) == YES_MSH) {
          peleref->ndiv = 1;
          /*  for(idir=0;idir<Z_MSH;idir++){
            for(itype=0;itype<NFACE_MSH;itype++){
              pele->face[idir][itype]=peleref->face[idir][itype];
                }
                }*/
        }
      }
    }
    /*   if(pele->pele_ref->nele==CODE_MSH)
        pele->pele_ref->nele = 1;
        else pele->pele_ref->nele++;*/
  }
}

/**\fn void MSH_check_element_ordering(s_cmsh *pcmsh,FILE *fpout)
 *\brief check element ordering
 *
 *Verify if the number of sub element and element in ref mesh is consistant whith the mesh on wich calculation will be done.
 */
void MSH_check_element_ordering(s_cmsh *pcmsh, FILE *fpout) {

  int i, j, n, count, k;
  s_ele_msh *pele, *peleref, *psub;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  int neleref, nele, neletot = 0, nsub;
  int code_gigo;
  int *gigo;
  int nlayer;
  LP_printf(fpout, "libmesh%4.2f: Checking Element referencing consistency:\n", VERSION_MSH);
  nlayer = pcmsh->pcount->nlayer;
  for (k = 0; k < nlayer; k++) {
    LP_printf(fpout, "------------------------LAYER : %s,%d---------------------\n", pcmsh->p_layer[k]->name, k);
    // neletot=0;
    play = pcmsh->play_ref[k];
    neleref = play->nele;
    for (i = 0; i < neleref; i++) {
      peleref = play->p_ele[i];
      nele = peleref->nele;
      code_gigo = 0;
      // LP_printf(fpout,"\tRef element %d with ratio %d contains %d subelements\n",MSH_get_ele_id(peleref,INTERN_MSH),peleref->nratio,nele);
      //  LP_printf(fpout,"\tRef element %d contains %d subelements:",MSH_get_ele_id(peleref,INTERN_MSH),nele); // BL to debug
      neletot += nele;
      /*   if(peleref->id[INTERN_MSH]==305)
           printf("For Debugging only\n");*/
      if (nele > 1) {
        nsub = 0;
        psub = NULL;
        for (j = 0; j < NCORNER_MSH; j++) {
          psub = peleref->p_subele[j];
          if (psub != NULL)
            nsub += psub->nele;
        }
        if (nsub != nele)
          LP_error(fpout, " number of subelements per element is inconsistent: %d instead of %d\n", nsub, nele);
        // else //BL to debug
        // LP_printf(fpout," number subelements per element of order 2 is consistent\n",nsub,nele); //BL to debug
      }
      /*
      else {
        if(nele>0){
          if(peleref->ndiv==1)
                    LP_printf(fpout," It is a grid element of order 1 itself\n"); //BL to debug
          else
            LP_printf(fpout," ndiv=%d --> contains smaller elements\n",peleref->ndiv); // BL to debug
        }
        else
          LP_printf(fpout," Empty\n"); // BL to debug
      }
      */ // BL to debug
    } // for i

  } // for k
  if (neletot != pcmsh->pcount->nele)
    LP_error(fpout, "Number of accessible elements through the reference grid %d is not equal to the total number of elements %d\n", neletot, pcmsh->pcount->nele);
  else
    LP_printf(fpout, "%d elements accessible through the reference grid\n", neletot);
}
/**\fn int MSH_is_ele_a_ref(s_ele_msh *pele)
 *\brief A FAIRE
 *
 * A FAIRE
 */
void MSH_link_neighbour(s_cmsh *pcmsh, FILE *fpout) {

  int i, j, n;
  s_ele_msh *pele, *peleref;
  s_layer_msh *play;
  s_descr_msh *pdescr;
  int nlayer;
  int gigo;

  nlayer = pcmsh->pcount->nlayer;
  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    for (j = 0; j < play->nele; j++) {
    }
  }
}

/**\fn void MSH_referencing_mesh(s_cmsh *pcmsh,FILE *fpout)
 *\brief create the referencing mesh in a new layer called play_ref in cmsh
 *
 * \arg pcmsh : is the structure wrapping all layers
 */
void MSH_referencing_mesh(s_cmsh *pcmsh, FILE *fpout) {
  int i, j, k;
  double ratio[Z_MSH];
  double l_min, l_max;
  int ratio_int[Z_MSH];
  double reste[Z_MSH];
  int chevauche = YES_MSH;
  char nom_force[STRING_LENGTH_LP];
  FILE *fp;
  char code_newsam[STRING_LENGTH_LP];

  // 1. Defining the corners and number of elements of the squared reference mesh layer*/

  LP_printf(fpout, "**********Starting referencing the mesh************\n");
  l_max = pcmsh->l_max;
  for (i = 0; i < Z_MSH; i++) {
    pcmsh->MINI[i] = pcmsh->mini[i];
    // pcmsh->MAXI[i]=pcmsh->maxi[i];
    ratio[i] = (pcmsh->maxi[i] - pcmsh->mini[i]) / l_max;
    ratio_int[i] = (int)ratio[i];
    reste[i] = ratio[i] - ratio_int[i];
    /*if(reste[i]>EPS)
      pcmsh->MAXI[i]=pcmsh->MINI[i]+l_max*(ratio_int[i]+1);
      else pcmsh->MAXI[i]=pcmsh->MINI[i];*/
    LP_printf(fpout, "ratio[%d] %f\n", i, ratio[i]);
  }

  if (pcmsh->dimension > (1 + EPS_MSH)) // on doit caler la grille de ref seulement dans le cas de mailles gigogne
  {
    for (i = 0; i < Z_MSH; i++) {
      pcmsh->compteur[i] = 0;
      chevauche = MSH_fit_ref_grid(i, pcmsh->compteur[i], pcmsh->mini[i], ratio[i], YES_MSH, pcmsh, fpout);
      // printf ("MINI[%d] %f \n",i,MINI[i]);
      // printf ("mini[%d] %f \n",i,mini[i]);
      if (pcmsh->compteur[i] > 0) {
        pcmsh->MINI[i] -= pcmsh->l_max;
        pcmsh->compteur[i]--; // definit le nb de mailles intercalees
        ratio_int[i]++;
      } else {
        if (reste[i] > EPS_MSH)
          ratio_int[i]++;
      }
      pcmsh->MAXI[i] = pcmsh->MINI[i] + l_max * ratio_int[i];
      printf("\n\n*********Reference grid coordinate %d found : MINI[%d] = %f*******************\n", i, i, pcmsh->MINI[i]);
      printf(" \t\tnumber of steps %d \n \n", pcmsh->compteur[i]);
    }
  } else {
    for (i = 0; i < Z_MSH; i++) {
      pcmsh->compteur[i] = 0;
      pcmsh->MINI[i] = pcmsh->mini[i];
      pcmsh->MAXI[i] = pcmsh->maxi[i];
    }
  }

  LP_printf(fpout, "libmesh%4.2f Finding the reference grid : \n", VERSION_MSH);
  LP_printf(fpout, "Starting with:\n\tBottom Left corner ( %f , %f )\n", pcmsh->MINI[X_MSH], pcmsh->MINI[Y]);

  // 2. Creating the reference layer
  for (i = 0; i < Z_MSH; i++)
    ratio_int[i] += 1; // NF 20/11/2013 i add one column and one row which are missing for whatever reason
  pcmsh->pele_ref = MSH_define_ref_grid(ratio_int, pcmsh, fpout);
  // Creating reference faces
  pcmsh->pfaces_ref = MSH_define_ref_faces(ratio_int, pcmsh, fpout);

  // copie des ele_ref pour chacuns des layers

  pcmsh->play_ref = MSH_generate_reference_layer(ratio_int, pcmsh->pele_ref, pcmsh->pcount->nlayer, fpout);

  LP_printf(fpout, "Reference grid defined\n");
  MSH_definir_code_gigogne(pcmsh, fpout);

  LP_printf(fpout, "Linking reference elements\n");
  // Linking reference elements
  MSH_link_ele_ref(pcmsh, fpout);

  // 3. Printing the reference layer
  system("mkdir $RESULT/INFO_SOUT/");
  MSH_print_ref_grid(pcmsh, fpout);
}
