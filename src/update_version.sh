#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libmesh
# FILE NAME: update_version.sh
# 
# CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS, 
#               Deniz KILIC
# 
# LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh 
# of squared cells.
#
# Library developed at the Geosciences Center, joint research center 
# of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
#
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------*/


#Automatic change of the version number
#> update_version.sh <new_version_number>

#gawk -f update_makefile.awk -v nversion=$1 Makefile
#mv awk.out Makefile

gawk -f update_param.awk -v nversion=$1 param_MSH.h
mv awk.out param_MSH.h

#gawk -f update_doxygen.awk -v nversion=$1 test_doxygen
#mv awk.out test_doxygen

echo "Version number updated in param.h and doxygen cmd file to" $1
