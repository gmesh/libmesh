/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_output.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"

void MSH_export_geom_surface_layer(s_cmsh *pcmsh, FILE *fp) {
  int nlayer, nele;
  s_ele_msh *pele;
  s_layer_msh *player;
  int l, e;
  FILE *layer_surf, *id_surf;
  char *name;

  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/geom_layer_surf.dat", getenv("RESULT"));
  layer_surf = fopen(name, "w");

  if (layer_surf == NULL)
    LP_error(fp, "Cannot open file %s \n", name);
  free(name);

  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/id_layer_surf.dat", getenv("RESULT"));
  id_surf = fopen(name, "w");
  if (id_surf == NULL)
    LP_error(fp, "Cannot open file %s \n", name);
  free(name);
  fprintf(id_surf, "ID_LAYER ID_INTERN ID_GIS ID_ABS\n");
  nlayer = pcmsh->pcount->nlayer;
  for (l = 0; l < nlayer; l++) {
    player = pcmsh->p_layer[l];
    nele = player->nele;
    for (e = 0; e < nele; e++) {
      pele = player->p_ele[e];

      if (pele->loc == TOP_MSH) {

        MSH_print_geom_ele_surf(pele, layer_surf);
        MSH_print_id_ele_surf(pele, id_surf);
      }
    }
  }

  fprintf(layer_surf, "END\n");
  fclose(layer_surf);
  fclose(id_surf);
}

void MSH_print_geom_ele_surf(s_ele_msh *pele, FILE *fp) {

  s_descr_msh *pdescr;
  int j;
  pdescr = pele->pdescr;
  fprintf(fp, "%d AUTO\n", pele->id[GIS_MSH]);
  for (j = 0; j < NCORNER_MSH; j++)
    fprintf(fp, "\t%f,%f\n", pdescr->coord[j][X_MSH], pdescr->coord[j][Y]);

  fprintf(fp, "\t%f,%f\n", pdescr->coord[0][X_MSH], pdescr->coord[0][Y]);
  fprintf(fp, "END\n");
}

void MSH_print_id_ele_surf(s_ele_msh *pele, FILE *fout) { fprintf(fout, "%d %d %d %d\n", pele->player->id, pele->id[INTERN_MSH], pele->id[GIS_MSH], pele->id[ABS_MSH]); }
