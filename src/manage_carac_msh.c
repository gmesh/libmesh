/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: manage_carac_msh.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"
/**\fn s_ele_msh *MSH_pick_ele_ref(s_cmsh *pcmsh,int nx,int ny,FILE *fpout)
 *\brief select element by their position in mesh
 *
 *
 */
s_ele_msh *MSH_pick_ele_ref(s_cmsh *pcmsh, int nx, int ny, int layer, FILE *fpout) {
  int i, j;
  return pcmsh->pele_ref[layer][ny][nx];
}
/**\fn void  MSH_link_ele_ref(s_cmsh *pcmsh,FILE *fpout)
 *\brief link reference elements through their faces
 *
 * une face lie deux éléments entre eux via leur direction (X ou Y) et
 * leurs positions par rapport à X ou Y (ONE ou TWO)
 * le sens du lien est inverse selon X et Y d'ou le bug entre Nord et Sud.
 */
void MSH_link_ele_ref(s_cmsh *pcmsh, FILE *fpout) {
  int i, j, *dim, k, ll;
  int I, J, LAYER; // to debug
  int ii, jj;
  int layer;
  int nlayer;
  s_ele_msh ***p_ele;
  s_face_msh ****p_faces;
  s_ele_msh *pele;
  s_face_msh *pface;
  nlayer = pcmsh->pcount->nlayer;
  for (layer = 0; layer < nlayer; layer++) {
    p_ele = pcmsh->pele_ref[layer];
    p_faces = pcmsh->pfaces_ref[layer];
    dim = pcmsh->dim;

    LP_printf(fpout, "Linking reference elements in layer %d : %s \n", layer, pcmsh->p_layer[layer]->name);
    /*  LP_printf(fpout,"********************-DEBUG-***********************\n");//BL to debug */
    for (i = 0; i < dim[Y]; i++) { // Je corrige
      for (j = 0; j < dim[X_MSH]; j++) {
        pele = p_ele[i][j];
        for (ll = X_MSH; ll < Z_MSH; ll++) {
          for (k = ONE; k < NFACE_MSH; k++) {

            I = i + ll * MSH_switch_direction(k, fpout); // to debug
            J = j + MSH_switch_direction(ll, fpout) * k; // to debug
            LAYER = layer;
            /* if(ll==Z)
              {
                if(layer>0 && layer<nlayer)
                  {
                    p_faces=pcmsh->pfaces_ref[layer+(1-k*ll)];
                    LAYER=layer+(1-k*ll);
                  }
                    I=i;
                    J=j;


                    }*/

            pele->face[ll][k] = p_faces[I][J][ll];

            //  pele->face[ll][k]=p_faces[i+ll*MSH_switch_direction(k,fpout)][j+MSH_switch_direction(ll,fpout)*k][ll]; //BL 23/05/14 switch(direction(k) car numerotation commence en haut à gauche.

            /*

        LP_printf(fpout,"face p_ele[%i][%i] type %s dir %s\n",i,j,MSH_name_direction(ll,fpout),MSH_name_type(k,fpout));//BL to debug
        LP_printf(fpout,"get face[%i][%i][%s] of layer %i\n\n",I,J,MSH_name_direction(ll,fpout),LAYER);//BL to debug
            */
            /*Suppose faire
            pele->face[X_MSH][ONE]=p_faces[i][j+ONE][X_MSH]
            pele->face[X_MSH][TWO]=p_faces[i][j+TWO][X_MSH]
            pele->face[Y][ONE]=p_faces[i+ONE][j][Y]
            pele->face[Y][TWO]=p_faces[i+TWO][j][Y] */
            pface = pele->face[ll][k];
            if (pface == NULL & ll != Z_MSH)
              LP_error(fpout, "In libmesh: %s l.%d: faces not allocated yet\n", __FILE__, __LINE__);
            pface->p_ele[MSH_switch_direction(k, fpout)] = pele;
          } // for k

        } // for ll

      } // for j

    } // for i
  }   // for layer
}

/**\fn void MSH_check_redundancy(s_cmsh *pcmsh,FILE *fp)
 *\brief check if their is same element in layers.
 *
 *
 */
void MSH_check_redundancy(s_cmsh *pcmsh, FILE *fp) {
  int nlay, i;
  s_layer_msh *play;

  nlay = pcmsh->pcount->nlayer;
  LP_printf(fp, "\tChecking element redundancies\n", nlay);
  for (i = 0; i < nlay; i++) {
    play = pcmsh->p_layer[i];
    MSH_check_layer_redundancy(play, fp);
  }
}
/**\fn void MSH_carac_mesh(s_cmsh *pcmsh,FILE *fp)
 *\brief print mesh caracteristics
 *
 *
 */
void MSH_carac_mesh(s_cmsh *pcmsh, FILE *fp) {
  int nlay, i;
  s_layer_msh *play;

  nlay = pcmsh->pcount->nlayer;
  LP_printf(fp, "\tMesh %s read with %d layer(s) :\n", pcmsh->name, nlay);
  for (i = 0; i < nlay; i++) {
    play = pcmsh->p_layer[i];
    MSH_carac_layer(play, fp);
  }
  LP_printf(fp, "Number of nested cell sizes: %d\n", pcmsh->nsize);

  LP_printf(fp, "\n************General Mesh characteristics:*************\n\tSmallest element side length: %f m\n\tLargest element side length: %f m\n", pcmsh->l_min, pcmsh->l_max);
  LP_printf(fp, "\tBottom left corner ( %f , %f )\n", pcmsh->mini[X_MSH], pcmsh->mini[Y]);
  LP_printf(fp, "\tTop right corner ( %f , %f )\n", pcmsh->maxi[X_MSH], pcmsh->maxi[Y]);
}
/**\fn s_cmsh *MSH_create_carac_glo()
 *\brief create s_cmsh structur
 *
 *
 */
s_cmsh *MSH_create_carac_glo() {
  s_cmsh *pcmsh;
  int i;

  pcmsh = new_carac_mesh();
  bzero((char *)pcmsh, sizeof(s_cmsh));

  pcmsh->pcount = new_counter_msh();
  bzero((char *)pcmsh->pcount, sizeof(s_count_msh));
  // BL ICI POUR PCOUNT_HYDRO !!!;

  pcmsh->nsize = 0;

  for (i = 0; i < Z_MSH; i++) {
    pcmsh->mini[i] = BIG_TS;
    pcmsh->maxi[i] = 0;
    pcmsh->Haut_g[i] = SMALL_TS;
  }
  pcmsh->mini[Z_MSH] = pcmsh->maxi[Z_MSH] = CODE_TS;
  pcmsh->l_min = 0;
  pcmsh->l_max = 0;

  return pcmsh;
}

/**\fn s_cmsh *MSH_chain_cmsh_fwd(s_cmsh *pd1,s_cmsh *pd2)
 *\brief chain carac mesh
 *
 *
 */
s_cmsh *MSH_chain_cmsh_fwd(s_cmsh *pd1, s_cmsh *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd2;
}

/**\fn void MSH_link_mesh_SIG(s_cmsh *pcmsh,FILE *fpout)
 *\brief print correspondance files between SIG id and intern id
 *
 *
 */
void MSH_link_mesh_SIG(s_cmsh *pcmsh, FILE *fpout) {

  s_ele_msh *pele;
  int nlay;
  int i;
  FILE *fich_corresp;
  char nom_corresp[STRING_LENGTH_LP]; // Si taille nom definie --> seg fault

  LP_printf(fpout, "Printing correspondance files between SIG and libmesh%4.2f:\n", VERSION_MSH);

  nlay = pcmsh->pcount->nlayer;

  for (i = 0; i < nlay; i++) {
    sprintf(nom_corresp, "%s/INFO_SOUT/%s_SIG_lmesh%4.2f.txt", getenv("RESULT"), pcmsh->p_layer[i]->name, VERSION_MSH);
    LP_printf(fpout, "\t layer %s in %s\n", pcmsh->p_layer[i]->name, nom_corresp);
    fich_corresp = fopen(nom_corresp, "w");
    fprintf(fich_corresp, "\"id[GIS_MSH]\",\"id[INTERN_MSH]\",\"id[ABS_MSH]\"\n");
    pele = pcmsh->p_layer[i]->pele;
    while (pele != NULL) {
      fprintf(fich_corresp, "%d,%d,%d\n", MSH_get_ele_id(pele, GIS_MSH), MSH_get_ele_id(pele, INTERN_MSH), MSH_get_ele_id(pele, ABS_MSH));
      pele = pele->next;
    }
    fclose(fich_corresp);
  }
}

/* BL cette fonction a été crée suite à la découverte d'un bug sur l'orgeval
les mailles à la frontiere ne sont plus reconnues je ne comprends pas pourquoi
il faudrait regarder les fonctions dans manage_face.c ou le type BOUND est déclaré.
Cette fonction va parcourir l'ensemble des mailles et regarder les voisins.
s'il n'y a pas de voisin le type de l'élément et de la face à travers laquelle on a "regarder" sera fixé à BOUND
*/

void MSH_check_boundary(s_cmsh *pcmsh, FILE *fpout) {
  int l, e;
  int nlayer, nele;

  s_ele_msh *pele;
  s_layer_msh *player;
  nlayer = pcmsh->pcount->nlayer;
  for (l = 0; l < nlayer; l++) {
    player = pcmsh->p_layer[l];

    nele = player->nele;
    for (e = 0; e < nele; e++) {

      pele = player->p_ele[e];
      MSH_check_boundary_ele(pele, fpout);
    }
  }
}

// NF 23/12/2015
/**\fn s_cmsh *MSH_rewind_cmsh(s_cmsh *pcmsh)
 * \brief rewinds pcmsh to the first of the chain
 */
s_cmsh *MSH_rewind_cmsh(s_cmsh *pcmsh) {
  while (pcmsh->prev != NULL)
    pcmsh = pcmsh->prev;
  return pcmsh;
}

// NF 23/12/2015
/**\fn s_cmsh *MSH_find_cmsh_in_simul(s_simul_msh *psimul,char *name)
 *\brief Finds a cmsh by name in a Simulation
 *
 *
 */
s_cmsh *MSH_find_cmsh_in_simul(s_simul_msh *psimul, char *name, FILE *fp) {
  s_cmsh *pcmsh;

  pcmsh = MSH_rewind_cmsh(psimul->pcmsh);

  while (strcmp(name, pcmsh->name) != 0) {
    pcmsh = pcmsh->next;
    if (pcmsh == NULL)
      LP_error(fp, "libmesh%4.2f : In %s l.%d No mesh called %s. Check your Command File\n", VERSION_MSH, __FILE__, __LINE__, name);
  }

  return pcmsh;
}

// NF 13/10/2015 Create a full mesh to be coupled in other applications
void MSH_create_msh(s_cmsh *pcmsh, int check_msh, FILE *poutputs) {
  int nlayer = pcmsh->pcount->nlayer;

  // NF,BL 19/10/2015 Introduction of check_msh YES or NO
  if (check_msh == YES_MSH)
    MSH_check_redundancy(pcmsh, poutputs);
  MSH_referencing_mesh(pcmsh, poutputs); // NF 24/12/2015 Il faudra certainement couper cette etape pour pouvoir partir d'une copie d'un maillage de reference
  MSH_order_element_in_ref(pcmsh, poutputs);
  MSH_check_element_ordering(pcmsh, poutputs);
  MSH_link_mesh_SIG(pcmsh, poutputs);
  LP_printf(poutputs, "creating faces\n");
  MSH_create_ele_faces(pcmsh, poutputs);
  MSH_check_boundary(pcmsh, poutputs);
  MSH_create_vertical_faces(pcmsh, poutputs);
  if (nlayer > 1)
    MSH_check_vertical_connexion(pcmsh, poutputs); // NG 10/09/19 : Pas de verification verticale nécessaire dans le cas monocouche
  MSH_tab_all_neigh(pcmsh, poutputs);
  MSH_calcul_on_all_faces(pcmsh, poutputs);
  if (check_msh == YES_MSH)
    MSH_list_all_ele_pneigh(pcmsh, poutputs);
}
