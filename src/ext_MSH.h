/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: ext_MSH.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file ext_MSH.h
Here are the external parameters, tabular and struturs which, once they have been initialized, can be used directly in the code whithout initialize them an other time.
*/
/* Variables used when reading input data */
extern FILE *yyin;
extern int yydebug;
/* Structure containing the simulation caracteristics */
extern s_simul_msh *Simul;
/* Table of the addresess of the read files */
extern s_file current_read_files[NPILE];
/* Number of folders defined by the ugloser */
extern int folder_nb;
/* File number */
extern int pile;
/* Position within the file being processed */
extern int line_nb;
/* Name of file folders */
extern char *name_out_folder[NPILE];

/* Functions of input.y */
void lecture(char *, FILE *);
int yylex();
#if defined GCC540 || GCC481 || defined GCC473 || defined GCC472 || defined GCC471
void yyerror(char const *);
#else
void yyerror(char *);
#endif /*test on GCC*/
