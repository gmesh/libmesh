/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libmesh
 * FILE NAME: struct_ID.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS,
 *               Deniz KILIC
 *
 * LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh
 * of squared cells.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file struct_ID.h
Here are the specific structurs of id series. had to create a new struct_file
because s_id_msh is called both in struct_MSH.h and struct_AQ_MSH.h
*/

typedef struct index_msh s_id_msh;

struct index_msh {
  int id;
  int id_lay;

  s_id_msh *next;
  s_id_msh *prev;
};

#define new_id_mesh() ((s_id_msh *)malloc(sizeof(s_id_msh)))
