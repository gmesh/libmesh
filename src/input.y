/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libmesh
* FILE NAME: input.y
* 
* CONTRIBUTORS: Nicolas FLIPO, Baptiste LABARTHE, Nicolas GALLOIS, 
*               Deniz KILIC
* 
* LIBRARY BRIEF DESCRIPTION: Creation and management of nested mesh 
* of squared cells.
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libmesh Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


%{
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <libprint.h>
#include <time_series.h>
#include "IO.h"
#include "CHR.h"
#include <spa.h>
#include "MSH.h"
#include "global_MSH.h"//only for the main program
#include "ext_MSH.h"//extern variables



  char name_layer[STRING_LENGTH_LP]; 
  s_layer_msh *player;
  s_ele_msh *pele;
  s_ft *pft;
  int ncoord=0;
  int coordo=0;
  int nele;
  int nele_abs;
  int nlayer=0;
  int intern_id=0;
  int nele_tot=1;
  int i=0;
  int id_new=0;
  int nm=0;
  s_cmsh *pcmsh;//NF 22/12/2015
  s_nestl_msh *pnestl;
  int nnested;
  /*#if defined GCC481 ||  defined GCC473 || defined GCC472 ||  defined GCC471
void yyerror(char const *);
#else
  void yyerror(char *);
#endif //test on GCC
  int yylex();*/
%}


%union {
  /* initial pour ProSe:*/
  double real;
  int integer;
  char *string;
  s_layer_msh *gw_layer;
  s_ele_msh *gw_ele;
  s_cmsh *cmsh;
}

%token <real> LEX_DOUBLE LEX_UNIT LEX_A_UNIT LEX_VAR_UNIT LEX_UNIT_MOL 
%token <integer> LEX_INT LEX_ANSWER LEX_RESOLUTION_MSH
%token  LEX_EQUAL LEX_OPENING_BRACE LEX_CLOSING_BRACE LEX_COLON LEX_SEMI_COLON LEX_VIRGULE LEX_INV  LEX_OPENING_BRACKET LEX_CLOSING_BRACKET LEX_POW LEX_INPUT_FOLDER LEX_OUTPUT_FOLDER LEX_AVIEW LEX_NEWSAM  LEX_LAYER LEX_WRITE_ODIC LEX_NESTED 
%token <string> LEX_NAME 


%type <real> flottant
%type <gw_layer> layer layers def_nested def_nesteds
%type <gw_ele>  lire_eles lire_ele lecture_eles
%type <string> le_name
%type <cmsh> mesh meshs

%start beginning
%%

  


beginning : paths 
            meshs
{
  Simul->pcmsh=$2;//NF 22/12/2015
}
;

/* This part enables to define specific input and output folders */
paths : path paths
| path
;

path : input_folders
| output_folder
;

input_folders : LEX_INPUT_FOLDER folders
;

/* List of input folders' names */
folders : folder folders
| folder 
;

folder : LEX_EQUAL LEX_NAME
{
  if (folder_nb >= NPILE) 
    LP_printf(Simul->poutputs,"Only %d folder names are available as input folders\n",NPILE);
  
  name_out_folder[folder_nb++] = strdup($2);
  LP_printf(Simul->poutputs,"path : %s\n",name_out_folder[folder_nb-1]);
  free($2);
} 
;

/* Definition of the folder where outputs are stored */
output_folder : LEX_OUTPUT_FOLDER LEX_EQUAL LEX_NAME
{
  int noname;
  char *new_name;
  char cmd[ STRING_LENGTH_LP];
  FILE *fp;

  fp=Simul->poutputs;

  Simul->name_outputs = $3;
  new_name = (char *)calloc(strlen($3) + 10,sizeof(char));
  sprintf(new_name,"RESULT=%s",$3);
  LP_printf(fp,"%s\n",new_name);
  noname = putenv(new_name); 
  if (noname == -1)
    LP_error(fp,"File %s, line %d : undefined variable RESULT\n",
		current_read_files[pile].name,line_nb); 
  sprintf(cmd,"mkdir %s",getenv("RESULT")); 
  system(cmd);
} 
;



meshs : mesh meshs
{$$=MSH_chain_cmsh_fwd($1,$2);}
| mesh 
;

mesh :  intro_mesh def_mesh brace
{
  //s_cmsh *pcmsh;//NF 22/12/2015
  //pcmsh=Simul->pcmsh;//NF 22/12/2015
  $$=pcmsh;
  //LP_printf(Simul->poutputs,"Mesh %s allocated\n",pcmsh->name);
  MSH_carac_mesh(pcmsh,Simul->poutputs);
}
| intro_mesh def_mesh nested brace
{
  //s_cmsh *pcmsh;//NF 22/12/2015
  //pcmsh=Simul->pcmsh;//NF 22/12/2015
  $$=pcmsh;
  //LP_printf(Simul->poutputs,"Mesh %s allocated\n",pcmsh->name);
  MSH_carac_mesh(pcmsh,Simul->poutputs);
};

intro_mesh : LEX_NEWSAM LEX_EQUAL brace
{
  pcmsh = MSH_create_carac_glo();
  nnested=0;
};

def_mesh : le_name option tab_layer  {
  sprintf(pcmsh->name,"%s",$1);
}
|  option tab_layer ;
|  tab_layer ;
|  le_name  tab_layer  {
  sprintf(pcmsh->name,"%s",$1);
};

option : LEX_WRITE_ODIC LEX_EQUAL LEX_ANSWER
{
  Simul->write_odic = $3;
 
}
;

tab_layer : layers
{
  int i;
  double ratio_tmp;
  double precision;
  s_cmsh *pcmsh;
  double dimension;

  //pcmsh=Simul->pcmsh;

  pcmsh->pcount->nlayer=nlayer;
  pcmsh->p_layer=MSH_tab_layer(player,nlayer,Simul->poutputs);

  ratio_tmp=pcmsh->l_max/pcmsh->l_min;
  pcmsh->dimension=(int)ratio_tmp;
  precision=fabs(ratio_tmp-pcmsh->dimension);
  if (precision>EPS_MSH){
    LP_error(Simul->poutputs,"Pb de Dimension calculee %f/%f = %f estime a %d\n",pcmsh->l_max,pcmsh->l_min,ratio_tmp,pcmsh->dimension);
  }

  for (i=nlayer-1;i>=0;i--)
    {
      pele=pcmsh->p_layer[i]->pele;
      MSH_calcul_sur_ele(pele,pcmsh);//NF 24/12/2015
    }

}
;

layers : layer layers
{
$$=MSH_chain_layer($1,$2);
}
|layer {$$=$1;}
;

layer : lire_layer carac_layer  brace
{
  
  // player->id=++nlayer;
  $$ = player;
  printf("layer %s, id %d read\n",player->name,player->id);
}
;

lire_layer : LEX_LAYER LEX_EQUAL brace le_name
{
  int i;
  player = MSH_create_layer($4,++nlayer);
  nele=0;
  if(Simul->write_odic==YES_MSH && nlayer==2)
    nele_tot=1;
}
;
 
carac_layer : lecture_eles LEX_AVIEW
{
  int i;
  player->nele=nele;
  player->p_ele=MSH_tab_ele($1,nele,Simul->poutputs);
  nele=0;
  //intern_id=Simul->pcmsh->pcount->nele;
}
;

le_name :LEX_NAME
{
  $$=$1;
  //sprintf(player->name,"%s",$1);
}
;


lecture_eles : lire_eles
{
  player->pele=$1;
  $$=$1;
}
;

lire_eles : lire_ele lire_eles
{
  $$=MSH_chain_ele($1,$2);
}
| lire_ele {
  $$=$1;}
; 


lire_ele : lire_id_ele lire_coordonne lire_coordonne lire_coordonne lire_coordonne lire_coordonne LEX_AVIEW
{

  MSH_ordinate_mesh(pele,pcmsh);
  pele->player=player;
  $$=pele;
}
;

lire_id_ele : LEX_INT LEX_AVIEW
{
  pele=MSH_create_element($1,ACTIVE);
  pele->id[INTERN_MSH] = nele++;
  pele->id[ABS_MSH] = nele_tot++;
  pele->nele=1;
  coordo=0;
}
;

lire_coordonne :  flottant LEX_VIRGULE flottant
{
  if (coordo<4)
    {
      pele->pdescr->coord[coordo][0] = $1;
      pele->pdescr->coord[coordo++][1] = $3;
    }
}
;

nested : intro_nested def_nesteds brace
{
  
  int i;
  double ratio_tmp;
  double precision;
  s_cmsh *pcmsh;
  double dimension;

  //pcmsh=Simul->pcmsh;

  pcmsh->pcount->nlayer=nlayer;
  pcmsh->play=MSH_browse_layer(player,BEGINNING_TS);
  pcmsh->p_layer=MSH_tab_layer(player,nlayer,Simul->poutputs);

  ratio_tmp=pcmsh->l_max/pcmsh->l_min;
  pcmsh->dimension=(int)ratio_tmp;
  precision=fabs(ratio_tmp-pcmsh->dimension);
  if (precision>EPS_MSH){
    LP_error(Simul->poutputs,"Mesh %s Pb de Dimension calculee %f/%f = %f estime a %d\n",pcmsh->name,pcmsh->l_max,pcmsh->l_min,ratio_tmp,pcmsh->dimension);
  }

  for (i=nlayer-1;i>=0;i--)
    {
      pele=pcmsh->p_layer[i]->pele;
      MSH_calcul_sur_ele(pele,pcmsh);//NF 24/12/2015
    }

  pnestl->pnestcmsh=pcmsh;
  Simul->nestl=pnestl;
}
;

intro_nested : LEX_NESTED LEX_EQUAL brace
{
  pnestl=MSH_create_nestl(nnested++);
  pcmsh=MSH_create_carac_glo();
  sprintf(pcmsh->name,"NESTED");
  nlayer=0;
}
|  option tab_layer ;
|  tab_layer ;
|  le_name  tab_layer  {
  sprintf(pcmsh->name,"%s",$1);
};

option : LEX_WRITE_ODIC LEX_EQUAL LEX_ANSWER
{
  Simul->write_odic = $3;
 
}
;



def_nesteds : def_nested def_nesteds
{$$=MSH_chain_layer($1,$2);}
| def_nested
{
  $$=player;
}
;


def_nested : LEX_RESOLUTION_MSH LEX_EQUAL brace le_name le_name brace
{
  s_layer_msh *play;
  s_cmsh *pcmsh_tmp;
  nlayer++;
  pcmsh_tmp=MSH_find_cmsh_in_simul(Simul,$4,Simul->poutputs);
  play=MSH_find_layer_in_cmsh(pcmsh_tmp,$5,Simul->poutputs);
  pnestl->nested[$1]=MSH_copy_layer(play,Simul->poutputs);
  player=pnestl->nested[$1];
};


;

brace :  LEX_OPENING_BRACE
|  LEX_CLOSING_BRACE
;

flottant : LEX_DOUBLE
{
  $$ = $1;
}
| LEX_INT
{
  $$ = (double)$1;
}
;

%%

/* Procedure used to display errors
 * automatically called in the case of synthax errors
 * Specifies the file name and the line where the error was found
 */
#if defined GCC540 || GCC481 || defined GCC473 || defined GCC472 ||  defined GCC471
void yyerror(char const *s)
#else 
void yyerror(char *s)
#endif
{
  if (pile >= 0) {
    //fprintf(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
    //exit -1;
    LP_error(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
  }
}

void lecture(char *name,FILE *fp) 
{


  pile = 0;
  current_read_files[pile].name = (char *)strdup(name);
  if ((yyin = fopen(name,"r")) == NULL)
    LP_error(Simul->poutputs,"File %s doesn't exist\n",name);
  current_read_files[pile].address = yyin;
  current_read_files[pile].line_nb = line_nb;

  LP_printf(fp,"\n****Reading input data****\n");
  yyparse();
  LP_printf(fp,"\n****Input data read****\n");
 
  
}
